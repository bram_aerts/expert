#########################################################
# E'XperT project					# 
#########################################################
# Description: 	Alarm data (CSV to Problog parser)	#
# Author(s): 	Bram Aerts - Shana Van Dessel		#
# Document:	parser_csv.py				#
# Date:		3 november 2015				#
#########################################################

from bs4 import BeautifulSoup

def get_ids(xml_file):					# List of all ID's
	soup = BeautifulSoup(open(xml_file), "xml")
	res = soup.findAll("TechnicalId")
	return [i.text for i in res] 

def to_timestamp(date, time, start_time_epoch):		# Creating timestamps
	import time
	epoch = int(time.mktime(time.strptime(date + ";" + time, "%d/%m/%Y;%h:%M:%s")))
	return (epoch - start_time_epoch)/2

def alarms_for_each_time(alarm_file, ids):		# List of all alarms
	all_alarms = dict()
	

def find_component(part, ids)
	# XXX
	return component

def find_alarmtype(part, component)
	# XXX
	return alarmtype



def alarms_per_timestamp(alarm_file, ids, point_in_time, start_index, alarms): 	# List of all alarms per timestamp
	f = open(alarm_file,"r")
	for i,line in enumerate(f.readline()):
		if i < start_index:
			continue
		parts = line.split(";")
		timestamp = to_timestamp(parts[0],parts[1])
		if timestamp < point_in_time:
			continue
		if timestamp > point_in_time:
			break
		component = find_component(parts[2], ids)
		alarmtype = find_alarmtype(parts[2], component)
		if parts[3] == "OFF":
			del alarms[(component, alarmtype)]
		if parts[3] == "ACK":
			continue
		if parts[3] == "ON":
			alarms[(component, alarmtype)] = True
	return i, alarms
		
def alarms_to_evidence(alarm_file, ids):
	fout = open("alarm.prob", "w")			


	fout.write("\n")
	
def main():
	alarms_to_evidence("alarm.csv", get_ids("terminal.xml"))							

if __name__ == "__main__":									
	main()
