""" E'XperT project - activePath.py: Detect active paths based on terminal layout and terminal state
"""
__author__ = "Shana Van Dessel, and Bram Aerts"

import sys
sys.path.insert(0, '..')
import toProblog as toProb

LayoutInputFile = "../TerminalLayout/terminalLayout.prob"	# problog terminal layout
StateInputFile = "../TerminalState/terminalState.prob"		# problog terminal state
LogicInputFile = "logic.prob"								# problog logic

CodeOutputFile = "activePath.prob"		# problog code 
ProblogOutputFile = "problogOutput.txt"	# problog output
ResultOutputFile = "activePath.csv"		# problog result

componentsName = 'component_in_active_path'
endPointsName = 'path'

# ****************************************************************************************************

def ActivePath(fParts=[LayoutInputFile,StateInputFile,LogicInputFile], fFull=CodeOutputFile, 
				fOutput=ProblogOutputFile, fResult=ResultOutputFile, write=True):
	print("Merge problog code parts (" + ", ".join([f.split('/')[-1] for f in fParts]) + ") ...")
	toProb.MergeProblogFiles(fParts,fFull,[__author__],"E'XperT project")

	print("Run problog code (" + fFull.split('/')[-1] + ") ...")
	output = toProb.RunProblogFile(fFull)
	toProb.WriteProblogOutputFile(output,fOutput)

	print("Write result to file (" + fResult.split('/')[-1] + ") ...")
	result = toProb.GetSpecificPredicatesArguments(output, [componentsName,endPointsName])
	
	if not write:
		return result

	with open(fResult,'w') as f:
		for key in result:
			for item in result[key]:
				f.write(key + "," + ",".join(item) + "\n")

	return result
	
# ****************************************************************************************************

def main():
	ActivePath()
	print("Done.")
	
if __name__ == "__main__":									
	main()
