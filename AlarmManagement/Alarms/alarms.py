""" E'XperT project - alarms.py: Handle the alarm data from the terminal
"""
__author__ = "Shana Van Dessel, and Bram Aerts"

import logging as lg
import time
import sys
import collections

sys.path.insert(0, '..')
import toProblog as toProb

logModes = {'debug': lg.DEBUG,'info': lg.INFO,'warning': lg.WARNING,'error': lg.ERROR,'critical': lg.CRITICAL}

alarmInputFile = "../../Data/alarms_testfile.csv"	# input data alarm logging
alarmOutputFile = "alarms.prob"						# output data alarm logging 

# ****************************************************************************************************

def Alarms(fCsv=alarmInputFile,fProblog=alarmOutputFile):
	print("Read alarm logging data from csv file (" + fCsv.split('/')[-1] + ") ...")
	data = ReadAlarmsFromCsv(fCsv)
	print("Write alarm logging data to problog file (" + fProblog.split('/')[-1] + ") ...")
	WriteAlarmsToProblog(fProblog,data)
	return 

# ----------------------------------------------------------------------------------------------------

def ReadAlarmsFromCsv(file):
	""" Get alarm logging data from csv file as a dictionary 
		with for each timestamp (key) a list off occuring alarms.
	"""
	lg.info("\n---------------------\n| CSV ALARM LOGGING |\n---------------------")
	data = collections.OrderedDict()
	with open(file,"r") as f:
		f.readline()											# skip first line
		
		for l in f:
			date, time, alarm, ack = l.strip().split(',')		# split alarm log in date, time, alarm and acknowledge

			alarm = alarm.split(':')							# split the specific alarm in id (if there is any) and message
			if len(alarm)>1:
				alarmId = alarm[0].strip()
				alarmMsg = alarm[1].strip()
			else:
				continue
				#alarmId = None
				#alarmMsg = alarm[0].strip()

			if (date,time) not in data:							# create new key
				data[(date,time)] = list()
				lg.debug(" " + date + " " + time)
			data[(date,time)].append((alarmMsg,alarmId,ack))	# add alarm (id,msg,ack) to dict (key=(date,time))
			lg.info("  \t" + (alarmId + " " if alarmId is not None else "") + alarmMsg + " (" + ack + ")")
	return data

# ----------------------------------------------------------------------------------------------------

def WriteAlarmsToProblog(file,data):
	""" Convert the alarm logging data to problog.
	"""
	lg.info("\n---------------------\n| PROBLOG EVIDENCE  |\n---------------------")
	alarms = GetAlarmList(data)

	with open(file,"w") as f:
		f.write(toProb.Header(f.name.split('/')[-1],[__author__],"E'XperT project"))
		f.write(toProb.Title("Alarms"))
		f.write(toProb.ListToEvidence("Alarm",alarms))
	return 

def GetAlarmList(data):
	""" Create for each timestamp a list off active alarms based on the alarm logging data.
	"""
	alarmList = list()
	alarmsPerTimestamp = list()

	alarm = list(data.items())[0] 		# Get first key and value from dict
	date,time = alarm[0]				# Get only the key from this (which is a tuple in this case)
	its = GetInitTimestamp(date,time) -1
	nts = GetTimestamp(date,time,its)
	lg.debug(" Timestamp " + str(nts))

	for key in data:
		date,time = key
		ts = GetTimestamp(date,time,its)
		for alarm in data[key]:
			while ts > nts:				# Timestamps in which nothing changes
				alarmList.extend([(alarmPT[0],alarmPT[1],nts) for alarmPT in alarmsPerTimestamp])
				lg.info("  Alarm list (timestamp " + str(nts) + "): " + str([x for x in alarmList if x[2]==nts]))
				nts += 1
				lg.debug(" Timestamp " + str(nts))
			alarmsPerTimestamp = addAlarmToList(alarmsPerTimestamp,alarm)
		
	alarmList.extend([(alarm[0],alarm[1],nts) for alarm in alarmsPerTimestamp])
	lg.info("  Alarm list (timestamp " + str(nts) + "): " + str([x for x in alarmList if x[2]==nts]))
	
	evidenceList = list()
	for x in alarmList:
		element = [str(y) for y in x]
		element.append("true")
		evidenceList.append(tuple(element))
	return evidenceList

def addAlarmToList(list,alarm):
	id, msg, ack = alarm
		
	if ack.lower() == "off":
		list.remove((id,msg))
		lg.debug(" \tAlarm removed " + str(alarm))

	elif ack.lower() == "on":
		list.append((id,msg))
		lg.debug(" \tAlarm added " + str(alarm))
	
	else:
		lg.debug(" \tAlarm acknowledged " + str(alarm))

	return list

def GetInitTimestamp(date,time):	
	epoch = GetTimestamp(date,time,0)
	return epoch

def GetTimestamp(dmy,hms,initEpoch):	
	date = dmy + " " + hms
	epoch = int(time.mktime(time.strptime(date,"%d/%m/%y %H:%M:%S"))) - initEpoch
	#print(" \tAlarm timestamp is " + str(epoch))
	return epoch

# ****************************************************************************************************

def Logging():
	""" Logging. """
	if len(sys.argv) > 1: 
		lg.basicConfig(level=logModes.get(sys.argv[1], lg.NOTSET))
	else:
		lg.basicConfig(level=lg.CRITICAL)
	return

# ****************************************************************************************************

def main():
	Logging()
	Alarms()
	print("Done.")
	
if __name__ == "__main__":									
	main()
