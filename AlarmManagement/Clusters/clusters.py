""" E'XperT project - clusters.py: Cluster the alarms based on the active paths.
"""
__author__ = "Shana Van Dessel, and Bram Aerts"

import sys
import logging as lg
logModes = {'debug': lg.DEBUG,'info': lg.INFO,'warning': lg.WARNING,'error': lg.ERROR,'critical': lg.CRITICAL}

alarmInputFile = "../../Data/alarms_testfile.csv"	# input data alarm logging
pathInputFile = "../ActivePath/activePath.csv"		# input data active paths
alarmOutputFile = "clusters.csv"			# output data alarm logging (clustered)

componentsName = 'component_in_active_path'

# ****************************************************************************************************

def Clusters(fIn=alarmInputFile,fOut=alarmOutputFile,fPath=pathInputFile):
	print("Get active components from csv file (" + fPath.split('/')[-1] + ") ...")
	components = ReadActiveComponentsFromCsv(fPath)
	print("Cluster alarm logging data (" + fIn.split('/')[-1] + " -> " + fOut.split('/')[-1] +") ...")
	ClusterAlarms(fIn,fOut,components)
	return 

# ----------------------------------------------------------------------------------------------------

def ReadActiveComponentsFromCsv(file):
	""" Get the active components (components on an active path) from csv file 
	"""
	lg.info("\n---------------------\n| ACTIVE COMPONENTS |\n---------------------")
	components = list()
	with open(file,'r') as f:
		for line in f:
			parts = line.strip().split(";")
			parts = [y for x in parts for y in x.split(",")]
			if parts[0] != componentsName:
				lg.debug(" No - " + line.rstrip())
				continue
			lg.debug(" Yes - " + line.rstrip())
			components.append(parts[1].lower())		
	lg.info("  Active components: " + ", ".join(components) + "\n")
	return components

# ----------------------------------------------------------------------------------------------------

def ClusterAlarms(fIn,fOut,components):
	with open(fIn,'r') as fi:
		with open(fOut,'w') as fo:
			fo.write("function," + fi.readline().strip())
			for line in fi:
				function = 0
				parts = line.strip().split(",")
				component = GetComponentByInstrument(parts[2].split(':')[0])
				if component.lower() in components:
					lg.info("  Alarm on ACTIVE component " + component + " \t(" + line.strip() + ")")
					function = 1
				else:
					lg.debug(" Alarm on inactive component " + component + " \t(" + line.strip() + ")")
					
				fo.write(str(function) + ',' + ','.join(parts) + "\n")
	return

def GetComponentByInstrument(iID):
	# TODO
	cID = iID.split('-')[0]
	lg.debug(" " + iID + " on " + cID)
	return cID
      
# ****************************************************************************************************

def Logging():
	""" Logging. """
	if len(sys.argv) > 1: 
		lg.basicConfig(level=logModes.get(sys.argv[1], lg.NOTSET))
	else:
		lg.basicConfig(level=lg.CRITICAL)
	return

# ****************************************************************************************************

def main():
	Logging()
	Clusters()
	print("Done.")
	
if __name__ == "__main__":									
	main()
