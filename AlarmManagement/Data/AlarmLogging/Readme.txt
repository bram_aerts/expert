Control_alarm_1...22: dit is de runtime data. Dit is een export van al de alarmen van 1 maand die de controle kamer te verwerken krijgt.

Control_alarm_stat_20150921-20150710: dit is een overzicht van het aantal keer dat een specifiek alarm is opgetreden van 10 juni tot 21 september 2015.
De alarmlogging gaat op dit moment terug tot 10 juni 2015.

Export_alarmregels_20150921: dit is de lijst met alle alarmregels die gedefinieerd zijn in de alarmlogging in WinCC.
Hier kan ook terug gevonden worden in welke klasse en type deze gedefinieerd zijn.