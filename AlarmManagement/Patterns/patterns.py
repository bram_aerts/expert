from __future__ import print_function
""" E'XperT project - alarms.py: Handle the alarm data from the terminal
"""
__author__ = "Shana Van Dessel, Bram Aerts and Wannes Meert"

import logging as lg
import pandas as pd
import sys
import re
from datetime import datetime

from ppmg import PPMG

logModes = {'debug': lg.DEBUG,'info': lg.INFO,'warning': lg.WARNING,'error': lg.ERROR,'critical': lg.CRITICAL}

alarmInputFile = "../Clusters/clusters.csv"		# alarm logging data, clustered by active path
graphOutputFile = "Graphs/patterns.dot"				# pattern graphs

# ****************************************************************************************************
def Patterns(fCsv=alarmInputFile,fDot=graphOutputFile,clustering=False):
	print("Read alarm logging data from csv file (" + fCsv.split('/')[-1] + ") ...")
	data = ReadAlarmsFromCsv(fCsv)
	data["description_ack"] = data["description"] + ' ' + data["ack"]
	
	if clustering:
		dataPerCluster = dict()				# Initialize structure 'dataPerCluster'
		for i in range(max(data["function"])+1):
			dataPerCluster[i]=dict() 
			for key in data:
				if key == "function":
					continue
				dataPerCluster[i][key] = list()
		for key in data:				# Fill structure 'dataPerCluster'
			if key == "function":
				continue
			for func, item in zip(data["function"],data[key]):
				dataPerCluster[func][key].append(item)
	else:
		dataPerCluster = dict({0:data})
	
	Patterns1(fDot,dataPerCluster)
	Patterns2(fDot,dataPerCluster)
	return

def Patterns1(fDot,data):
	print("Search for naive patterns ...")
	for func in data:
		if func == 0 and len(data.keys()) > 1:
			filename = CreateFilename(fDot,pre="noFunction_")
		elif func != 0:
			filename = CreateFilename(fDot,pre= ("function"+str(func)+'_') )
	
		PatternsNaive(fDot,data[func])				
		PatternsOnComponents(fDot,data[func])
		PatternsOnTanks(fDot,data[func])		
		#PatternsNaive(fDot,data[func],depth=8,occur=10)		# Wannes Meert
		#PatternsOnComponents(fDot,data[func],depth=14,occur=3)	# Wannes Meert
		#PatternsOnTanks(fDot,data[func],depth=8,occur=3)		# Wannes Meert
	return

def Patterns2(fDot,data):
	print("Search for (more) intelligent patterns ...")
	for func in data:
		if func == 0 and len(data.keys()) > 1:
			filename = CreateFilename(fDot,pre="noFunction_")
		elif func != 0:
			filename = CreateFilename(fDot,pre= ("function"+str(func)+'_') )
		PatternsUpcoming(fDot,data[func])
	return

# ----------------------------------------------------------------------------------------------------

def ReadAlarmsFromCsv(file):
	""" Get alarm logging data from csv file.
	"""
	data = pd.read_csv(file, sep=',', index_col=0, parse_dates=[["date","time"]], date_parser=date_parser)
	lg.info("  Data:\n" + str(data[:10]) + "\n...\n")
	return data

def date_parser(x, y):
	return datetime.strptime(x+' '+y, '%d/%m/%y %H:%M:%S')

# ----------------------------------------------------------------------------------------------------

def RecognizePatterns(filename,seq,depth=6,occur=1):
	lg.info("  ppmg with depth of {} and minimal appearance of {}".format(depth,occur))
	model = PPMG(depth=depth, autofill_graph="greedy")
	model.learn_sequence(seq)
	with open(filename, 'w') as f:
		print(model.to_dot(min_amount=occur),file=f)
	print("... " + filename.split('/')[-1])
	return

# -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

def PatternsUpcoming(filename,data):
	fn = CreateFilename(filename,post="Upcoming")
	dataOn = data[data["ack"]=="ON"]
	seq = dataOn["description"]
	
	if len(seq) == 0:
		lg.error(" No sequence found!")
		return

	lg.info("  Data for upcoming alarms patterns:\n" + "\n".join( (seq[:10] if len(seq)>10 else seq) ))
	RecognizePatterns(fn,seq)
	return

# -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

def PatternsNaive(filename,data,depth=8,occur=1):
	fn = CreateFilename(filename,post="Naive")
	seq = data["description_ack"]
	
	if len(seq) == 0:
		lg.error(" No sequence found!")
		return

	lg.info("  Data for naive patterns:\n" + "\n".join( (seq[:10] if len(seq)>10 else seq) ))
	RecognizePatterns(fn,seq,depth=depth,occur=occur)
	return

def PatternsOnComponents(filename,data,depth=8,occur=1):
	fn = CreateFilename(filename,post="OnComponents")
	reComp = re.compile(""".*(T[0-9]+|XV[0-9]+|HV[0-9]+|P[0-9]+).*""")
	seq = []
	prev = None
	for msg in data["description"]:
		result = reComp.match(msg)
		if result is not None and result.group(1) != prev:
			seq.append(result.group(1))
			prev = result.group(1)
	if len(seq) == 0:
		lg.error(" No sequence found!")
		return
	lg.info("  Data for naive patterns on components:\n" + "\n".join( (seq[:10] if len(seq)>10 else seq) ))
	RecognizePatterns(fn,seq,depth=depth,occur=occur)
	return

def PatternsOnTanks(filename,data,depth=8,occur=1):
	fn = CreateFilename(filename,post="OnTanks")
	reComp = re.compile(""".*(T[0-9]+).*""")
	seq = []
	prev = None
	for msg in data["description"]:
		result = reComp.match(msg)
		if result is not None and result.group(1) != prev:
			seq.append(result.group(1))
			prev = result.group(1)
	if len(seq) == 0:
		lg.error(" No sequence found!")
		return
	lg.info("  Data for naive patterns on components:\n" + "\n".join( (seq[:10] if len(seq)>10 else seq) ))
	RecognizePatterns(fn,seq,depth=depth,occur=occur)
	return

# ****************************************************************************************************

def Logging():
	""" Logging. """
	if len(sys.argv) > 1: 
		lg.basicConfig(level=logModes.get(sys.argv[1], lg.NOTSET))
	else:
		lg.basicConfig(level=lg.CRITICAL)
	return

# ****************************************************************************************************

def CreateFilename(filename,pre='',post=''):
	name = filename.split('/')[-1]
	path = filename[:-len(name)]
	name,ext = name.split('.')
	newFilename = path + pre + name + post + '.' + ext
	lg.debug(" " + filename.split('/')[-1] + " -> " + newFilename.split('/')[-1])
	return newFilename

def main():
	Logging()
	Patterns()
	print("Done.")
	
if __name__ == "__main__":									
	main()
