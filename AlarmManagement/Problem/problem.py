""" E'XperT project - problem.py: Suggest problem based on terminal layout and alarms
"""
__author__ = "Shana Van Dessel, and Bram Aerts"

import sys
sys.path.insert(0, '..')
import toProblog as toProb

LayoutInputFile = "../TerminalLayout/terminalLayout.prob"	# problog terminal layout
AlarmsInputFile = "../Alarms/alarms.prob"					# problog evidence alarms
LogicInputFile = "logic.prob"								# problog logic

CodeOutputFile = "problem.prob"			# full problog code
ProblogOutputFile = "problogOutput.txt"	# problog result
ResultOutputFile = "problem.csv"		# problog result

problem1 = 'fout'
problem2 = 'slecht_contact'

# ****************************************************************************************************

def Problem(fParts=[LayoutInputFile,AlarmsInputFile,LogicInputFile], fFull=CodeOutputFile, 
				fOutput=ProblogOutputFile, fResult=ResultOutputFile, write=True):
	print("Merge problog code parts (" + ", ".join([f.split('/')[-1] for f in fParts]) + ") ...")
	toProb.MergeProblogFiles(fParts,fFull,[__author__],"E'XperT project")

	print("Run problog code (" + fFull.split('/')[-1] + ") ...")
	output = toProb.RunProblogFile(fFull)
	toProb.WriteProblogOutputFile(output,fOutput)

	print("Write result to file (" + fResult.split('/')[-1] + ") ...")
	result = toProb.GetSpecificPredicatesArguments(output, [problem1,problem2],values=True)
	if not write:
		return result
	with open(fResult,'w') as f:
		for key in result:
			for item in result[key]:
				f.write(key + "," + ",".join(item) + "\n")

	return result
	
# ****************************************************************************************************

def main():
	Problem()
	print("Done.")
	
if __name__ == "__main__":									
	main()
