""" E'XperT project - model.py: Handle the terminal layout data
"""
__author__ = "Shana Van Dessel"

import logging as lg
import sys
import xml.etree.ElementTree as et

sys.path.insert(0, '..')
import toProblog as toProb

logModes = {'debug': lg.DEBUG,'info': lg.INFO,'warning': lg.WARNING,'error': lg.ERROR,'critical': lg.CRITICAL}

modelInputFile = "../../Data/TerminalLayout/model.xml"	# input data terminal
modelOutputFile = "terminalLayout.prob"					# output data terminal

# ****************************************************************************************************

def TerminalLayout(fxml=modelInputFile,fproblog=modelOutputFile):
	print("Read terminal layout data from xml file (" + fxml.split('/')[-1] + ") ...")
	data = ReadLayoutFromXml(fxml)
	print("Write terminal layout data to problog file (" + fproblog.split('/')[-1] + ") ...")
	WriteLayoutToProblog(fproblog,data)
	return 

# ----------------------------------------------------------------------------------------------------

def ReadLayoutFromXml(file):
	""" Get terminal layout from xml file. 
	"""
	lg.info("\n--------------------\n|  XML MODEL       |\n--------------------")
	root = et.parse(file).getroot()

	data = dict()
	data['tanks'] = GetComponents(root,'equipment','Tanks',['Connections'])
	data['valves'] = GetComponents(root,'equipment','Valves')
	data['quays'] = GetComponents(root,'equipment','ProductTransferConnections')	
	data['pumps'] = GetComponents(root,'equipment','Pumps',['In','Out'])
	data['pipes'] = GetComponents(root,None,'pipelines',['Connections'])
	return data 

def GetComponents(root,kind,comp,attr=list()):
	""" Get a specific type of component from the xml tree. 
	"""
	lg.info("  Get " + comp + "... ")
	if kind is not None:
		xmlComponents = root.find(kind).find(comp)
	else:
		xmlComponents = root.find(comp)

	if attr == []:
		lg.debug(" \tSearch for " + comp)
		components = GetComponentsAsList(xmlComponents)
	else:
		lg.debug(" \tSearch for " + comp + " and " + ', '.join(attr))
		components = GetComponentsAsDict(xmlComponents,attr)

	lg.info(" \t" + str(len(components)) + " " + comp +" found")
	return components
			
def GetComponentsAsList(xml):	
	""" Get components as a simple list . 
	"""	
	components = list()	
	for x in xml:
		c = x.find('TechnicalId').text
		components.append(c)
		lg.debug(" \t" + c)
	return components	
		
def GetComponentsAsDict(xml,attr):
	""" Get components as a dict. For each component ID (key) the dict contains another dict for the component's attributes. 
		This dict holds the name of each attribute (key) and its ID('s) (value). 
	"""
	components = dict()
	for x in xml:
		c = x.find('TechnicalId').text
		components[c] = dict()	
		lg.debug(" \t" + c)
		for y in attr:
			components[c][y] = list()	
			for z in x.findall(".//"+y):
				for u in z.findall('.//TechnicalId'):
					ca = u.text
					components[c][y].append(ca)
			lg.debug(" \t  " + ", ".join(components[c][y]))		
	return components

# ----------------------------------------------------------------------------------------------------

def WriteLayoutToProblog(file,data):
	""" Convert the terminal layout data to problog. 
	"""
	lg.info("\n--------------------\n|  PROBLOG MODEL   |\n--------------------")

	tanks =[[key] for key in data['tanks']]
	tankConns = [[item] for key in data['tanks'] 
						for item in data['tanks'][key]['Connections']]
	quays = [[key] for key in data['quays']]
	valves = [[key] for key in data['valves']]
	pumps = [[key,item1,item2] for key in data['pumps'] 
							for item1 in data['pumps'][key]['In'] 
							for item2 in data['pumps'][key]['Out']]
	pipes = [[key] for key in data['pipes']]
	conns1 = [[key,item] for key in data['tanks'] 
						for item in data['tanks'][key]['Connections']]
	conns2 = [[key,item] for key in data['pipes'] 
						for item in data['pipes'][key]['Connections']]
	conns1.extend(conns2)
	conns = [[str(i+1),item[0],item[1]] for i,item in enumerate(conns1)]
	
	with open(file,"w") as f:
		f.write(toProb.Header(f.name.split('/')[-1],[__author__],"E'XperT project"))
		f.write(toProb.Title("Terminal layout"))

		f.write(toProb.ListToPredicates("tank",tanks) + "\n")
		f.write(toProb.DictToPredicates(dict({"tankConnector":tankConns})) + "\n")
		f.write(toProb.DictToPredicates(dict({"quay":quays})) + "\n")
		f.write(toProb.DictToPredicates(dict({"valve":valves})) + "\n")
		f.write(toProb.DictToPredicates(dict({"pump":pumps})) + "\n")
		f.write(toProb.DictToPredicates(dict({"pipe":pipes})) + "\n")
		f.write(toProb.DictToPredicates(dict({"connection":conns})) + "\n")
		# XXX TODO XXX
		f.write(\
"""instrument(t1_s,sensor,t1).	
instrument(xv101_s,sensor,xv101).
instrument(xv102_s,sensor,xv102).
instrument(xv103_s,sensor,xv103).
instrument(xv104_s,sensor,xv104).
instrument(xv105_s,sensor,xv105).
instrument(xv301_s,sensor,xv301).
instrument(xv302_s,sensor,xv302).
instrument(xv303_s,sensor,xv303).
instrument(xv304_s,sensor,xv304).
instrument(xv305_s,sensor,xv305).
instrument(xv306_s,sensor,xv306).
instrument(xv307_s,sensor,xv307).
""")
	return

# ****************************************************************************************************

def Logging():
	""" Logging. """
	if len(sys.argv) > 1: 
		lg.basicConfig(level=logModes.get(sys.argv[1], lg.NOTSET))
	else:
		lg.basicConfig(level=lg.CRITICAL)
	return

# ****************************************************************************************************

def main():
	Logging()
	TerminalLayout()
	print("Done.")
	
if __name__ == "__main__":									
	main()
