""" toProblog.py: import file Problog stuff
"""
__author__ = "Shana Van Dessel"

from problog.program import PrologString
from problog import get_evaluatable
import re
invalidSigns = ["-",".",","," "]
regex = {"predicateArgs": r'(?<=\().*(?=\))',"predicateName": r'[a-zA-Z0-9_]+(?=\()'}
#commentSign = "%"

# ****************************************************************************************************

# PROBLOG FILES

def RunProblogModel(model):
	""" Run a problog model.
 	"""
	return get_evaluatable().create_from(PrologString(model)).evaluate()

def RunProblogFile(file):
	""" Run a problog model from a file.
 	"""
	with open(file,'r') as f: 
		model = f.read()
	return RunProblogModel(model)

def WriteProblogOutputFile(result,file):
	""" Write the resulting problog output (dict) from "RunProblogFile" to a file. 
	"""
	with open(file,'w') as f:
		for component in result:
			f.write(str(component) + ":" + str(result[component]) + "\n")
	return

def MergeProblogFiles(oldFiles,newFile,authors,project):
	""" Merge different problog files into one, removing all old headers and replacing it by a new one. 
	"""
	with open(newFile,'w') as f:
		f.write(Header(f.name.split('/')[-1],authors,project))
		for oldFile in oldFiles:
			with open(oldFile,'r') as g:
				for line in g:
					if line[0] != '%':
						break
				for line in g:
					f.write(line)
				f.write("\n")
	return

# ----------------------------------------------------------------------------------------------------

# PROBLOG DATA RETRIEVAL

def GetPredicateArguments(predicate):
	""" Get the name, the arguments and (optional) the value of a predicate.
	"""
	name = re.search(regex["predicateName"], predicate, re.I|re.M)		# search for regex
	args = re.search(regex["predicateArgs"], predicate, re.I|re.M)		# search for regex
	if name is None or args is None: 				
		print("this is not a predicate: " + predicate)
		return None
	name = name.group(0)
	args = args.group(0).split(',')
	
	#if len(args)==1:
	#	return name, args[0]
	return name, tuple(args)

def GetPredicatesArguments(predicates,values=False):
	""" Get a list of the predicates' arguments per kind of predicate. 
	"""
	d = dict()
	for key in predicates:
		name,args = GetPredicateArguments(str(key))
		if values == True:
			args.append(predicates[key])
		if name not in d:
			d[name] = list()
		d[name].append(args)
	return d

def GetSpecificPredicatesArguments(predicates,wantedPredicates,values=False):
	""" Get a list of the predicates' arguments per kind of predicate. 
	"""
	d = dict()
	for wantedPredicate in wantedPredicates:
		d[wantedPredicate] = list()
	for key in predicates:
		name,args = GetPredicateArguments(str(key))
		if values == True:
			args += (str(predicates[key]),)
		if name not in d:
			continue
		d[name].append(args)
	return d

# ----------------------------------------------------------------------------------------------------

# PROBLOG CODE RULES

def Predicate(name,args):
	""" Creates a predicate based on a given predicate name and its arguments.
		(Invalid signs and such are replaced befor creating the problog code. )
		ex.: name="double" args=(1,2) -> double(1,2).
 	"""
	s = StringToProblog(name) + "(" 
	for a in args[:-1]:
		s += StringToProblog(a) + ","	
	s += StringToProblog(args[-1]) + ")."
	return s

def ListToPredicates(name,list):
	""" Converts a list of tuples to predicates with the given name. The predicate's name is the same 
		for each tuple and each tuple holds the arguments of the predicate.
		(Invalid signs and such are replaced befor creating the problog code. )
			ex.: name="double" list=[(1,2),(2,4),...] -> double(1,2). double(2,4). ...
 	"""
	s = ""
	for item in list:
		s += Predicate(name,item) + "\n"
	return s

def DictToPredicates(dictionary):
	""" Converts a dictionary of lists to predicates. The dictionary's keys match the predicate names.
		Each key holds a list of tuples, which in turn hold the arguments of the predicate. 
		(Invalid signs and such are replaced befor creating the problog code. )
		  	ex.: dict = {"double":[(1,2),(2,4),...], "triple":[(1,3),(2,6),...], ...}
			  -> double(1,2). double(2,4). ... triple(1,3). triple(2,6). ... ...
 	"""
	s = ""
	for key in dictionary:
		s += ListToPredicates(key,dictionary[key])
	return s

def Evidence(name,args,bool):
	""" Creates evidence based on a given predicate name, its arguments and whether the evidence is true or false.
		(Invalid signs and such are replaced befor creating the problog code. )
			ex.: name="double" args=(1,2) bool="True" -> evidence(double(1,2),true).
 	"""
	s = "evidence(" + (Predicate(name,args))[:-1] + "," + bool + ")." 
	return s

def ListToEvidence(name,list):
	""" Converts a list of tuples to evidence. The predicate's name is the same for each tuple and each tuple 
		holds the arguments of the predicate and (as last element) whether the evidence is true or false
		(Invalid signs and such are replaced befor creating the problog code. )
			ex.: name="double" list=[(1,2,true),(1,3,false),...] 
			  -> evidence(double(1,2),true). evidence(double(1,3),false). ...
 	"""
	s = ""
	for item in list:
		s += Evidence(name,item[:-1],item[-1]) + "\n"
	return s

def DictToEvidence(dictionary):
	""" Converts a dictionary of lists to evidence. The dictionary's keys match the predicate names.
		Each key holds a list of tuples, which in turn hold the arguments of the predicate and (as 
		last element) whether the evidence is true or false
		(Invalid signs and such are replaced befor creating the problog code. )
			ex.: dict = {"double"=[(1,2,true),(1,3,false),...], "triple":[(1,3,true),...], ...}
 			  -> evidence(double(1,2),true). evidence(double(1,3),false). ... evidence(triple(1,3),true). ... ...
 	"""
	s = ""
	for key in dictionary:
		s += ListToEvidence(key,dictionary[key])
	return s

# ----------------------------------------------------------------------------------------------------

# PROBLOG SYNTAX

def ReplaceInvalidSigns(s):
	""" Replace all signs that are not allowed by Problog.
	"""
	for i in invalidSigns:
		if i in s:
			s = s.replace(i,"_")
	return s

def StringToProblog(s):
	""" Convert a (camel case) string to Problog syntax.
	"""
	s = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', s)
	s = re.sub('([a-z0-9])([A-Z])', r'\1_\2', s).lower()
	s = ReplaceInvalidSigns(s)
	return s

def StringToProblog2(s):
	""" Convert a string to Problog syntax without using underscores.
	"""
	s = s.lower()
	s = ReplaceInvalidSigns(s)
	return s

# ----------------------------------------------------------------------------------------------------

# PROBLOG HEADERS AND COMMENT

def AddHeader(openFile,filename,authors,project):
	""" Add a header to an open problog file. The header is based on the filename, 
		the authors and eventual a project name.
	"""
	openFile.write(Header(filename,authors,project))
	return

def Header(filename, authors, project=None):
	""" Return a header in problog comment containing the filename, the authors and eventual a project name.
	"""
	minComment = 45
	items = list()

	if project is not None:							# create comment for project name
		items.append("% Project:  " + project)

	items.append("% Filename: " + filename)			# create comment for filename

	if len(authors) == 1:							# create comment for authors
		items.append("% Authors:  " + authors[0])
	else:
		items.append("% Authors:  " + ", ".join(authors[:-1]) + " and "+ authors[-1])

	l = max([len(i) for i in items])				# adjust all comments to the same length
	l = max(l,minComment)
	for x,i in enumerate(items):
		items[x] = i + (l-len(i))*" " + " %\n"
	line = "%" + l*"%" + "%\n"
	return line + "".join(items) + line + "\n"
	
def AddTitle(openFile, title):
	""" Add a title to an open problog file.
	"""
	openFile.write(Title(title))
	return

def Title(title):
	""" Return a title in problog comment.
	"""
	minComment = 30
	s1 = "% " + title + "\n"
	l = max(len(s1),minComment)
	s0 = "%" + l*"-" + "\n"
	return s0 + s1 + s0 + "\n"

# ****************************************************************************************************

def main():
	print("Test comments")
	print("Header:")
	print(HeaderString("example.prob", ["jos","jef","jul"], "example project"))
	print("Title:")
	print(TitleString("This is a title"))
	
if __name__ == "__main__":									
	main()

