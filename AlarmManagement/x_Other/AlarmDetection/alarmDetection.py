""" E'XperT project - alarmDetection.py: Detecting all known ID's (from component files), and regular expressions representing ID's, in alarm messages (from alarm files). 
"""
__author__ = "Shana Van Dessel, and Bram Aerts"

import re	
import glob
import copy

inputFileComponents  = "../../Data/Components/components.csv"
inputFileAlarms  = "../../Data/Alarms/alarms.csv" 
outputFile = "alarmsResult.csv"
outputFileUnknown = "alarmsUnknown.csv"

types = [   '62','ANT', 'Collector','HV','K','Mineraalleiding',
			'P','ST','Tankleiding','XV','ZST']

categs = {	'Ant': ['ANT'], 
		   #'Collector': ['Collector'],
		   #'K': ['K'], 	
		 	'Klep': ['XV','HV'], 
		    'Leiding': ['Mineraalleiding','Tankleiding'],
			'Pomp': ['P','Pomp','Membraampomp'], 
			'Steiger': ['ST','ZST','62']
}

regexs = [ 	r'\b6[0-9]{2}[A-Z]?\b',				# 62x  vb. 623 , 625B
			r'\bANT_[TS]?[0-9]+(_[0-9]+)+\b',	# ANT  vb. ANT_0050_02_1 , ANT_S0050_01
			r'\bCollector[A-Z]?_[A-Z0-9]+\b', 	# Col. vb. CollectorE_S , Collector_ST623
			r'\bHV_[A-Z0-9,/]+(_[A-Z0-9]+)+\b',	# HV   vb. HV_2450_1_A , HV_L3/L12_623_003
			r'\bK[0-9]\b',						# K    vb. K3
			r'\bMineraalleiding_[0-9]+\b',		# Min. vb. Mineraalleiding_12
			r'\bP[0-9]+[A-Z]?\b',				# P    vb. P1 , P105 , P108A
			r'\bST[0-9]+[A-Z]*(_[A-Z])*\b',		# ST   vb. ST623 , ST625A , ST625BCDE_D
			r'\bTankleiding_T[0-9]+_[0-9]\b',	# Tan. vb. Tankleiding_T2450_1
			r'\bXV(_[A-Z0-9]+)*\b',				# XV   vb. XV_LA23_001 , XV_L8_A_ZST1_001
			r'\bZST[0-9]+\b'					# ZST  vb. ZST1
] 
reTypes = dict((x,y) for x,y in zip(regexs,types))		

# ----------------------------------------------------------------------------------
# Gather data from files
# ----------------------------------------------------------------------------------

def getData(fsrc,field=0,unique=True):
	""" get a particular column/field from a csv file
	Arg: fsrc = csv input file, 
		 field = particular column from file, 
		 unique = only collect unique elements (or every appearance)
	Ret: list of collected elements
	"""
	data = list()	
	skip = 0
	with open(fsrc,"r") as f:
		for line in f:
			allParts = list()
			parts = line.rstrip().split(',')		# split on ','
			for part in parts:						# split on ';'
				[allParts.append(p) for p in part.split(';')]
			a = allParts[field].replace('-','_')	# get specific field and convert '-' to '_'
			if a == '' or ('?' in a):				# empty line or unknown
				skip +=1
				continue
			if unique and (a in data):				# double line
				skip +=1
				continue
			data.append(a)			
	print('\t # elements: \t{0:6d}/{1:6d} (skipped: {2})').format(len(data), len(data)+skip,skip)
	return data

def getComponents(fsrc):
	""" Collect component id's from csv file 
	Arg: fsrc = component file
	Ret: list of collected elements
	"""
	print '\nComponents:'
	return getData(fsrc)

def getAlarms(fsrc, unique=True):
	""" Collect (different) alarm messages from csv file
	Arg: fsrc = alarm file, unique = only collect unique elements (or every appearance)
	Ret: list of collected elements
	"""
	print '\nAlarms:'
	return getData(fsrc, 2, unique)
 
# ----------------------------------------------------------------------------------
# Match (regex) component ID's on alarm messages
# ----------------------------------------------------------------------------------

def getType(comp):
	""" Get the type of a component 
	"""
	global types
	cType = ''
	for t in types:
		if comp[:len(t)] == t and len(cType)<=len(comp):
					cType = t
	if len(cType)==0:
		cType = 'Onbekend'		
	#print("Type of "+comp+" is '"+cType+"'")
	return cType

def getReType(comp):
	""" Get the type of a regular expression
	"""
	global reTypes
	if comp in reTypes:
	#	print("Type of "+comp+" is '"+reTypes[comp]+"'")
		return reTypes[comp]
	else:
	#	print("Type of "+comp+" is 'onbekend'.")
		return 'Onbekend'		

def getCategory(comp):
	""" Get the category of a component 
	"""
	global categs
	cItem = ''
	cCategory = ''
	for category in categs:
		for item in categs[category]:
			if comp[:len(item)] == item and len(cItem)<=len(comp):
					cItem = item
					cCategory = category
	if len(cCategory)==0:
		cCategory = 'Onbekend'		
	#print ("Type of", comp, "is", "'"+cCategory+"'")
	return cCategory

def regexMatch(regex,alarm):
	""" test whether the regular expression matches a word in the alarm message
	"""
	return re.search(regex, alarm, re.I|re.M)

def stringMatch(string,alarm):
	""" test whether the string matches part of the alarm message
	"""
	return  string in alarm

def exactMatch(string,alarm):
	""" test whether the string matches a word in the alarm message 
	"""
	return ((' '+string+' ' in alarm)	
			or (':'+string+' ' in alarm)	
			or (string+':' in alarm and string[0] == alarm[0])		
			or (string+' ' in alarm and string[0] == alarm[0])		
			or (' '+string in alarm and string[-1] == alarm[-1]))

def findComponents(ids,msg,exact=True,regex=False):
	""" Match component ID's on alarm messages
 	Arg: ids = list of all component id's, 
		 msg = list of all alarm messages, 
		 exact = find an exact match (or just a match)
		 regex = use regex's to match instead of components
	Ret: dictionary: 
		 ['ok'] = all alarms in which a component ID was found, 
		 ['nok'] = all alarms in which no component ID was found, 
		 ['id'] = all alarms in which this component ID was found, 
		 ['msg'] = all component ID's that were found in this alarm, 
		 ['type'] = all alarms in which this type of component was found,
	"""
	global types
	found = list()								# all matched messages
	unfound = list()							# all mismatched messages

	foundPerId = dict()							# all matched messages per ID
	if not regex: foundPerId = dict((i,[]) for i in ids)	
	foundPerMsg = dict((m,[]) for m in msg)		# all matched ID's per message
	foundPerType = dict((t,[]) for t in types)	# all matched ID's per type
	foundPerType['Rest']=[]
	
	for m in msg:
		for i in ids:
			if regex and regexMatch(i,m): 		# match a regex
				foundPerMsg[m].append(i)
				foundPerType[getReType(i)].append(m)

			elif((not exact and stringMatch(i,m))	# match (part of) a string
				   or (exact and exactMatch(i,m))):	# match an exact string
				foundPerMsg[m].append(i)
				foundPerId[i].append(m)
				foundPerType[getType(i)].append(m)

		if not foundPerMsg[m] == []:
			found.append(m)						# list of all found messages
		else:
			unfound.append(m)					# list of all unfound messages

	findings = dict()
	findings['ok']=found
	findings['nok']=unfound
	findings['id']=foundPerId
	findings['msg']=foundPerMsg
	findings['type']=foundPerType
	return findings

def findComponentsInAlarms(fcomp,falarm,unique=True,exact=True,regex=False):
	""" Search for component ID's (from file) in alarm messages (from other file).
 	Arg: fcomp = component file, 
		 falarm = alarm file, 
		 unique = go thru unique alarms (or all alarms in file)
		 exact = find an exact match (or just a match)
		 regex = use regex's to match instead of components
	Ret: dictionary: 
		 ['ok'] = all alarms in which a component ID was found, 
		 ['nok'] = all alarms in which no component ID was found, 
		 ['id'] = all alarms in which this component ID was found, 
		 ['msg'] = all component ID's that were found in this alarm, 
		 ['type'] = all alarms in which this type of component was found,
	"""
	if not regex: 
		components = getComponents(fcomp)			# get list of all different components from file
	else:
		global regexs								# use list of regex's instead
		components = regexs
	alarms = getAlarms(falarm,unique)				# get list of all (different) alarms from file
	findings = findComponents(components,alarms,exact,regex)	# search for components in alarms
	return findings

# ----------------------------------------------------------------------------------
# Print results (to screen/file)
# ----------------------------------------------------------------------------------

def printAllResults(found,regex=False):
	""" Print the results 
	Arg: found = dictionary with all data,regex = can't print per ID
	"""
 	aFnd, aTot = printResults(found)
	printTable('type',found['type'],aFnd,aTot)	# COMPONENT TYPE
	if not regex:
		printTable('id',found['id'],aFnd,aTot)	# COMPONENT ID
	return

def printResults(found):
	""" Print the (most basic) results
	Arg: found = dictionary with all data,
	"""
	aFnd = len(found['ok'])			# number of found messages
	aTot = aFnd + len(found['nok'])	# total number of messages
	print('\nStatistics:')
	print('\t # found: \t{0:6d}/{1:6d} ({2:4.2f}%)'.format(aFnd, aTot, 100*float(aFnd)/aTot))
	print('\t # not found: \t{0:6d}/{1:6d} ({2:4.2f}%)'.format(len(found['nok']),aTot,100*float(len(found['nok']))/aTot))
	return aFnd, aTot

def printTable(name,found_,aFnd,aTot):
	""" Print a table of the results per element
	Arg: found = dictionary with the elements to print, aFnd = number of found alarms, aTot = all alarms
	"""
	pLine = dict()								 # lines
	pLine['F'] = '\n\t '+61*'-'					 # -first line
	pLine['L'] = '\t '+61*'-'+'\n'				 # -last line
	pLine['O'] = '\t|'+61*'_'+'|'				 # -outer line
	pLine['I'] = '\t|'+61*'-'+'|'				 # -inner line

	print(pLine['F'])	
	print('\t| {:60s}|'.format('Per component ' + name.upper()))
	print(pLine['I'])
	print('\t| {0:20s} | {1:10s} | {2:10s} | {3:10s} |'.format('Component '+name,'#Found','%Found','%Total'))
	print('\t| {0:20s} | {1:10s} | {2:10s} | {3:10s} |'.format('','','(/'+str(aFnd)+')','(/'+str(aTot)+')'))
	print(pLine['I'])
	print('\t| {0:20s} | {1:10d} | {2:9.2f}% | {3:9.2f}% |'.format('ALL',aFnd,100,(100*float(aFnd)/aTot)))
	print(pLine['I'])
	for key in sorted(found_):
		aKey = len(found_[key])
		if not aKey == 0:
			print('\t| {0:20s} | {1:10d} | {2:9.2f}% | {3:9.2f}% |'.format(key,aKey,(100*float(aKey)/aFnd),(100*float(aKey)/aTot)))
	print(pLine['L'])	
	return 
	
def writeStatistics(fdst,found,unique,exact,regex=False):
	""" Write some stats to a csv file
	Arg: fdst = csv output file,
		 found = dictionary with all data,
		 new = start new file (or append to old file),
		 unique = go thru unique alarms (or all alarms in file),
		 exact = find an exact match (or just a match)
	"""
	aFnd = len(found['ok'])			# number of found messages
	aTot = aFnd + len(found['nok']) # total number of messages
	
	with open(fdst,'w') as f:
		f.write("Statistieken omtrent het herkennen van component ID's in alarmberichten")
		f.write('\n')	
		f.write("\nCase: {} component ID's - {} alarmen."
		  .format(('EXACTE' if exact else 'GEDEELTELIJKE'),('UNIEKE' if unique else 'ALLE')))
		f.write('\nAantal {}alarmen,{}'
		  .format(('unieke ' if unique else ''), aTot))
		f.write('\nAantal {}alarmen waarin minstens een {}component herkend werd,{}'
		  .format(('unieke ' if unique else ''),('deel van een ' if not exact else ''), aFnd))
		f.write('\n')
		f.write('\nOverzicht: Per component type.')
		f.write('\nComponent type,Aantal keer gevonden,Percentage van het totaal aantal gevonden alarmen,'
		  +'Percentage van het totaal aantal alarmen')
		for key in sorted(found['type']):
			aKey=len(found['type'][key])
			#f.write('\n{},{},{:4.4}%,{:4.4}%'.format(key,aKey,(100*float(aKey)/aFnd),(100*float(aKey)/aTot)))
			f.write('\n{},{},{},{}'.format(key,aKey,float(aKey)/aFnd,float(aKey)/aTot))
		f.write('\n')
		f.write('\nOverzicht: Per component ID.')
		f.write('\nComponent ID,Aantal keer gevonden,Percentage van het totaal aantal gevonden alarmen,'
		  +'Percentage van het totaal aantal alarmen')
		for key in sorted(found['id']):
			aKey=len(found['id'][key])
			#f.write('\n{},{},{:4.4}%,{:4.4}%'.format(key,aKey,(100*float(aKey)/aFnd),(100*float(aKey)/aTot)))
			f.write('\n{},{},{},{}'.format(key,aKey,float(aKey)/aFnd,float(aKey)/aTot))
		f.write('\n')
	return

def generateFilename(defFile,add):
	newFile = defFile.split('.')[0]+add+'.'+defFile.split('.')[1]
	#print('{} --> {}.').format(defFile, newFile)
	return newFile

# ----------------------------------------------------------------------------------

def main():
	cases = [1,2,3,4]
	x_print = True		# print results on screen
	x_write = False		# write results to file

	for case in cases:								# find component id's in alarm messages
		print('\n----------------------------------')
		if case == 1:
			print("CASE 1: UNIQUE ALARMS & EXACT ID'S")
			options = [True,True,False]				# options 0:unique alarm? 1:exact id? 2:use regex?
	
		elif case == 2:
			print("CASE 2: ALL ALARMS & EXACT ID'S")
			options = [False,True,False]

		elif case == 3:
			print("CASE 3: UNIQUE ALARMS & REGEX ID'S")
			options = [True,True,True]
	
		elif case == 4:
			print("CASE 4: ALL ALARMS & REGEX ID'S")
			options = [False,True,True]

		elif case == 5:
			print("CASE 5: UNIQUE ALARMS & PARTIAL ID'S")
			options = [True,False,False]

		elif case == 6:
			print("CASE 6: ALL ALARMS & PARTIAL ID'S")
			options = [False,False,False]
		print('----------------------------------')
		
		found = findComponentsInAlarms(inputFileComponents,inputFileAlarms,
				 unique=options[0],exact=options[1],regex=options[2])

		if x_write:
			gFile = generateFilename(outputFile,'_case'+str(case))
			writeStatistics(gFile,found,unique=options[0],exact=options[1],regex=options[2])
			gFile = generateFilename(outputFileUnknown,'_case'+str(case))
			with open (gFile,"w") as f:
				f.write("Lijst van onbekende alarmen in case "+str(case)+":\n")
				for i in sorted(found['nok']):
					f.write(i+'\n')
					
		if x_print:
			printAllResults(found,regex=options[2])
		else:
			printResults(found)


	print('----------------------------------')

if __name__ == "__main__":
    main()
