"""
	mergeFiles.py: Merge several files into one file.
"""
__author__ = "Shana Van Dessel"
	
import glob
PRINTX = True #False

componentFiles = "../../Data/Components/*.csv"
newComponentFile  = "../../Data/Components/components.csv"
alarmFiles = "../../Data/Alarms/Control_Alarm_[0-9]*.csv" 
newAlarmFile  = "../../Data/Alarms/alarms.csv" 

def mergeFiles(sourceFiles,destinationFile):
	""" Merge several files into one file 
	"""
	skipped = 0;
	fsrc = getFiles(sourceFiles)
	printX('merging files:')

	with open(destinationFile, 'w') as fdst:									# Open destination file
		for fnext in fsrc:
			if fnext == destinationFile:										# Don't merge destination file itself
				skipped +=1
				continue
			printX(fnext)
			with open(fnext) as f:												# Open source file
				f.readline()
				for line in f:
					if all(map(lambda x: x is '', line.rstrip().split(','))):	# Skip empty lines
						continue
					fdst.write(line)
	printX('merged '+str(len(fsrc)-skipped)+' files, new file:', destinationFile, )
	return None

def getFiles(path):
	""" Get (all) file(s) in a directory by the pathname ("path\filename.extention" or "path\*.*")
	"""
	return glob.glob(path)

def printX(*strings):
	global PRINTX
	if not PRINTX:
		return None
	for s in strings:
		print s
	return None

def main():
	mergeFiles(componentFiles,newComponentFile)	# merge component files
	mergeFiles(alarmFiles,newAlarmFile)			# merge alarm files
	
if __name__ == "__main__":
    main()
