""" E'XperT project - alarmPrediction.py: Predict the following alarm(s) based on sequences of previous alarms.
"""
__author__ = "Shana Van Dessel"

#! /usr/bin/env python

# Includes
from   __future__ import print_function
import sys
import logging 	as lg
from   ppmg		import PPMG

# Files
fin  = 'alarm_testfile.csv'
fout = 'tree.dot'

# Defines
LOG = {'debug': lg.DEBUG,'info': lg.INFO,'warning': .WARNING,'error': lg.ERROR,'critical': lg.CRITICAL}
DEPTH   = 6
CONTEXT = 2

# Globals
lenline = 0		# length of a csv line
lenalarm = 0 	# max length of an alarm (name)

# FUNCTIONS
def getData(fin, start=0):
	global lenline
	global lenalarm

	data = list()
	with open(fin,"r") as f:	
		if start == 0:
			lenline  = len(f.readline())	# get length
			start    = 1					# toss line
		lg.debug('Start reading from line ' + str(start) + '\n')
		start = start * lenline
		f.seek(start)
		for line in f:
			cells = line.split(',')
			if cells[3] == 'ON':
				data.append(cells[2])
				if len(cells[2]) > lenalarm:
					lenalarm = len(cells[2])
		lg.debug('Alarms added: \n' + str(data) + '\n')
	return data

def getPpmg(depth,data):
	ppmg = PPMG(depth=depth)
	ppmg.learn_sequence(data)
	lg.debug('Graph nodes: \n' + str(ppmg.graph.nodes())+ '\n')
	return ppmg

def getPredictions(ppmg,data,context):
	try:
		predictions = ppmg.explore_probabilities(context)
		lg.info('Prediction: \n' + str(predictions)+ '\n')

	except ZeroDivisionError:
		lg.error('Not able to make a prediction, no link found.\n')
		predictions = None

	return predictions

def getBestPrediction(ppmg,data,context):
	predictions = getPredictions(ppmg,data,context)
	if predictions == None:
		return None 
	best = max(predictions)
	best = [best,predictions[best]]	
	return best

def printPredictions(predictions):
	for p in predictions:
		printPrediction(p, predictions[p])
	print('')
	return None

def printPrediction(key,value):
	global lenalarm
	print ('{alarm: <{space}} : {chance: 4.2f}%'.format(alarm=key, space=lenalarm, chance=value*100))
	return None

def getTree(fout,ppmg):
    with open(fout, 'w') as f:
        print(ppmg.to_dot(), file=f)
	return None

def main(argv):
	if len(sys.argv) > 1: 				
   	 	loglevel = LOG.get(sys.argv[1], lg.NOTSET)
		lg.basicConfig(level=loglevel)
	else:
		lg.basicConfig(level=lg.CRITICAL)

	data = list()
	graph = None

#	while(1):
#		print 'New'
	start = len(data)
	data.extend(getData(fin,start))
	ppmg = getPpmg(DEPTH,data)
	getTree(fout,ppmg)

	if CONTEXT < DEPTH: 
		lencontext = len(data)-CONTEXT
	else:
		lencontext = len(data)-(DEPTH-1)
	context = data[lencontext:]

	preds = getPredictions(ppmg,data,context)	
	print('PREDICTION:')
	printPredictions(preds)
	pred = getBestPrediction(ppmg,data,context)
	print ('MOST LIKELY TO HAPPEN NEXT:\n' + pred[0] + '\n')
	#printPrediction(pred[0],pred[1])

#		sleep(60)

if __name__ == '__main__':
    main(sys.argv[1:])

