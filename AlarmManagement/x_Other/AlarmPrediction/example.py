#! /usr/bin/env python3
"""
Example of how to use PPM-G.

Copyright (c) 2015 Wannes Meert, KU Leuven. All rights reserved.
This code is part of the PPM-G distribution.
"""
from __future__ import print_function

import os
import sys
import networkx as nx
from itertools import combinations, permutations

from ppmg import PPMG


def example1():
    """Build a PPM-G model for the sequence "abracadabra"."""
    data = list("abracadabra")
    # We do not specify a graph, PPMG will assume an alphabet where all
    # possible sequences are allowed.
    ppmg = PPMG(depth=3)
    ppmg.learn_sequence(data)
    with open('ppmg.dot', 'w') as ofile:
        print(ppmg.to_dot(), file=ofile)

    print('P(d|ra) = {} (should be 0.07142)'.format(ppmg.probability('d', list('ra'))))
    print('P(r|ab) = {}'.format(ppmg.probability('r', list('ab'))))

    print('P(.|ra) = {}'.format(ppmg.explore_probabilities(list('ra'))))


def example2():
    """Build a PPM-G model for the sequence "abracadabra"."""
    data = list("abracadabra")
    # We build the underlying graph data structure manually
    graph = nx.Graph()
    # All combinations are possible
    for b,e in combinations(set(data), 2):
        print('Add edge: {}--{}'.format(b,e))
        graph.add_edge(b,e)
    ppmg = PPMG(depth=3, graph=graph)
    with open('graph.dot', 'w') as ofile:
        print(nx.to_agraph(ppmg.graph), file=ofile)
    ppmg.learn_sequence(data)


def main(argv):
    example1()
    # example2()


if __name__ == '__main__':
    main(sys.argv[1:])
