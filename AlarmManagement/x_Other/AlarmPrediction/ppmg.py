"""
ppmg.py

Prediction by Partial Match - Graph is a variation of the PPM-C algorithm [1]
that assumes a graph underneath instead of an alphabet. This takes into
account that in a graph not every node is reachable from every other node. This
is what the PPM-C algorithm does to compute an escape but in environments with
large branching factors the number of seen pairs is potentially negligible
compared to the not observed pairs.

The graph expresses a constraints on the possible sequences that are expected
to exist (it is not enforced in the current version, but the escape is not
well defined in such cases).

[1] Ron Begleiter, Ran El-Yaniv, and Golan Yona. "On Prediction Using Variable
    Order Markov Models". In: Journal of Artificial Intelligence Research 22
    (2004), pp. 385-421.

Requirements:
- Python 2 or 3
- NetworkX

Copyright (c) 2015 Wannes Meert, KU Leuven. All rights reserved.
This code is part of the PPM-G distribution.
"""
import operator
import networkx as nx
import json


class PPMG():
    def __init__(self, depth=4, graph=None):
        self.root = TreeNode('^', 0)
        self.depth = depth
        self.autofill_graph = False
        if graph is None:
            self.graph = nx.Graph()
            self.autofill_graph = True
        else:
            self.graph = graph

        # Learn also last items in sequence that are closer than depth to end.
        # This is useful if every possible node should also be available in
        # the root level
        self.learn_until_end = False

    def add_letters(self, letters):
            alphabet = set(letters)
            alphabet.difference_update(self.graph.nodes())
            alphabet = list(alphabet)
            #print('Automatically adding letters: {}'.format(alphabet))
            if len(self.graph.nodes()) == 0:
				self.graph.add_node(alphabet[0])
            for letter in alphabet:
                for other in self.graph.nodes():
                    if letter != other:
                        #print('Add edge {}--{}'.format(letter, other))
                        self.graph.add_edge(letter, other)

    def learn_sequence(self, sequence):
        """Incrementally learn a variable order n-gram from the given sequence.
        This method should be called for all sequences available, the results
        are incrementally merged into the current model.
        """
        if self.autofill_graph:
            self.add_letters(sequence)
        r = len(sequence)
        if not self.learn_until_end:
            r -= self.depth-1
        for start in range(r):
            self.root.add_sequence(sequence[start:], self.depth)

    def probability(self, symbol, context):
        """P(symbol | context)
        :param symbol:
        :param context: List of history/context symbols
        :return: Probability
        """
        return self.root.calculateGlobalChance(context, symbol, self.graph)


    def explore_probabilities(self, context, normalize=False):
        """Iterate over all possible next symbols s and return P(s | context)
        """
        return self.root.exploreChances(context, self.graph, self.root, normalize)


    def to_dot(self):
        dot = 'digraph ppmg {\nrankdir=LR;\n'
        dot += self.root.to_dot()
        dot += '}\n'
        return dot


    def to_ascii(self):
        ascii = ''
        if self.root is not None:
            ascii += '{}\n'.format(self.root.name)
            ascii += self.root.to_ascii()
        return ascii


    def to_json(self):
        nodes = self.root.to_list()
        return json.dumps(nodes, indent=2)


    def from_json(self, data):
        nodes = json.loads(data)
        nodes_dict = dict()
        for hash, name, amount, depth, parent in nodes:
            node = TreeNode(name, amount, depth)
            node.hash = hash
            nodes_dict[hash] = node
            nodes_dict[parent].add_child(node)


    def __str__(self):
        if self.root is None:
            return ''
        else:
            return self.to_ascii()


num_nodes = 0

class TreeNode(object):
    """A tree consists of TreeNodes.
    """

    def __init__(self, name, amount=0, depth=0, children=None):
        global num_nodes
        self.name = name
        self.amount = amount
        self.depth = depth
        num_nodes += 1
        self.hash = num_nodes
        if children is None:
            self.children = list()
        else:
            self.children = children

    def to_ascii(self, level=0):
        """Export as ASCII format."""
        ascii = ''
        indent = '  '*level
        for child in self.children:
            ascii += '{}+-{} ({})\n'.format(indent, child.name, self.amount)
            ascii += child.to_ascii(level + 1)
        return ascii

    def to_dot(self):
        """Export as Graphviz dot format."""
        dot = '  {} [label="{}"];\n'.format(self.hash, self.name)
        for child in self.children:
            dot += '  {} -> {} [label="{}"];\n'.format(self.hash, child.hash, child.amount)
            dot += child.to_dot()
        return dot

    def to_list(self, parent):
        nodes = [(self.hash, self.name, self.amount, self.depth, parent.hash)]
        for child in self.children:
            nodes.extend(child.to_list(self))
        return nodes

    def add_child(self, child):
        """
        :param child: Node
        :return: None
        """
        self.children.append(child)


    def add_sequence(self, sequence, max_depth = 0):
        """
        Learn a new sequence
        :param sequence:
        :return:
        """
        if self.depth >= max_depth:
            return
        found = False
        for node in self.children:
            #print('{} == {}'.format(node.name, sequence[0]))
            if node.name == sequence[0]:
                found = True
                node.amount += 1
                if len(sequence) > 1:
                    node.add_sequence(sequence[1:], max_depth)
        if not found:
            child = TreeNode(sequence[0], 1, self.depth+1)
            self.add_child(child)
            if len(sequence) > 1:
                child.add_sequence(sequence[1:], max_depth)


	#return self.root.calculateGlobalChance(context, symbol, self.graph)
    def calculateGlobalChance(self, context, symbol, graph):
        return self.calculateChance(context, symbol, context, self, graph)

    def calculateChance(self, context, symbol, originalContext, tree, graph):
        found = False
        for child in self.children:
            if child.name == context[0]:
                found = True
                if len(context) > 1:
                    return child.calculateChance(context[1:],symbol, originalContext, tree, graph)
                else:
                    return child.checkChance(symbol, originalContext, tree, graph)
        if not found:
            if len(originalContext) > 1:
                return tree.calculateGlobalChance(originalContext[1:], symbol, graph)
            else:
                return float(1)/float(graph.degree(context[0]))


    def checkChance(self, symbol, originalContext, tree, graph):
        amountOfSymbol = 0
        totalAmount = 0
        found = False
        for child in self.children:
            if child.name == symbol:
                found = True
                amountOfSymbol = child.amount
            totalAmount = totalAmount + child.amount
        if found:
            result = float(amountOfSymbol)/float(len(self.children) + totalAmount)
            return result
        else:
            if len(originalContext) > 1:
                result = (float(len(self.children))/float(len(self.children) + totalAmount)) * tree.calculateGlobalChance(originalContext[1:], symbol, graph)
                return result
            else:
                result = (float(len(self.children))/float(len(self.children) + totalAmount)) * float(1)/float(graph.degree(self.name))
                return result

    def exploreChances(self, context, graph, tree, normalize=False):
        last = context[len(context)-1]
        # print('last = {}'.format(last))
        neighbors = graph.neighbors(last)
        nextNodes = list()
        absoluteChances = list()
        normalizedChances = dict()
        total = 0
        for nbr in neighbors:
            result = tree.calculateGlobalChance(context, nbr, graph)
            absoluteChances.append(result)
            total += result
            nextNodes.append(nbr)
        count = 0
        # print(absoluteChances)
        for nbr in neighbors:
            if normalize:
                normalizedChances[nbr] = float(absoluteChances[count])/float(total)
            else:
                normalizedChances[nbr] = float(absoluteChances[count])
            count += 1
        return normalizedChances


    def multipleExplore(self, context, amount, graph, tree):
        print('context: ')
        print(context)
        nextNodes = self.exploreChances(context, graph, tree)
        print(nextNodes)
        sorted_nextNodes = sorted(nextNodes.iteritems(), key = operator.itemgetter(1), reverse=True)
        amountChosen = len(sorted_nextNodes)
        chosen_nodes = list()
        chosen_nodes.append((context[-1], 1, context[-1]))
        for node in sorted_nextNodes:
            chosen_nodes.append((node[0], node[1], context[-1]))
        final_result = list()
        for node in sorted_nextNodes:
            final_result.append((node[0], node[1], context))
        index = 0
        while amountChosen < amount:
            (nextNode, nextChance, nextContext) = final_result[index]
            print('nextContext 1: ')
            print(nextContext)
            nextContextClone = list()
            for n in nextContext:
                nextContextClone.append(n)
            nextContextClone.append(nextNode),
            print('nextContext 2: ')
            print(nextContextClone)
            searchContext = nextContextClone[-(d-1):]
            nextNodes = self.exploreExtendedChances(searchContext, graph, nextChance)
            print(nextNodes)
            for node in nextNodes.items():
                final_result.append((node[0], node[1], nextContextClone))
            final_result = sorted(final_result, key = operator.itemgetter(1), reverse=True)
            index += 1
            (nextNode, nextChance, nextContext) = final_result[index]
            #print final_result
            found = False
            for node in chosen_nodes:
                # changed to take last node into account for direction
                if nextNode == node[0] and nextContext[-1] == node[3]:
                    found = True
            if not found:
                chosen_nodes.append((nextNode, nextChance, nextContext[-1]))
                amountChosen += 1
        coords = dict()
        index = 0
        for node in chosen_nodes:
            coords[index] = (node[0], node[1], node[2], node[3])
            index += 1
        return coords

    def exploreExtendedChances(self, context, graph, chance):
        neighbors = graph.neighbors(context[len(context)-1])
        nextNodes = list()
        absoluteChances = list()
        normalizedChances = dict()
        total = 0
        for nbr in neighbors:
            result = tree.calculateGlobalChance(context, nbr, graph)
            absoluteChances.append(result)
            total += result
            nextNodes.append(nbr)
        count = 0
        for nbr in neighbors:
            #print 'chance for ' + str(nbr)
            normalizedChances[nbr] = (float(absoluteChances[count])/float(total))*chance
            #print normalizedChances[nbr]
            count += 1

        return normalizedChances

    def exploreBigNodesTree(self, context, amount, graph, tree):
        #print 'context: '
        #print context
        nextNodes = self.exploreChancesInBigNodesTree(context, graph, tree)
        #print nextNodes
        sorted_nextNodes = sorted(nextNodes.iteritems(), key = operator.itemgetter(1), reverse=True)
        amountChosen = len(sorted_nextNodes)
        chosen_nodes = list()
        chosen_nodes.append((context[-1], 1, context[-1]))
        for node in sorted_nextNodes:
            chosen_nodes.append((node[0], node[1], context[-1]))
        final_result = list()
        for node in sorted_nextNodes:
            final_result.append((node[0], node[1], context))
        index = 0
        while amountChosen < amount and len(final_result) > 0:
            (nextNode, nextChance, nextContext) = final_result[index]
            #print 'nextContext 1: '
            #print nextContext
            nextContextClone = list()
            for n in nextContext:
                nextContextClone.append(n)
            nextContextClone.append(nextNode),
            #print 'nextContext 2: '
            #print nextContextClone
            searchContext = nextContextClone[-(d-1):]
            nextNodes = self.exploreExtendedChancesInBigNodesTree(searchContext, graph, nextChance, tree)
            #print "next Nodes"
            #print nextNodes.items()
            for node in nextNodes.items():
                found = False
                for resultNode in chosen_nodes:
                    #print resultNode[0]
                    if resultNode[0] == node[0]:
                        found = True
                if not found:
                    final_result.append((node[0], node[1], nextContextClone))
            final_result = sorted(final_result, key = operator.itemgetter(1), reverse=True)
            index += 1
            (nextNode, nextChance, nextContext) = final_result[index]
            #print final_result
            found = False
            for node in chosen_nodes:
                # changed to take last node into account for direction
                if nextNode == node[0] and nextContext[-1] == node[3]:
                    found = True
            if not found:
                chosen_nodes.append((nextNode, nextChance, nextContext[-1]))
                amountChosen += 1
        coords = dict()
        index = 0
        for node in chosen_nodes:
            coords[index] = (node[0], node[1], node[2], node[3])
            index += 1
        return coords

    def exploreChancesInBigNodesTree(self, context, graph, tree):
        neighbors = self.neighborInBigNodeTree(graph, context[len(context)-1])
        nextNodes = list()
        absoluteChances = list()
        normalizedChances = dict()
        total = 0
        for nbr in neighbors:
            result = tree.calculateGlobalChance(context, nbr, graph)
            absoluteChances.append(result)
            total += result
            nextNodes.append(nbr)
        count = 0
        for nbr in neighbors:
            #print 'chance for ' + str(nbr)
            normalizedChances[nbr] = float(absoluteChances[count])/float(total)
            #print normalizedChances[nbr]
            count += 1

        return normalizedChances

    def exploreExtendedChancesInBigNodesTree(self, context, graph, chance, tree):
        neighbors = self.neighborInBigNodeTree(graph, context[len(context)-1])
        nextNodes = list()
        absoluteChances = list()
        normalizedChances = dict()
        total = 0
        for nbr in neighbors:
            result = tree.calculateGlobalChance(context, nbr, graph)
            absoluteChances.append(result)
            total += result
            nextNodes.append(nbr)
        count = 0
        for nbr in neighbors:
            #print 'chance for ' + str(nbr)
            normalizedChances[nbr] = (float(absoluteChances[count])/float(total))*chance
            #print normalizedChances[nbr]
            count += 1

        return normalizedChances

    def neighborInBigNodeTree(self, graph, nodeID):
        try:
            if graph.degree(nodeID) > 2 or graph.degree(nodeID) == 1:
                return  graph.neighbors(nodeID)
            else:
                closeNeighbors = graph.neighbors(nodeID)
                neighbors = list()
                for nbr in closeNeighbors:
                    deg = graph.degree(nbr)
                    if deg > 2 or deg == 1:
                        neighbors.append(nbr)
                    else:
                        neighbors.append(self.checkEndOfPath(nodeID, nbr, graph))
                return neighbors
        except nx.NetworkXError as err:
            return list()

    def checkEndOfPath(self, prevNode, currentNode, graph):
        nbrs = graph.neighbors(currentNode)
        index = 0
        if nbrs[index] == prevNode:
            index = 1
        if graph.degree(nbrs[index]) == 2:
            return self.checkEndOfPath(currentNode, nbrs[index], graph)
        else:
            return currentNode
