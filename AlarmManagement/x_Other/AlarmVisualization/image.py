"""
	image.py: E'XperT project - Visualize the terminal based on the xml file #
"""
__author__ = "Shana Van Dessel"

# Imports
import sys
import math
from copy import copy
from itertools import combinations_with_replacement	
import logging as log
import xml.dom.minidom as dt
import xml.etree.ElementTree as et

# Files
f_model = 'model.xml'
f_image = 'model.xgml'

# Mapping
LOG   = { 'info': log.INFO, 	'debug':log.DEBUG,	'warning':log.WARNING,
		  'error':log.ERROR,	'critical':log.CRITICAL }

color = { 'white':  '#FFFFFF', 'black':   '#000000', 'grey': '#888888', 
          'red':    '#FF0000', 'green':   '#00FF00', 'blue': '#0000FF', 
	  	  'yellow': '#FFFF00', 'magenta': '#FF00FF', 'cyan': '#00FFFF' }
		  
font  = { 'name':  'Dialog',  'size':  '9' }

# Settings		
IMG = True	# use figures in stead of simple shapes
LAB = True	# give components a label
COL = False	# give components a color

# --------------------------------------------------------------------------------

# CLASSES
# Class Model
class Model(object):
	def __init__(self,grid):
		self.grid = 30
		self.tanks = Components()
		self.conns = Components()
		self.valves = Components()
		self.pumps = Components()
		self.quays = Components()
		self.lines = Components()	

#	# Make components
	def make(self,root):
		log.debug(' NEW ')				# Tanks and connections
		self.makeTanks(root)
		log.debug(' Tanks: \n\t' + str(self.tanks))
		log.debug(' Conns: \n\t' + str(self.conns))
		log.debug(' NEW ')				# Valves
		self.makeValves(root)		
		log.debug(' Valves: \n\t' + str(self.valves))
		log.debug(' NEW ')				# Pumps
		self.makePumps(root)	
		log.debug(' Pumps: \n\t' + str(self.pumps))
		log.debug(' NEW ')				# Quays
		self.makeQuays(root)
		log.debug(' Quays: \n\t' + str(self.quays))
		log.debug(' NEW ')				# Lines
		self.makePipelines(root)
		log.debug(' Lines: \n\t' + str(self.lines))
		return
		
	def makeTanks(self,root):
		tanks = root.find('equipment').find('Tanks')
		for t in tanks:
			tank = Tank(t.find('TechnicalId').text,0,0)
			self.tanks.addComponent(tank)			
			for tc in t.find('Connections'):
				tankconnection = TankConnection(tc.find('TechnicalId').text,0,0,t.find('TechnicalId').text)
				self.conns.addComponent(tankconnection)
		return

	def makeValves(self,root):
		valves = root.find('equipment').find('Valves')
		for v in valves:
			valve = Valve(v.find('TechnicalId').text,0,0)
			self.valves.addComponent(valve)
		return
	
	def makePumps(self,root):	
		pumps = root.find('equipment').find('Pumps')
		for p in pumps:
			pump = Pump(p.find('TechnicalId').text,0,0,
				    p.find('In').find('TechnicalId').text,-self.grid,0,
				    p.find('Out').find('TechnicalId').text,+self.grid,0)
			self.pumps.addComponent(pump)	
		return
	
	def makeQuays(self,root):		
		quays = root.find('equipment').find('ProductTransferConnections')
		for q in quays:
			quay = Quay(q.find('TechnicalId').text,0,0)
			self.quays.addComponent(quay)
		return
	
	def makePipelines(self,root):
		for p in root.find('pipelines'):
			line = Pipeline(p.find('TechnicalId').text)
			for c in p.find('Connections'):
					line.addConnection(c.find('TechnicalId').text)
			self.lines.addComponent(line)
		return
	
#	# Position components
	def position(self):
		log.debug(' SET ')				# Tanks and connections
		self.positionTanks()
		log.debug(' Tanks: \n\t' + str(self.tanks.window) + '\n\t' + str(self.tanks))
		log.debug(' Conns: \n\t' + str(self.conns))		
		log.debug(' SET ')				# Quays	
		self.positionQuays()
		log.debug(' Quays: \n\t' + str(self.quays.window) + '\n\t' + str(self.quays))	
		log.debug(' SET ')				# Pumps
		self.positionPumps()
		log.debug(' Pumps: \n\t' + str(self.pumps))
		log.debug(' SET ')				# Valves
		self.positionValves()	
		log.debug(' Valves: \n\t' + str(self.valves))	
		log.info('  ' + str(self))
		return
		
	def positionTanks(self):
		for i,t in enumerate(self.tanks.list): 		# i, tank
			t.x = self.ceilToGrid(t.w/2)
			t.y = self.ceilToGrid((t.h + self.grid)*i)
			if i == 0: 	self.tanks.newWindow(t.x,t.y,t.w,t.h)
			else:		self.tanks.extendWindow(t.x,t.y,t.w,t.h)		
			j=0
			for tc in self.conns.list:		# j, tank connection
				if tc.pid == t.id:
					tc.x = self.ceilToGrid(t.x + t.w/2)
					tc.y = self.floorToGrid(t.y + t.h*0.35 - self.grid*j)		
					j+=1
					
					if i == 0: 	self.conns.newWindow(tc.x,tc.y,tc.w,tc.h)
					else:		self.conns.extendWindow(tc.x,tc.y,tc.w,tc.h)
					
			self.tanks.addWindow(self.conns.window)
		return
		
	def positionQuays(self):
		for i,q in enumerate(self.quays.list):
			q.x = self.tanks.window.xmax - q.w/2
			#q.x = self.ceilToGrid(self.tanks.window.xmax - q.w/2)
			q.y = self.ceilToGrid(self.tanks.window.ymax + q.h/2 + self.grid)
			if i == 0: 	self.quays.newWindow(q.x,q.y,q.w,q.h)
			else:		self.quays.extendWindow(q.x,q.y,q.w,q.h)
		return
	
	def positionPumps(self):
		for i,p in enumerate(self.pumps.list):
			p.x = self.ceilToGrid(max(self.tanks.window.xmax, self.quays.window.xmax) + self.grid*12 + self.grid*3*i)
			p.input.x = p.input.x + p.x
			p.output.x = p.output.x + p.x
			p.y = self.ceilToGrid(max(self.tanks.window.ymax, self.quays.window.ymax) + self.grid*3)
			p.input.y = p.input.y + p.y
			p.output.y = p.output.y + p.y
		return
		
	def positionValves(self): 
		valves_prev = list()			# valves needed from previous iteration
		valves_next = list()			# valves needed for next iteration
		valves_todo = list()			# valves that aren't done yet
		lines_todo = list(self.lines.list)
		for x in self.valves.list:
			valves_todo.append(x.id)		
		log.debug(' Valves to do: ' + str(len(valves_todo)))

		# Valves for pumps
		pumpLines = dict() 			# keep track of the x-coordinates of the pipelines connecting pumps
		for p in self.pumps.list:	
			valves = list()
			for l in lines_todo:
				if(p.input.id in l.connections):			# line is connected to input pump
					if valves == []: valves = list(l.connections)
					else: 			 valves.extend(l.connections)
				if(p.output.id in l.connections):			# line is connected to output pump
					if valves == []: valves = list(l.connections)
					else: 			 valves.extend(l.connections)
			for v in valves:						# if the same valve appears twice in this list, 
				if valves.count(v) == 2:				# it means this one is connected to the pumps in- and output
					break
				      
			if valves.count(v) != 2:
				log.error('Something went terribly wrong!')
				continue
				
			v = self.valves.byId(valves[0])
			v.x = p.x
			v.y = min(self.tanks.window.ymin, self.quays.window.ymin) - self.grid*3
			for l in lines_todo:
				if(v.id in l.connections and p.input.id in l.connections):
					lin = l
					pumpLines[l] = p.input.x
				if(v.id in l.connections and p.output.id in l.connections):
					lout = l
					pumpLines[l] = p.output.x
			self.positionCombinatedValves(v,p.input.x,p.output.x,lin,lout)
			log.debug(' Valve set: ' + str(v))
			valves_next.append(v.id)
			
		valves_prev,valves_next,valves_todo = self.valvesState(valves_prev,valves_next,valves_todo)
		log.debug(' Valves to do: ' + str(len(valves_todo)))
		
		# Valves for tanks
		for i in [(tc,v,l) for tc in self.conns.list for v in self.valves.list for l in lines_todo
							      if (tc.id in l.connections and v.id in l.connections)]:
			i[1].x = i[0].x + self.grid*3		# i[1]: v(valve), i[0]: tc(tank connection)
			i[1].y = i[0].y
			log.debug(' Valve set: ' + str(i[1]) + '\t- ' + str(i[2]))
			valves_next.append(i[1].id)
			lines_todo.remove(i[2])			# i[2]: l(pipeline)
			
			self.positionCombinatedValves(i[1],i[1].x-self.grid,i[1].x+self.grid,i[2],None)
			
		valves_prev,valves_next,valves_todo = self.valvesState(valves_prev,valves_next,valves_todo)
		log.debug(' Valves to do: ' + str(len(valves_todo)))
		
		valves = list(combinations_with_replacement(valves_prev, 2))
		for i in [(vids[0],vids[1],l) for vids in valves for l in lines_todo
					    if vids[0] in l.connections and vids[1] in l.connections and not vids[0] == vids[1]]:
			vid = (list(set(i[2].connections) - set([i[0],i[1]])))[0]
			v = self.valves.byId(vid)
			v.x = self.valves.byId(i[0]).x + self.grid*3
			v.y = self.valves.byId(i[0]).y
			log.debug(' Valve set: ' + str(v) + '\t- ' + str(i[2]))
			#valves_prev.remove(i[0])
			#valves_prev.remove(i[1])
			valves_next.append(v.id)
			lines_todo.remove(i[2])
			
			self.positionCombinatedPipes(i[2],i[0],self.valves.byId(i[0]).output.id)
			self.positionCombinatedPipes(i[2],i[1],self.valves.byId(i[1]).output.id)
			self.positionCombinatedValves(v,v.x-self.grid,v.x+self.grid,i[2],None)
			  
		#if not valves_prev == []: 
		#	log.error(' Valves skipped: ' + str(valves_prev))
		valves_prev,valves_next,valves_todo = self.valvesState(valves_prev, valves_next, valves_todo)
		log.debug(' Valves to do: ' + str(len(valves_todo)))
		
		for i in [(v,l) for v in valves_prev for l in lines_todo if v in l.connections]:
			vids = (list(set(i[1].connections) - set([i[0]])))
			for vid in vids:
				v = self.valves.byId(vid)
				for j in [l for l in pumpLines if vid in l.connections]:
					#print 'l' , j.id
					#print 'v' , v.id
					v.x = pumpLines[l] - self.grid
					v.y = self.valves.byId(i[0]).y + self.grid
					
					self.positionCombinatedValves(v,v.x-self.grid,v.x+self.grid,i[1],j)
					v.input.y -= self.grid
					break
				      
				valves_next.append(v.id)
			
		valves_prev,valves_next,valves_todo = self.valvesState(valves_prev,valves_next,valves_todo)
		log.debug(' Valves to do: ' + str(len(valves_todo)))	
		
		# Valves for quays
		for i in [(q,v,l) for q in self.quays.list for v in self.valves.list for l in lines_todo
							if (q.id in l.connections and v.id in l.connections)]:
			i[1].x = i[0].x + i[0].w/2 + self.grid*3		# i[1]: v(valve), i[0]: q (quay)
			i[1].y = i[0].y
			log.debug(' Valve set: ' + str(i[1]) + '\t- ' + str(i[2]))
			valves_next.append(i[1].id)
			lines_todo.remove(i[2])
			self.positionCombinatedValves(i[1],i[1].x-self.grid,i[1].x+self.grid,i[2],None)
			
		valves_prev,valves_next,valves_todo = self.valvesState(valves_prev, valves_next, valves_todo)
		log.debug(' Valves to do: ' + str(len(valves_todo)))
		
		for i in [(v,l) for v in valves_prev for l in lines_todo if v in l.connections]:
			valves = (list(set(i[1].connections) - set([i[0]])))
			for j,vid in enumerate(valves):
				v = self.valves.byId(vid)
				v.x = self.valves.byId(i[0]).x + self.grid*3
				v.y = self.valves.byId(i[0]).y + self.grid*j
				log.debug(' Valve set: ' + str(v) + '\t- ' + str(i[1]))
				valves_next.append(v.id)
				
				self.positionCombinatedValves(v,v.x-self.grid,v.x+self.grid,i[1],None)
			self.positionCombinatedPipes(i[1],i[0],self.valves.byId(i[0]).output.id)
				 
			lines_todo.remove(i[1])		
		valves_prev,valves_next,valves_todo = self.valvesState(valves_prev, valves_next, valves_todo)
		log.debug(' Valves to do: ' + str(len(valves_todo)))	
		
		#print 'PREV' ,valves_prev
		#for i in [(v,l) for v in valves_prev for l in lines_todo if v in l.connections]:
		#	valves = (list(set(i[1].connections) - set([i[0]])))
		#	print valves
		#print 'TODO', valves_todo
		
		# Valves for matrix
		# XXX TODO XXX
		for x in valves_todo:
			self.valves.list.remove(self.valves.byId(x))
		return

	def positionCombinatedValves(self,v,xin,xout,lin=None,lout=None):
		v.input = Connector(v.id+'-p1',xin,v.y)			# create first access point
		v.inputLine = ConnectorLine(v.id+'-l1',v.input.id,v.id)
		v.output = Connector(v.id+'-p2',xout,v.y)		# create second access point	
		v.outputLine = ConnectorLine(v.id+'-l2',v.output.id,v.id)
		if lin is not None:
			self.positionCombinatedPipes(lin,v.id,v.input.id)	# reroute pipeline
		if lout is not None:
			self.positionCombinatedPipes(lout,v.id,v.output.id)	# reroute pipeline
		return
	
	def positionCombinatedPipes(self,l,old,new):			# reroute pipelines
		l.connections.remove(old)
		l.connections.append(new)
		return
	
#	# small methods and to string method	
	def valvesState(self,prev,next,todo):
		prev = list(next)
		next = []
		for x in prev: 
			todo.remove(x)
		return prev,next,todo
	      
	def ceilToGrid(self,a):
		return math.ceil(a/self.grid) * self.grid

	def floorToGrid(self,a):
		return math.floor(a/self.grid) * self.grid

	def __str__(self):
		string = self.__class__.__name__ + ': \n'
		string += '\t' + str(self.tanks) + '\n\t' + str(self.conns) + '\n\t' + str(self.valves) 
		string += '\n\t' + str(self.pumps) + '\n\t' + str(self.quays) + '\n\t' + str(self.lines)
		return string

# -----

# Class Components
class Components(object):
	def __init__(self):
		self.list = []
		self.window = Window()

	def byId(self,id):
		for i in self.list:
			if i.id == id:
				return i
		return
		
	def addComponent(self,comp):			# add a component to the list
		self.list.append(comp)
		#if not comp.__class__.__name__ == 'Pipeline': 
		#	self.extendWindow(comp.x,comp.y,comp.w,comp.h)
		#log.debug('New ' + str(comp))
		return
	
	def newWindow(self,x,y,w,h):			# set a new window
		self.window.newWindow(x-w/2,x+w/2,y-h/2,y+h/2)
		#log.debug('New ' + str(self.window))
		return
				
	def extendWindow(self,x,y,w,h):			# extend the old window
		self.window.extendWindow(x-w/2,x+w/2,y-h/2,y+h/2)
		#log.debug(str(self.window))
		return
		
	def addWindow(self, window):
		self.window.extendWindow(window.xmin,window.xmax,window.ymin,window.ymax)
		#log.debug(str(self.window))	
		return
		
	def __str__(self):
		string = self.__class__.__name__ +  ': \n'
		for x in self.list:
			string += '\t' + str(x) + '\n' 
		return string

# Class Window
class Window(object):
	def __init__(self):
		self.newWindow(0,0,0,0)
		
	def newWindow(self,xmin,xmax,ymin,ymax):		# set new coordinates
		self.xmin = xmin 
		self.xmax = xmax  
		self.ymin = ymin
		self.ymax = ymax
		return			
		
	def extendWindow(self,xmin,xmax,ymin,ymax):	# alter old coordinates
		if xmin < self.xmin: self.xmin = xmin 
		if xmax > self.xmax: self.xmax = xmax
		if ymin < self.ymin: self.ymin = ymin
		if ymax > self.ymax: self.ymax = ymax	
		return
			
	def __str__(self):
		string = self.__class__.__name__ +  ': \n\t'
		string += str(self.xmin) + ',' + str(self.xmax) + ',' + str(self.ymin) + ',' + str(self.ymax)
		return string	
	
# Class Component	
class Component(object):
	def __init__(self,id):
		self.id = id

	def __str__(self):
		return self.__class__.__name__ + ' ' + str(self.id)

# Class Pipeline
class Pipeline(Component):
	def __init__(self,id):
		super(Pipeline,self).__init__(id)
		self.connections = []

	def addConnection(self,c):
		self.connections.append(c)
		return
				
	def __str__(self):
		string = self.__class__.__name__ + ' ' + str(self.id) + ' (c=' 
		for i,x in enumerate(self.connections):
			if not i == 0: string += ',' 
			string += str(x) 
		string += ')'
		return string
		
# Class Equipment
class Equipment(Component):
	def __init__(self,id,x,y,w,h,shape,figure,line,fill):
		super(Equipment,self).__init__(id)
		self.x = x
		self.y = y
		self.w = w
		self.h = h
		self.shape = shape
		if figure is not None: self.figure = figure
		else: 				   self.figure = shape
		if line is not None: self.color = line
		else: 				  self.color = color['black']
		if fill is not None: self.fill = fill
		else: 				 self.fill = color['white']

	def __str__(self):
		return self.__class__.__name__ + ' ' + str(self.id) + ' (x=' + str(self.x) + ',y=' + str(self.y) + ')'

# Class Tank
class Tank(Equipment):
	def __init__(self,i,x,y):
		super(Tank,self).__init__(i,x,y,120,210,'rectangle','com.yworks.flowchart.dataBase',color['red'],None)

# Class TankConnection
class TankConnection(Equipment):
	def __init__(self,i,x,y,par):
		super(TankConnection,self).__init__(i,x,y,20,20,'rectangle','com.yworks.flowchart.directData',color['red'],None)
		self.pid = par
	
	def __str__(self):
		return self.__class__.__name__ + ' ' + str(self.id) + ' (x=' + str(self.x) + ',y=' + str(self.y) + ') (tank ' + str(self.pid) + ')'

# Class Quay		
class Quay(Equipment):
	def __init__(self,i,x,y):
		super(Quay,self).__init__(i,x,y,180,70,'rectangle','com.yworks.flowchart.directData',color['magenta'],None)

# Class Valve
class Valve(Equipment):
	def __init__(self,i,x,y):
		super(Valve,self).__init__(i,x,y,30,20,'triangle','com.yworks.flowchart.decision',color['green'],None)
		self.input = None
		self.output = None
		self.inputLine = None
		self.outputLine = None

# Class Pump
class Pump(Equipment):
	def __init__(self,i,x,y,ii,xi,yi,io,xo,yo):
		super(Pump,self).__init__(i,x,y,30,30,'ellipse','com.yworks.flowchart.start2',color['blue'],None)
		self.input = Connector(ii,xi,yi)
		self.output = Connector(io,xo,yo)
		self.inputLine = ConnectorLine(self.id+'-l1',self.input.id,self.id)
		self.outputLine = ConnectorLine(self.id+'-l1',self.output.id,self.id)
		
	def __str__(self):
		return self.__class__.__name__ + ' ' + str(self.id) + ' (x=' + str(self.x) + ',y=' + str(self.y) + ') (in=' + str(self.input.id) + ',out=' + str(self.output.id) + ')'
		
# Class Connector
class Connector(Component):
	def __init__(self,i,x,y):
		super(Connector,self).__init__(i)
		self.x = x
		self.y = y

	def __str__(self):
		return self.__class__.__name__ + ' ' + str(self.id) + ' (x=' + str(self.x) + ',y=' + str(self.y) + ')'

# Class ConnectorLine	      
class ConnectorLine(Component):
	def __init__(self,i,p1,p2):
		super(ConnectorLine,self).__init__(i)
		self.connections = [p1,p2]

	def __str__(self):
		return self.__class__.__name__ + ' ' + str(self.id) + ' (x=' + str(self.x) + ',y=' + str(self.y) + ')'
	
# --------------------------------------------------------------------------------

# Class Image
class Image(object):
	def __init__(self):
		self.xgml = self.xgml()
		self.graph = self.graph()
	
	# Make components
	def make(self,model):
		log.debug(' Draw tanks')		# Tanks and connections
		self.makeTanks(model.tanks.list,model.conns.list)
		log.debug(' Draw valves')		# Valves
		self.makeValves(model.valves.list)
		log.debug(' Draw pumps')		# Pumps
		self.makePumps(model.pumps.list)
		log.debug(' Draw quays')		# Quays
		self.makeQuays(model.quays.list)	
		log.debug(' Draw pipelines')		# Pipelines
		self.makePipelines(model.lines.list)
		
		# XXX TODO XXX
		a = Connector('A1',800,60)
		self.addPoint(a)
		y = Pipeline(None)
		y.addConnection('XV103')
		y.addConnection('A1')
		self.addPipeline(y)
		a = Connector('A2',800,300)
		self.addPoint(a)
		y = Pipeline(None)
		y.addConnection('XV203')
		y.addConnection('A2')
		self.addPipeline(y)
		a = Connector('A3',800,420)
		self.addPoint(a)
		y = Pipeline(None)
		y.addConnection('XV302')
		y.addConnection('A3')
		self.addPipeline(y)
		a = Connector('A4',800,450)
		self.addPoint(a)
		y = Pipeline(None)
		y.addConnection('XV303')
		y.addConnection('A4')
		self.addPipeline(y)
		
		return
	      
  	def makeEquipment(self,equipmentList):
		for x in equipmentList:
			self.addEquipment(x)
		return
	      
	def makeCombinedEquipment(self,equipmentList):
		for x in equipmentList:
			self.addEquipment(x)
			self.addPoint(x.input)
			self.addPoint(x.output)
			self.addPipeline(x.inputLine)
			self.addPipeline(x.outputLine)
		return 
	      
	def makeTanks(self,tanks,conns):
		self.makeEquipment(tanks)
		self.makeEquipment(conns)
		return 
	      
	def makeValves(self,valves):
		self.makeCombinedEquipment(valves)
		return 
	      
	def makePumps(self,pumps):	      
		self.makeCombinedEquipment(pumps)
		return
			
	def makeQuays(self,quays):			
		self.makeEquipment(quays)
		return
	      
	def makePipelines(self,pipes):		
		for x in pipes:	
			self.addPipeline(x)
		return
 
	def addEquipment(self, comp):				# add an equipment component to the image
		node = self.node(comp)
		self.graphics(node,comp)						
		if LAB and not comp.__class__.__name__ == 'Connector': self.labelGraphics(node,comp)
		return node
	
	def addPipeline(self, comp):				# add a pipeline component to the image
		edges = list()
		src = comp.connections[0]
		dsts = list(comp.connections)
		dsts.remove(src)
		for dst in dsts:
			edge = self.edge(comp, src,dst)
			self.graphics2(edge,comp,src,dst)
			if LAB and not comp.__class__.__name__ == 'ConnectorLine': self.labelGraphics(edge,comp)
			edges.append(edge)
		return edges
	
	def addPoint(self, comp):				# add a connector component (point) to the image
		node = self.node(comp)
		self.graphics3(node,comp)
		return node
		
#	# XGML tags
	def xgml(self):						# create xgml tag
		xgml = self.root('xgml')
		self.attribute(xgml,'Creator','String','yFiles')
		self.attribute(xgml,'Version','String','2.12')
		log.info('  xgml root created')
		return xgml
		
	def graph(self):					# create graph tag
		graph = self.section(self.xgml,'graph')			
		self.attribute(graph,'hierarchic','int','1')
		self.attribute(graph,'label','String',' ')
		self.attribute(graph,'directed','int','1')
		log.info('    graph tag added')
		return graph

	def node(self, comp):					# create node tag
		node = self.section(self.graph,'node')			
		self.attribute(node,'id','String', comp.id)
		log.info('\tnode tag added: ' + comp.id)
		return node

	def edge(self,comp,src,dst):				# create edge tag
		edge = self.section(self.graph,'edge')			
		self.attribute(edge,'id','String',comp.id)
		self.attribute(edge,'source','String',src)
		self.attribute(edge,'target','String',dst)
		log.info('\tedge tag added: ' + str(comp.id) + ' \t(from=' + src + ',to=' + dst + ')')
		return edge
		
	def graphics(self,node,comp): 	 			# create graphics tag (node)
		graphics = self.section(node,'graphics')
		self.attribute(graphics,'x','double',comp.x)
		self.attribute(graphics,'y','double',comp.y)
		self.attribute(graphics,'w','double',comp.w)
		self.attribute(graphics,'h','double',comp.h)
		if IMG: 
			self.attribute(graphics,'customconfiguration','String',comp.figure)
		else:	
			self.attribute(graphics,'type','String',comp.shape)
		if COL: 
			self.attribute(graphics,'fill','String',comp.fill)
			self.attribute(graphics,'outline','String',comp.color)		
		else:	
			self.attribute(graphics,'fill','String',color['white'])
			self.attribute(graphics,'outline','String',color['black'])
		log.debug('\t  graphics      tag added: ' + str(comp.id) + '(x=' + str(comp.x) + ',y=' + str(comp.y) + ')')
		return graphics

	def graphics2(self,edge,comp,src,dst):			# create graphics tag (edge)
		graphics = self.section(edge,'graphics')				
		if COL: self.attribute(graphics,'fill','String',comp.color)
		else:   self.attribute(graphics,'fill','String',color['black'])
		log.debug('\t  graphics      tag added: ' + str(comp.id) + '(f=' + str(src) + ',t=' + str(dst) + ')')
		return graphics
	
	def graphics3(self,node,comp): 	 			# create graphics tag (point)
		graphics = self.section(node,'graphics')
		self.attribute(graphics,'x','double',comp.x)
		self.attribute(graphics,'y','double',comp.y)
		self.attribute(graphics,'w','double',3)
		self.attribute(graphics,'h','double',3)
		self.attribute(graphics,'type','String','ellipse')
		self.attribute(graphics,'fill','String',color['black'])
		self.attribute(graphics,'outline','String',color['black'])		
		log.debug('\t  graphics      tag added: ' + str(comp.id) + '(x=' + str(comp.x) + ',y=' + str(comp.y) + ')')
		return graphics	
		
	def labelGraphics(self,node,comp):			# create LabelGraphics tag
		label = self.section(node,'LabelGraphics')
		self.attribute(label,'text','String',comp.id)
		self.attribute(label,'fontSize','int',font['size'])
		self.attribute(label,'fontName','string',font['name'])
		self.attribute(label,'model',None,None)
		log.debug('\t  labelgraphics tag added: ' + str(comp.id))
		return label
		
	# XGML elements
	def root(self,name):					# create root element
		root = et.Element('section', name=name)
		#log.debug('\t\tsection   element created: ' + name)
		return root
		
	def attribute(self,parent,key,type,value):		# create attribute element 
		if value is None:
			if type is None:
				attribute = et.SubElement(parent,'attribute',key=str(key))
			else:
				attribute = et.SubElement(parent,'attribute',key=str(key),type=type)
		else:
			attribute = et.SubElement(parent,'attribute',key=str(key),type=type).text = str(value)
		#log.debug('\tattribute element created: ' + key)
		return attribute

	def section(self,parent,name): 				# create section element
		section = et.SubElement(parent,'section',name=name)
		#log.debug('\tsection   element created: ' + name)
		return section
		
	# Write to file / screen / string
	def rough_write(self,filename):
		tree = et.ElementTree(self.xgml)
		tree.write(filename, xml_declaration=True, encoding='utf-8', method='xml')
		log.info('  xgml tree written')

	def pretty_write(self,filename):
		tree = dt.parseString(et.tostring(self.xgml, 'utf-8'))
		string = tree.toprettyxml(indent='\t')
		with open(filename,'w') as f:
			f.write(string)
		log.info('  xgml tree written')

	def rough_print(self):
		print et.tostring(self.xgml)

	def pretty_print(self):
		tree = dt.parseString(et.tostring(self.xgml,'utf-8'))
		print tree.toprettyxml(indent='\t')

	def rough_string(self):
		return et.tostring(self.xgml)

	def pretty_string(self):
		tree = dt.parseString(et.tostring(self.xgml,'utf-8'))
		return tree.toprettyxml(indent='\t')

# --------------------------------------------------------------------------------------

# MAIN
def main():	
	# logging
	if len(sys.argv) > 1: 					
   	 	loglevel = LOG.get(sys.argv[1], log.NOTSET)
		log.basicConfig(level=loglevel)
	else:
		log.basicConfig(level=log.CRITICAL)

	# model
	log.info('\n ******************* \n *      MODEL      * \n ******************* \n')
	root_xml = et.parse(f_model).getroot()
	model = Model(30)
	model.make(root_xml)
	model.position()
	
	# image
	log.info('\n ******************* \n *      IMAGE      * \n ******************* \n')
	image = Image()
	image.make(model)
	image.pretty_write(f_image)
	
if __name__ == '__main__':									
	main()
