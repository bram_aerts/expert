""" E'XperT project - visualisation.py: Visualize the problog result on an xgml image of the terminal 
"""
__author__ = "Shana Van Dessel"

import re
import sys
import copy
import xml.etree.ElementTree as et
import logging as lg

inputFileResult = 'result.txt'
inputFileImage  = 'image.xgml'
outputFileImage = 'visualization.xgml'

problemsRegex = {'fout':r'(?<=fout\().*(?=\))',
		 'slecht_contact': r'(?<=slecht_contact\().*(?=\))'}
functionRegex = {'function': r'(?<=component_in_active_path\().*(?=\))',
		 'endpoints': r'(?<=\bpath\().*(?=\))' }
functionColour = '#0000FF'
logModes = {'debug': lg.DEBUG,'info': lg.INFO,'warning': lg.WARNING,'error': lg.ERROR,'critical': lg.CRITICAL}

# --------------------------------------------------------------------
# Gather the needed data from the result and image file
# --------------------------------------------------------------------

def getProblems(fin,regex):
	""" Get components per problem from a text file with the problog result 
	ARG: fin = input file with problog result, regex = regex's to search file
	RET: result = dictionary containing the results
	"""
	result = dict()	
	with open(fin,"r") as f:
		lg.info('  Read file: ' + fin)
		for r in regex:
			lg.debug(' Search for: ' + r + ' ' + regex[r])
		for l in f:
			for r in regex:
				if r not in result:						# Make new key for this regex
					result[r]=dict()
				m = re.search(regex[r], l, re.I|re.M)	# search for regex
				if m is None: 								#   not found...
					continue  
				lg.debug(" \t" + l.strip()) 				#   found!
				result[r][m.group(0).split('_')[0]] = l.split(':')[1].strip()	# others do
	for r in result:
		lg.info('  Results for: '+r)
		for x in result[r]:
			lg.info(' \t' + x + ': ' + result[r][x])
	lg.info('  Got it!\n')
	return result

def getFunction(fin,regex):
	""" Get all the components who are being used by an ongoing function from a text file with the problog result 
	ARG: fin = input file with problog result, regex = regex's to search file
	RET: result = dictionary containing the results
	"""
	result = list()	
	with open(fin,"r") as f:
		lg.info('  Read file: ' + fin)
		for r in regex:
			lg.debug(' Search for: ' + r + ' ' + regex[r])		
		for l in f:
			for r in regex:
				m = re.search(regex[r], l, re.I|re.M)	# search for regex
				if m is None: 								#   not found...
					continue  
				lg.debug(" \t" + l.strip()) 				#   found!
				for x in m.group(0).split(','):
					result.append(x)	
	lg.info('  Results:')
	for x in result:
		lg.info(' \t' + x)
	lg.info('  Got it!\n')
	return result

def getImage(fin):
	"""	Get the image from a xgml file
	ARG: fin = xgml input file with the image of the terminal model,
	RET: image = xgml tree from file
	"""
	lg.info('  Read file: ' + fin)
	image = et.parse(fin)	
	lg.info('  Got it!\n')
	return image

# --------------------------------------------------------------------
# Combine the result and image into a new image, visualizing the result
# --------------------------------------------------------------------

def resultToImage(fout,result,image,function=False,outline=False):
	"""	 Find components in the image and change their colour based on the result. 
		 Write the result to an xgml image 
	ARG: fout = outputfile for new image, result = components to visualize on the image, 
		 function = visualize function or else problems, outline = colour outline as well or just fill
	"""
	check = False						# check if component is found in image
	toColour = ['fill']					# list of attributes that need to be changed
	if outline:
	      toColour.append('outline')	  
	root = image.getroot()				# get xgml tag 
	graph = image.find('section')		# get graph tag 
	for rKey in result:
		check = 0
		lg.debug(' \t' + rKey)
		if function:					# get colour for function
			#global functionColour
			newColour = functionColour	
		else:							# get colour for problem
			newColour = getColour(result[rKey])	

		#for element in graph.findall("section[@name='node']"):
		for element in graph.findall("section"):			# get section tag (node,edge) 
			label = element.findall(".//*[@key='label']")	# get attribute tag (key=label)
			if label == [] or label == ['None']:
				continue
			lKey = label[0].text
			if lKey is None:
				continue
			#lg.debug(' Key: ' + lKey)
			if rKey.lower().replace('-','_') == lKey.lower().replace('-','_'):
				check += 1
				graphics = element.findall(".//*[@name='graphics']")[0]			# get section tag (graphics) from element
				for x in toColour:
					colour = graphics.findall(".//attribute[@key='"+x+"']")[0]	# get attribute tag (key=x) from graphics
					colour.text = newColour
					colour.set('updated', 'yes')
		lg.debug(' \t' + str(check) + ' components found')
				
		if check == 0:
			lg.error(" Component not found!")
		else:
			lg.info(' \t' + rKey + ' changed')
	image.write(fout)
	lg.info('  Got it!\n')
	return

def getColour(oriVal):
	"""	Map a value from 0 to 1 on one of 300 colour gradients going from yellow to dark red
	ARG: oriVal = the original value (between 0 and 1)
	RET: clr = corresponding color (between yellow and red)
	"""
	oriVal = float(oriVal)		# convert string to int
	val = int(oriVal*300)		# scale original value to 0-300
	if val<=255:			# map 0-255 on a colour
		rgb = (255,255-val,0)
	else:				# map 256-300 on a colour
		rgb = (255-(300-val),0,0)
	clr = '#%02x%02x%02x' % (rgb[0],rgb[1],rgb[2])
	lg.debug(' \tGenerate colour: ' + str(float(int(oriVal*10000))/100) + '% (' + str(val) + '/300) --> ' + clr)
	return clr

def getFilename(base,part):
	"""	Create a new filename based on a default name and an additional part
	ARG: base = default filename, part = additional part that needs to be add to the filename
	RET: name = new filename
	"""
	name = base.split('.')[0] + '_' + part + '.' + base.split('.')[1]
	lg.debug(' Generate filename: ' + base + ' --> ' + name)
	return name

# --------------------------------------------------------------------
# Main
# --------------------------------------------------------------------

def logging():
	""" get logging (different levels)"""
	if len(sys.argv) > 1: 
		lg.basicConfig(level=logModes.get(sys.argv[1], lg.NOTSET))
	else:
		lg.basicConfig(level=lg.CRITICAL)
	return

def data():
	""" gather all needed data from files
		- components with which something is wrong
		- components who are in a ongoing function
		- image on which these components will be displayed 
	"""
	lg.info("\n--------------------\n|  DATA            |\n--------------------")
	lg.info('  PROBLEMS')
	problems= getProblems(inputFileResult,problemsRegex)
	lg.info('  FUNCTION')
	function = getFunction(inputFileResult,functionRegex)
	lg.info('  IMAGE')
	image = getImage(inputFileImage)
	return problems, function, image	

def visualization(problems, function, image):
	""" visualize the results on the image """
	lg.info("\n--------------------\n|  VISUALIZATION   |\n--------------------")
	for x in problems:
		lg.info('  PROBLEMS - ' + x)
		visualize(problems[x], image, x)
	lg.info('  FUNCTION')
	visualize(function, image, 'function',function=True)
	return

def visualize(components, image, name, function = False):
	""" visualize components on the image """
	f = getFilename(outputFileImage,name)
	lg.info('  Write file: ' + f)
	resultToImage(f,components,copy.deepcopy(image),function)
	return 

def main():	
	logging()
	problems,function, image = data()
	visualization(problems, function, image)

if __name__ == '__main__':									
	main()
