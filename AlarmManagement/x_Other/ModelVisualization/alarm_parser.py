#########################################################
# E'XperT project										# 
#########################################################
# Description:   Alarm data (CSV) to Problog parser		#
# Author(s): 	 Shana Van Dessel - Bram Aerts			#
# Document:	 	 model_parser.py						#
#########################################################

# IMPORTS
from bs4 import BeautifulSoup
import sys
import time
import logging
import itertools

# PREDEFINED

# files
f_xml_model = "model.xml"			# IN: terminal model in XML
f_csv_alarm = "alarm.csv"			# IN: alarm data in CSV
f_prob_evid = "alarm.prob"			# OUT: alarm data (evidence) in PROBLOG
f_ignore = "ignore.csv"				# IN: list with alarms to ignore
f_sensor_type = "type_sensor.csv"	# IN: list with different types of sensors
f_alarm_type  = "type_alarm.csv"	# IN: list with different types of alarms 

# strings
evidence_true = "evidence(alarm(", "),true).\n" 
evidence_false = "evidence(alarm(", "),false).\n" 
LOGLEVELS = {'debug': logging.DEBUG,'info': logging.INFO,'warning': logging.WARNING,'error': logging.ERROR,'critical': logging.CRITICAL}

# -----------------------------------------------------------------------------------------------------------------------------------------------------

# FUNCTIONS

# * model * 
def get_model_list(model_xml_file, alarm_type_file, sensor_type_file):	# create model list (id, alarm, sensor)	
	model_list = dict()
	model_list["sensor"] = get_instrument_ids(model_xml_file)
	model_list["id"] = list(get_ids(model_xml_file) - set(model_list["sensor"]	))
	model_list["alarm"] = get_list(alarm_type_file)
	
	for key in model_list.keys():
		logging.info(' Model list (' + str(key) + ') = ' + str(model_list[key]))
	return model_list

def get_ids(xml_file):							# get all (different) id's from xml file		
	soup = BeautifulSoup(open(xml_file), "xml")
	id_list = soup.findAll("TechnicalId")
	id_list = [to_prob(i.text) for i in id_list]	
	id_list = set(id_list)

	logging.debug(' ID\'s = ' + str(id_list))	
	return id_list

def get_instrument_ids(xml_file):					# get all (different) instrument id's from xml file		
	soup = BeautifulSoup(open(xml_file), "xml")
	instruments = soup.terminal.Instruments.findChildren(recursive=False)
	id_list = []
	for i in instruments:
		id_list.append(to_prob(i.TechnicalId.text))
	
	logging.debug(' Instrument ID\'s = ' + str(id_list))	
	return id_list

# * alarms *
def get_alarm_list(alarm_csv_file, model_list):				# create alarm list (ts (id, alarm, sensor): true/false)
	alarm_list = dict()
	index = 0
	timestamp = 0

	with open(alarm_csv_file,"r") as f:
		f.readline()
		parts = f.readline().rstrip().split(";")
		first_timestamp = to_timestamp(parts[0],parts[1], 0)	

	while index is not None:
		if timestamp == 0:
			index, alarm_list[timestamp] = get_alarms_per_timestamp(alarm_csv_file, model_list, first_timestamp, timestamp, index, dict())
 		else:
			index, alarm_list[timestamp] = get_alarms_per_timestamp(alarm_csv_file, model_list, first_timestamp, timestamp, index, dict(alarm_list[timestamp-1]))

		timestamp += 1

	for ts in alarm_list:
		logging.info(' Alarm list [' + str(ts) + '] = ' + str(alarm_list[ts]))
	return alarm_list

def get_alarms_per_timestamp(alarm_csv_file, model_list, first_timestamp, timestamp, index, alarms): 
	end = True
	with open(alarm_csv_file,"r") as f:
		f.readline()
		
		for i,line in enumerate(f):
			
			if i < index:
				continue

			if is_ignore(line):
				continue

			parts = to_prob(line).rstrip().split(";")
			new_timestamp = to_timestamp(parts[0],parts[1], first_timestamp)

			if new_timestamp < timestamp:
				continue
			if new_timestamp > timestamp:
				end = False
				break

			component = get_element(model_list["id"],parts[2])
			sensor_type = get_element(model_list["sensor"],parts[2])
			alarm_type = get_element(model_list["alarm"],parts[2])
			
			if parts[3] == "off":
				del alarms[(component, alarm_type, sensor_type)]
			if parts[3] == "ack":
				continue
			if parts[3] == "on":
				alarms[(component, alarm_type, sensor_type)] = True
			
		if end:
			i = None
			
	logging.debug(' Alarm list (ts=' + str(timestamp-1) + ') = ' + str(alarms))	
	return i, alarms

def is_ignore(line):								# alarms to ignore
	return False	# XXX TODO XXX

# * evidence *
def alarms_to_evidence(f_prob_evid, model_list, alarm_list):					# write alarms (as evidence) to problog file
	with open(f_prob_evid,"w") as f:
		for timestamp in alarm_list:
			combi = itertools.product(model_list["id"], model_list["alarm"], model_list["sensor"])
			for x in combi:
				text = ",".join(x) + "," + str(timestamp)
				to_prob(text)
				
				if x in alarm_list[timestamp].keys():		
					f.write(evidence_true[0] + text + evidence_true[1])	
				else:
					f.write(evidence_false[0] + text + evidence_false[1]) 
	return 0

# * common * 
def to_timestamp(dmy, hms, start_time_epoch):					# creating timestamp
	epoch = int(time.mktime(time.strptime(dmy + ";" + hms, "%d/%m/%y;%H:%M:%S")))
	
	logging.debug(' \tTimestamp = ' + str(epoch-start_time_epoch))
	return (epoch - start_time_epoch)

def to_prob(text):								# change to lower case, without "-"
	return text.lower().replace("-","_").replace(" ","_")

def get_element(things, text):							# find specific elements in a string		
	match = ""
	for i in things:
		if str(i) in text and len(i) > len(match):
			match = i
	if match == "":
		logging.warning('\tNo match found  (\"' + text + '\").')
		match = "other"

	logging.debug('\tMatch = ' + match)
	return match

def get_list(a_file):							# get all items from a file (list/1 column)
	a_list = []
	with open(a_file,"r") as f:
		f.readline()
		for i in f:
			if i == "":
				continue
			a_list.append(to_prob(i.rstrip()))

	logging.debug(' CSV list = ' + str(a_list))	
	return a_list

# -----------------------------------------------------------------------------------------------------------------------------------------------------

# MAIN	
def main():
	# logging
	if len(sys.argv) > 1: 
   	 	loglevel = LOGLEVELS.get(sys.argv[1], logging.NOTSET)
    		logging.basicConfig(level=loglevel)
	else:
		logging.basicConfig(level=logging.CRITICAL)
	
	# function calls
	logging.info(' MODEL LIST - id, alarm, sensor')
	model_list = get_model_list(f_xml_model, f_alarm_type, f_sensor_type)

	logging.info(' ALARM LIST - ts: (id, alarm, sensor): true/false')
	alarm_list = get_alarm_list(f_csv_alarm, model_list)

	logging.info(' EVIDENCE - evidence(alarm(ts, id, alarm, sensor), true/false).')
	alarms_to_evidence(f_prob_evid, model_list, alarm_list)							

if __name__ == "__main__":									
	main()

