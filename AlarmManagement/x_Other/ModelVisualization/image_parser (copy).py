#####################################################################
# E'XperT project													# 
#####################################################################
# Description:  Terminal model (XML) to vector immage (XGML) parser	#
# Author(s): 	Shana Van Dessel 									#
# Document:	 	image_parser.py										#
#####################################################################

# IMPORTS
import logging
import sys
import xml.etree.ElementTree as ET
import xml.dom.minidom as DT

# PREDEFINED 
f_model = 'model.xml'
f_image = 'model.xgml'

LOG = {'debug': logging.DEBUG,'info': logging.INFO,'warning': logging.WARNING,'error': logging.ERROR,'critical': logging.CRITICAL}
IMG = True
LAB = False

prop    = {	'tank':		(60,100,'rectangle','com.yworks.flowchart.dataBase'), 
			'connector':(10,10, 'rectangle','com.yworks.flowchart.directData'), 
			'pump':		(20,20, 'ellipse',	'com.yworks.flowchart.start2'), 
			'valve':	(20,10, 'triangle',	'com.yworks.flowchart.decision')}

color   = {'white': '#FFFFFF', 'black': '#000000'}
font    = {'name':  'Dialog',  'size':  '12'}
figures = {'rectangle', 'ellipse', 'triangle', 'roundrectangle'}

# ----------------------------------------------------------------------------------------------------------------------------------------------

# FUNCTIONS
def xml_to_xgml(xml_file, xgml_file):
	i=0;

	root_xgml = tag_xgml()
	logging.debug('xgml \n' + pretty_string(root_xgml))
	
	graph = tag_graph(root_xgml)
	logging.debug('graph \n' + pretty_string(graph))
	
	root_xml= ET.parse(xml_file).getroot()

	# Tanks
	x=0; y=0
	x2=prop['tank'][0]/2 #- prop['connector'][0]/2

	for tank in root_xml.find('equipment').find('Tanks'):
		tid = tank.find('TechnicalId').text
		node = tag_node(graph,i,tid) ; i+=1
		tag_graphics(node,prop['tank'][2],x,y,prop['tank'][0],prop['tank'][1])
		tag_LabelGraphic(node,tid)
		logging.debug('node ' + tid + '\n' + pretty_string(node))

	# Tank connections
		y2 = y + prop['tank'][1]/2 - prop['tank'][1]*0.1

		for tankconnection in tank.find('Connections'):
			tid = tankconnection.find('TechnicalId').text
			node = tag_node(graph,i,tid); i+=1
			tag_graphics(node,prop['connector'][2],x2,y2,prop['connector'][0],prop['connector'][1]); y2 -= prop['connector'][1] * 1.4
			if not LAB:
				tid = None
			tag_LabelGraphic(node,tid)
			logging.debug('node ' + str(tid) + '\n' + pretty_string(node))	

		y+= prop['tank'][1] * 1.2
		

	#rough_write(root_xgml, xgml_file)
	pretty_write(root_xgml, xgml_file)
	logging.debug('XGML tree written to ' + xgml_file)
	return 0

# Tags
def tag_xgml():
	xgml = root('xgml')
	attribute(xgml,'Creator','String','yFiles')
	attribute(xgml,'Version','String','2.12')
	return xgml

def tag_graph(xgml):
	graph = section(xgml,'graph')
	attribute(graph,'hierarchic','int','1')
	attribute(graph,'label','String',' ')
	attribute(graph,'directed','int','1')
	return graph

def tag_node(graph, i, key):
	node = section(graph,'node')
	attribute(node,'id','int', i)
	attribute(node,'label','String', key)
	return node

def tag_graphics(node, t, x, y, w, h): 	# type, x, y, width, height
	graphics = section(node,'graphics')
	attribute(graphics,'x','double',x)
	attribute(graphics,'y','double',y)
	attribute(graphics,'w','double',w)
	attribute(graphics,'h','double',h)
	if t in figures:
		attribute(graphics,'type','String',t)
	else:
		attribute(graphics,'customconfiguration','String',t)
	attribute(graphics,'fill','String',color['white'])
	attribute(graphics,'outline','String',color['black'])
	return graphics

def tag_LabelGraphic(node, key):
	label = section(node,'LabelGraphics')
	#if key is None:
	#	return
	attribute(label, 'text', 'String', key)
	attribute(label, 'fontSize', 'int', font['size'])
	attribute(label, 'fontName', 'string', font['name'])
	attribute(label, 'model', None, None)
	return label

# Elements
def attribute(p, k, t, v): 			# parent, key, type, value
	if v is None:
		if t is None:
			ET.SubElement(p, 'attribute', key=str(k))
		else:
			ET.SubElement(p, 'attribute', key=str(k), type=t)

	else:
		ET.SubElement(p, 'attribute', key=str(k), type=t).text = str(v)

	return 0

def section(p, n): # parent, name
	return ET.SubElement(p, 'section', name=n)

def root(n):
	return ET.Element('section', name=n)

# Writes
def rough_write(root, filename):
	tree = ET.ElementTree(root)
	tree.write(filename, xml_declaration=True,encoding='utf-8', method='xml')
	return 0

def pretty_write(root, filename):
	tree = DT.parseString(ET.tostring(root, 'utf-8'))
	string = tree.toprettyxml(indent='\t')
	with open(filename,'w') as f:
		f.write(string)
	return 0

# Markups
def rough_string(root):
	return ET.tostring(root)

def pretty_string(root):
	tree = DT.parseString(ET.tostring(root, 'utf-8'))
	return tree.toprettyxml(indent='\t')

# ----------------------------------------------------------------------------------------------------------------------------------------------

# MAIN
def main():	
	# logging
	if len(sys.argv) > 1: 
   	 	loglevel = LOG.get(sys.argv[1], logging.NOTSET)
		logging.basicConfig(level=loglevel)
	else:
		logging.basicConfig(level=logging.CRITICAL)
	
	# parsing xml model to xgml image													
	xml_to_xgml(f_model, f_image)

if __name__ == '__main__':									
	main()

