#####################################################################
# E'XperT project													# 
#####################################################################
# Description:  Terminal model (XML) to vector immage (XGML) parser	#
# Author(s): 	Shana Van Dessel 									#
# Document:	 	image_parser.py										#
#####################################################################

# IMPORTS
import sys
import math
import logging
import xml.dom.minidom as DT
import xml.etree.ElementTree as ET

# PREDEFINED VALUES
f_model = 'model.xml'
f_image = 'model.xgml'

LOG = {'debug': logging.DEBUG,'info': logging.INFO,'warning': logging.WARNING,'error': logging.ERROR,'critical': logging.CRITICAL}
IMG = True
LAB = True

grid    = 30
prop    = {	'tank':		(120,210,'rectangle','com.yworks.flowchart.dataBase'), 
			'quay':		(120,60, 'rectangle','com.yworks.flowchart.directData'),
			'connector':(20, 20, 'rectangle','com.yworks.flowchart.directData'), 
			'pump':		(20, 20, 'ellipse',  'com.yworks.flowchart.start2'), 
			'valve':	(30, 20, 'triangle', 'com.yworks.flowchart.decision'),
			'point':	(3,'ellipse') }

color   = { 'white': '#FFFFFF', 'black': '#000000', 'red': '#FF0000' }
font    = { 'name':  'Dialog',  'size':  '9' }
figures = { 'rectangle', 'ellipse', 'triangle', 'roundrectangle' }

# -------------------------------------------------------------------------------------

# FUNCTIONS
def xml_to_xgml(xml_file):
	root_xml= ET.parse(xml_file).getroot()	# get xml root

	root_xgml = tag_xgml()					# set xgml root 
	graph = tag_graph(root_xgml)
	
	valves = []								# debug valves
	for x in root_xml.find('equipment').find('Valves'):
		valves.append(x.find('TechnicalId').text)

	#drawing
	xl,yl = draw_tanks(root_xml,graph, valves)
	xl += prop['tank'][0]/2 - prop['quay'][0]/2
	draw_quays(root_xml,graph,xl,yl)
	#draw_pumps(root_xml,graph)

	logging.info('  xgml tree ready')
	return root_xgml

# ---

# Drawings
def draw_tanks(root,graph, list_v):
	xt=0; yt=0								# x tank, y tank
	xc= xt + prop['tank'][0]/2				# x tank connector

	for tank in root.find('equipment').find('Tanks'):
		# Tank
		tid = tank.find('TechnicalId').text
		draw_figure(graph,tid,'tank',xt,yt)

		yc = grid_floor(yt + prop['tank'][1]*0.35)

		for tankconnection in tank.find('Connections'):
			# Tank connections
			tid = tankconnection.find('TechnicalId').text
			draw_figure(graph,tid,'connector',xc,yc)

			# Connection points
			draw_point(graph,tid+'-pt',xc+grid,yc)		
			draw_line(graph,tid+'-ln',tid,tid+'-pt')

			# Valves
			for pipeline in root.find('pipelines'):
				ids = []
				lid = pipeline.find('TechnicalId').text
				for connection in pipeline.find('Connections'):
					ids.append(connection.find('TechnicalId').text)

				if tid in ids:
					ids.remove(tid)
					draw_figure(graph,ids[0],'valve',xc+3*grid,yc)	

					draw_point(graph,ids[0]+'-l-pt',xc+2*grid,yc)
					draw_line(graph,ids[0]+'-l-ln',ids[0],ids[0]+'-l-pt' )	
					draw_point(graph,ids[0]+'-r-pt',xc+4*grid,yc)
					draw_line(graph,ids[0]+'-r-ln',ids[0],ids[0]+'-r-pt' )	

					draw_pipeline(graph,lid,tid+'-pt',ids[0]+'-l-pt')

					if len(ids) > 1:
						logging.warning('Meerdere objecten aan eenzelfde tankconnectie.')
				
			yc -= grid							# y tank connector
		yt += grid_ceil(prop['tank'][1] + grid)	# y tank

	return xt, yt

def draw_quays(root,graph,xq,yq):			
	for quay in root.find('equipment').find('ProductTransferConnections'):
		# Quay
		tid = quay.find('TechnicalId').text
		draw_figure(graph,tid,'quay',xq,yq)

		# Connection point
		draw_point(graph,tid+'-pt',xq+prop['quay'][0]/2+grid,yq)		
		draw_line(graph,tid+'-ln',tid,tid+'-pt')
		
		yq += grid_ceil(prop['quay'][1] + grid)	# y quay

	return 0

def draw_figure(graph,tid,item,x,y):		# parent, id, item, x-pos, y-pos
	logging.info('  draw ' +item +' ' +str(tid) +'\t(x=' +str(x) +',y=' +str(y) +')')
	i = prop[item][3] if IMG else prop[item][2]
	node = tag_node(graph, tid)
	graphics = tag_graphics(node,i,x,y,prop[item][0],prop[item][1]) 
	if LAB: tag_LabelGraphics(node,tid)
	return node, graphics

def draw_pipeline(graph,tid,src,dst):		# parent, id, source, destination
	logging.info('  draw pipeline ' + str(tid) + ' (from ' + src + ' to ' + dst + ')')
	edge = tag_edge(graph,tid,src,dst)	
	if LAB: tag_LabelGraphics(edge,tid)
	return edge

def draw_point(graph,tid,x,y):				# parent, id, x-pos, y-pos
	logging.debug(' draw point ' +str(tid) +'\t(x=' +str(x) +',y=' +str(y) +')')
	node = tag_node(graph, tid)
	graphics = tag_graphics(node,prop['point'][1],x,y,prop['point'][0],prop['point'][0]) 			
	attribute(graphics,'fill','String',color['black'])
	return graphics

def draw_line(graph,tid,src,dst):			# parent, id, source, destination
	logging.debug(' draw line ' + str(tid))	
	edge = tag_edge(graph,tid,src,dst)	
	return edge

# ---

# XGML Elements
def tag_xgml():
	xgml = root('xgml')
	attribute(xgml,'Creator','String','yFiles')
	attribute(xgml,'Version','String','2.12')

	logging.debug('\txgml element created.')
	return xgml

def tag_graph(xgml):
	graph = section(xgml,'graph')
	attribute(graph,'hierarchic','int','1')
	attribute(graph,'label','String',' ')
	attribute(graph,'directed','int','1')

	logging.debug('\tgraph element created.')
	return graph

def tag_node(graph,key):
	node = section(graph,'node')
	attribute(node,'id','String', key)
	#if label: 
	#	attribute(node,'label','String', key)

	logging.debug('\tnode element (' + key + ') created.')
	return node

def tag_graphics(node,t,x,y,w,h): 			# parent, type, x-pos, y-pos, width, height
	graphics = section(node,'graphics')
	attribute(graphics,'x','double',x)
	attribute(graphics,'y','double',y)
	attribute(graphics,'w','double',w)
	attribute(graphics,'h','double',h)
	if t in figures:
		attribute(graphics,'type','String',t)
	else:
		attribute(graphics,'customconfiguration','String',t)
	attribute(graphics,'fill','String',color['white'])
	attribute(graphics,'outline','String',color['black'])

	logging.debug('\tgraphics element created.')
	return graphics

def tag_LabelGraphics(node,key):
	label = section(node,'LabelGraphics')
	attribute(label,'text','String',key)
	attribute(label,'fontSize','int',font['size'])
	attribute(label,'fontName','string',font['name'])
	attribute(label,'model',None,None)

	logging.debug('\tlabelgraphics element created.')
	return label

def tag_edge(graph,tid,source,target):
	edge = section(graph,'edge')
	if tid is not None: attribute(edge,'id','String',tid)
	attribute(edge,'source','String',source)
	attribute(edge,'target','String',target)
	logging.debug('\tedge element (' + str(tid) + ') created.')

	line = section(edge,'graphics')
	attribute(line,'fill','String',color['black'])	
	logging.debug('\tgraphics element created.')
	return edge

# XGML Element tags
def attribute(p,k,t,v): 					# parent, key, type, value
	if v is None:
		if t is None:
			ET.SubElement(p,'attribute',key=str(k))
		else:
			ET.SubElement(p,'attribute',key=str(k),type=t)
	else:
		ET.SubElement(p,'attribute',key=str(k),type=t).text = str(v)
	return 0

def section(p,n): 							# parent, name
	return ET.SubElement(p,'section',name=n)

def root(n):								# name
	return ET.Element('section', name=n)

# ---

# Rounding ceil/floor
def grid_ceil(x):
	return math.ceil(x/grid) * grid

def grid_floor(x):
	return math.floor(x/grid) * grid

# ---

# Write to file / screen / string
def rough_write(root,filename):
	tree = ET.ElementTree(root)
	tree.write(filename, xml_declaration=True, encoding='utf-8', method='xml')
	logging.info('  xgml tree written')
	return 0

def pretty_write(root,filename):
	tree = DT.parseString(ET.tostring(root, 'utf-8'))
	string = tree.toprettyxml(indent='\t')
	with open(filename,'w') as f:
		f.write(string)
	logging.info('  xgml tree written')
	return 0

def rough_print(root):
	print ET.tostring(root)
	return 0

def pretty_print(root):
	tree = DT.parseString(ET.tostring(root,'utf-8'))
	print tree.toprettyxml(indent='\t')
	return 0

def rough_string(root):
	return ET.tostring(root)

def pretty_string(root):
	tree = DT.parseString(ET.tostring(root,'utf-8'))
	return tree.toprettyxml(indent='\t')

# -------------------------------------------------------------------------------------

# MAIN
def main():	

	if len(sys.argv) > 1: 					# logging
   	 	loglevel = LOG.get(sys.argv[1], logging.NOTSET)
		logging.basicConfig(level=loglevel)
	else:
		logging.basicConfig(level=logging.CRITICAL)
	
												
	xgml = xml_to_xgml(f_model)				# parse xml model to xgml image	

	#rough_print(xgml)						# print xgml
	#pretty_print(xgml)
	#rough_write(xgml, f_image)				# write xgml
	pretty_write(xgml, f_image)

if __name__ == '__main__':									
	main()

