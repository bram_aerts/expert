#####################################################################
# E'XperT project													# 
#####################################################################
# Description:  Terminal model (XML) to vector immage (XGML) parser	#
# Author(s): 	Shana Van Dessel 									#
# Document:	 	image_parser.py										#
#####################################################################

# IMPORTS
import sys
import math
import logging as log
import xml.dom.minidom as dt
import xml.etree.ElementTree as et

# PREDEFINED VALUES
f_model = 'model.xml'
f_image = 'model.xgml'

LOG = {'debug':log.DEBUG,'info':log.INFO,'warning':log.WARNING,'error':log.ERROR,'critical':log.CRITICAL}
IMG = True
LAB = True

color = { 'white': '#FFFFFF', 'black': '#000000', 'red': '#FF0000' }
font  = { 'name':  'Dialog',  'size':  '9' }

# CLASSES
class Model(object):
	def __init__(self,grid):
		self.grid = 30
		self.tanks = Components()	
		self.valves = Components()
		self.pumps = Components()
		self.quays = Components()
		self.lines = Components()	

	def makeModel(self,xml_root):
		x = xml_root.find('equipment').find('Tanks')
		self.makeTanks(x)
		self.positionTanks()
		x = xml_root.find('equipment').find('ProductTransferConnections')
		self.makeQuays(x)
		self.positionQuays()
		log.info('  ' + str(self))

	def makeTanks(self,xml_tanks):
		for t in xml_tanks:
			tid = t.find('TechnicalId').text
			tank = Tank(tid,0,0)
			self.tanks.addComponent(tank)
		log.debug(' Tanks: \n\t' + str(self.tanks))

	def positionTanks(self):
		for i,t in enumerate(self.tanks.lijst):
			t.x = t.w/2
			t.y = self.ceilToGrid((t.h + self.grid)*i)
			if i == 0: 	self.tanks.changeWindow(t.x,t.y,t.w,t.h)
			else:		self.tanks.extendWindow(t.x,t.y,t.w,t.h)
	
		log.debug(' Tanks: \n\t' + 'Window:\n\t' + str(self.tanks.xmin) + ',' + str(self.tanks.xmax) 
					+ ',' + str(self.tanks.ymin) + ',' + str(self.tanks.ymax) + '\n\t' + str(self.tanks))

	def makeQuays(self,xml_quays):	
		for q in xml_quays:
			tid = q.find('TechnicalId').text
			quay = Quay(tid,0,0)
			self.quays.addComponent(quay)
		log.debug(' Quays: \n\t' + str(self.quays))

	def positionQuays(self):
		for i,q in enumerate(self.quays.lijst):
			q.x = self.tanks.xmax - q.w/2
			q.y += self.ceilToGrid(self.tanks.ymax + q.h/2 + self.grid)
			if i == 0: 	self.quays.changeWindow(q.x,q.y,q.w,q.h)
			else:		self.quays.extendWindow(q.x,q.y,q.w,q.h)
		log.debug(' Quays: \n\t' + 'Window:\n\t' + str(self.quays.xmin) + ',' + str(self.quays.xmax) 
					+ ',' + str(self.quays.ymin) + ',' + str(self.quays.ymax) + '\n\t' + str(self.quays))

	def makePipelines(self,xml_pipes):
		log.info(str(self.lines))

	def ceilToGrid(self,a):
		return math.ceil(a/self.grid) * self.grid

	def floorToGrid(self,a):
		return math.floor(a/self.grid) * self.grid

	def __str__(self):
		string = self.__class__.__name__ + ': \n'
		string += '\t' + str(self.tanks) + '\t' + str(self.quays)
		return string

class Components(object):
	def __init__(self):
		self.lijst = []
		self.changeWindow(0,0,0,0)

	def addComponent(self,comp):
		self.lijst.append(comp)
		self.extendWindow(comp.x,comp.y,comp.w,comp.h)

	def changeWindow(self,x,y,w,h):
		self.xmin = x-w/2  
		self.xmax = x+w/2  
		self.ymin = y-h/2
		self.ymax = y+h/2
		#log.debug('New window: ' + str(self.xmin) + ',' + str(self.xmax) + ',' + str(self.ymin) + ',' + str(self.ymax))

	def extendWindow(self,x,y,w,h):
		if x-w/2 < self.xmin: self.xmin = x-w/2  
		if x+w/2 > self.xmax: self.xmax = x+w/2
		if y-h/2 < self.ymin: self.ymin = y-h/2
		if y+h/2 > self.ymax: self.ymax = y+h/2
		#log.debug('Window: ' + str(self.xmin) + ',' + str(self.xmax) + ',' + str(self.ymin) + ',' + str(self.ymax))

	def __str__(self):
		string = self.__class__.__name__ +  ': \n'
		for x in self.lijst:
			string += '\t' + str(x) + '\n' 
		return string

class Component(object):
	def __init__(self,i):
		self.ID = i

	def labelGraphics(parent):		# parent = node
		label = self.section(parent,'LabelGraphics')
		self.attribute(label,'text','String',self.ID)
		self.attribute(label,'fontSize','int',font['size'])
		self.attribute(label,'fontName','string',font['name'])
		self.attribute(label,'model',None,None)
		log.debug('\tlabelgraphics element created.')
		return lab

	def attribute(self,parent,key,typ,val): 	
		if val is None:
			if typ is None:
				attribute = et.SubElement(parent,'attribute',key=str(key))
			else:
				attribute = et.SubElement(parent,'attribute',key=str(key),type=typ)
		else:
			attribute = et.SubElement(parent,'attribute',key=str(key),type=typ).text = str(val)
		log.debug('\t\tattribute element created.')
		return attribute

	def section(self,parent,name): 	
		section = et.SubElement(parent,'section',name=name)
		log.debug('\t\tsection element created.')				
		return section

	def __str__(self):
		return self.__class__.__name__ + ' ' + str(self.ID)

class Pipeline(Component):
	def __init__(self,i,s,d):
		super(Pipeline,self).__init__(i)
		self.src = s
		self.dst = d

	def image(self,parent):		# parent = graph
		edge = self.edge(parent)
		self.graphics(edge)
		if LAB: self.labelGraphics(edge)
		return 0

	def edge(self,parent):		# parent = graph
		edge = self.section(parent,'edge')
		self.attribute(edge,'id','String',self.ID)
		self.attribute(edge,'source','String',self.src)
		self.attribute(edge,'target','String',self.dst)
		log.debug(' edge element (' + str(self.ID) + ') created.')
		return edge

	def graphics(self,parent):  # parent = edge
		graphics = self.section(parent,'graphics')
		self.attribute(graphics,'fill','String',color['black'])
		log.debug('\tgraphics element created.')
		return graphics

	def __str__(self):
		return self.__class__.__name__ + ' ' + str(self.ID) + ' (from:' + str(self.s) + ',to:' +  str(self.d) + ')' 

class Equipment(Component):
	def __init__(self,i,x,y,w,h,s,f):
		super(Equipment,self).__init__(i)
		self.x = x
		self.y = y
		self.w = w
		self.h = h
		self.shape = s
		self.figure = f

	def image(self,parent):		# parent = graph
		node = self.node(parent)
		self.graphics(edge)
		if LAB: self.labelGraphics(edge)
		return 0

	def node(parent):			# parent = graph
		node = self.section(parent,'node')
		self.attribute(node,'id','String', self.ID)
		log.debug(' node element created: ' + self.ID)
		return node

	def graphics(parent):  		# parent = node
		graphics = self.section(parent,'graphics')
		self.attribute(graphics,'x','double',self.x)
		self.attribute(graphics,'y','double',self.y)
		self.attribute(graphics,'w','double',self.w)
		self.attribute(graphics,'h','double',self.h)
		if IMG: self.attribute(graphics,'customconfiguration','String',self.figure)
		else:	self.attribute(graphics,'type','String',self.shape)
		self.attribute(graphics,'fill','String',color['white'])
		self.attribute(graphics,'outline','String',color['black'])
		log.debug('\tgraphics element created.')
		return graphics

	def __str__(self):
		return self.__class__.__name__ + ' ' + str(self.ID) + ' (x=' + str(self.x) + ',y=' + str(self.y) + ')'

class Tank(Equipment):
	def __init__(self,i,x,y):
		super(Tank,self).__init__(i,x,y,120,210,'rectangle','com.yworks.flowchart.dataBase')

class TankConnection(Equipment):
	def __init__(self,i,x,y):
		super(TankConnection,self).__init__(i,x,y,20,20,'rectangle','com.yworks.flowchart.directData')

class Quay(Equipment):
	def __init__(self,i,x,y):
		super(Quay,self).__init__(i,x,y,120,60,'rectangle','com.yworks.flowchart.directData')

class Valve(Equipment):
	def __init__(self,i,x,y):
		super(Valve,self).__init__(i,x,y,20,10,'triangle','com.yworks.flowchart.decision')

class Pump(Equipment):
	def __init__(self,i,x,y):
		super(Pump,self).__init__(i,x,y,20,20,'ellipse','com.yworks.flowchart.start2')

# MAIN
def main():	
	# logging
	if len(sys.argv) > 1: 					
   	 	loglevel = LOG.get(sys.argv[1], log.NOTSET)
		log.basicConfig(level=loglevel)
	else:
		log.basicConfig(level=log.CRITICAL)

	# model
	root  = et.parse(f_model).getroot()
	model = Model(30)
	model.makeModel(root)

	# image
	# TODO XXX
	
if __name__ == '__main__':									
	main()
