#####################################################################
# E'XperT project													# 
#####################################################################
# Description:  Terminal model (XML) to vector immage (XGML) parser	#
# Author(s): 	Shana Van Dessel 									#
# Document:	 	test.py												#
#####################################################################

# IMPORTS
from bs4 import BeautifulSoup
import logging
import xml.etree.cElementTree as ET

# PREDEFINED 
# Files
f_model = "model.xml"
f_image = "model.xgml"

# Strings
xml_start = '<?xml version="1.0" encoding="UTF-8"?>'



# ----------------------------------------------------------------------------------------------------------------------------------------------

# FUNCTIONS
def xml_to_xgml(f_xml, f_xgml):

	xgml = ET.Element("section", name="xgml")
	ET.SubElement(xgml, "attribute", key="Creator", type="String").text = 'yFiles'
	ET.SubElement(xgml, "attribute", key="Version", type="String").text = '2.12'
		
	graph = ET.SubElement(xgml, "session", name="graph")
	ET.SubElement(graph, "attribute", key="hierarchic", type="int").text = '1'
	ET.SubElement(graph, "attribute", key="label", type="String").text = ' '
	ET.SubElement(graph, "attribute", key="directed", type="int").text = '1'
	
	with open(f_xgml,"w") as f:
		f.write(xml_start)

	tree = ET.ElementTree(xgml)
	tree.write(f_xgml)
	
	return 0

# ----------------------------------------------------------------------------------------------------------------------------------------------

# MAIN
def main():																
	xml_to_xgml(f_model, f_image)

if __name__ == "__main__":									
	main()

