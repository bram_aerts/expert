#####################################################################
# E'XperT project													# 
#####################################################################
# Description:  Terminal model (XML) to vector immage (XGML) parser	#
# Author(s): 	Shana Van Dessel 									#
# Document:	 	test.py												#
#####################################################################

# IMPORTS
from bs4 import BeautifulSoup
from yattag import Doc, indent
import logging

# PREDEFINED 
# Files
f_model = "model.xml"
f_image = "model.xgml"

# Strings
xml_start = '<?xml version="1.0" encoding="UTF-8"?>'

# ----------------------------------------------------------------------------------------------------------------------------------------------

# FUNCTIONS
def xml_to_xgml(f_xml, f_xgml):
	doc, tag, text = Doc().tagtext()

	with tag('section', name="xgml"):
		with tag('attribute', key="Creator", type="String"):
			text('yFiles')
		with tag('attribute', key="Version", type="String"):
			text('2.12')
		with tag('session', name="graph"):
			with tag('attribute', key="hierarchic", type="int"):
				text('1')
			with tag('attribute', key="label", type="int"):
				text(' ')
			with tag('attribute', key="directed", type="int"):
				text('1')

	result = indent(doc.getvalue(), indentation = ' '*4, newline = '\r\n')

	with open(f_xgml,"w") as f:
		f.write(xml_start + '\n')
		f.write(result)

	return 0

# ----------------------------------------------------------------------------------------------------------------------------------------------

# MAIN
def main():																
	xml_to_xgml(f_model, f_image)

if __name__ == "__main__":									
	main()

