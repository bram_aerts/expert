

# FUNCTIONS
def xml_to_xgml(xml_file):
	root_xml= ET.parse(xml_file).getroot()	# get xml root

	root_xgml = tag_xgml()					# set xgml root 
	graph = tag_graph(root_xgml)
	
	valves = []								# debug valves
	for x in root_xml.find('equipment').find('Valves'):
		valves.append(x.find('TechnicalId').text)

	#drawing
	xl,yl = draw_tanks(root_xml,graph, valves)
	xl += prop['tank'][0]/2 - prop['quay'][0]/2
	draw_quays(root_xml,graph,xl,yl)
	#draw_pumps(root_xml,graph)

	logging.info('  xgml tree ready')
	return root_xgml

# ---

# Drawings
def draw_tanks(root,graph, list_v):
	#xt=0; yt=0								# x tank, y tank
	xc= xt + prop['tank'][0]/2				# x tank connector

	for tank in root.find('equipment').find('Tanks'):
		# Tank
		#tid = tank.find('TechnicalId').text
		#draw_figure(graph,tid,'tank',xt,yt)

		yc = grid_floor(yt + prop['tank'][1]*0.35)

		for tankconnection in tank.find('Connections'):
			# Tank connections
			tid = tankconnection.find('TechnicalId').text
			draw_figure(graph,tid,'connector',xc,yc)

			# Connection points
			draw_point(graph,tid+'-pt',xc+grid,yc)		
			draw_line(graph,tid+'-ln',tid,tid+'-pt')

			# Valves
			for pipeline in root.find('pipelines'):
				ids = []
				lid = pipeline.find('TechnicalId').text
				for connection in pipeline.find('Connections'):
					ids.append(connection.find('TechnicalId').text)

				if tid in ids:
					ids.remove(tid)
					draw_figure(graph,ids[0],'valve',xc+3*grid,yc)	

					draw_point(graph,ids[0]+'-l-pt',xc+2*grid,yc)
					draw_line(graph,ids[0]+'-l-ln',ids[0],ids[0]+'-l-pt' )	
					draw_point(graph,ids[0]+'-r-pt',xc+4*grid,yc)
					draw_line(graph,ids[0]+'-r-ln',ids[0],ids[0]+'-r-pt' )	

					draw_pipeline(graph,lid,tid+'-pt',ids[0]+'-l-pt')

					if len(ids) > 1:
						logging.warning('Meerdere objecten aan eenzelfde tankconnectie.')
				
			yc -= grid							# y tank connector
		#yt += grid_ceil(prop['tank'][1] + grid)	# y tank

	#return xt, yt


def draw_point(graph,tid,x,y):				# parent, id, x-pos, y-pos
	logging.debug(' draw point ' +str(tid) +'\t(x=' +str(x) +',y=' +str(y) +')')
	node = tag_node(graph, tid)
	graphics = tag_graphics(node,prop['point'][1],x,y,prop['point'][0],prop['point'][0]) 			
	attribute(graphics,'fill','String',color['black'])
	return graphics

def draw_line(graph,tid,src,dst):			# parent, id, source, destination
	logging.debug(' draw line ' + str(tid))	
	edge = tag_edge(graph,tid,src,dst)	
	return edge

# ---

# XGML Elements
def tag_xgml():
	xgml = root('xgml')
	attribute(xgml,'Creator','String','yFiles')
	attribute(xgml,'Version','String','2.12')

	logging.debug('\txgml element created.')
	return xgml

def tag_graph(xgml):
	graph = section(xgml,'graph')
	attribute(graph,'hierarchic','int','1')
	attribute(graph,'label','String',' ')
	attribute(graph,'directed','int','1')

	logging.debug('\tgraph element created.')
	return graph



# Write to file / screen / string
def rough_write(root,filename):
	tree = ET.ElementTree(root)
	tree.write(filename, xml_declaration=True, encoding='utf-8', method='xml')
	logging.info('  xgml tree written')
	return 0

def pretty_write(root,filename):
	tree = DT.parseString(ET.tostring(root, 'utf-8'))
	string = tree.toprettyxml(indent='\t')
	with open(filename,'w') as f:
		f.write(string)
	logging.info('  xgml tree written')
	return 0

def rough_print(root):
	print ET.tostring(root)
	return 0

def pretty_print(root):
	tree = DT.parseString(ET.tostring(root,'utf-8'))
	print tree.toprettyxml(indent='\t')
	return 0

def rough_string(root):
	return ET.tostring(root)

def pretty_string(root):
	tree = DT.parseString(ET.tostring(root,'utf-8'))
	return tree.toprettyxml(indent='\t')

# -------------------------------------------------------------------------------------

# MAIN
def main():	

	if len(sys.argv) > 1: 					# logging
   	 	loglevel = LOG.get(sys.argv[1], logging.NOTSET)
		logging.basicConfig(level=loglevel)
	else:
		logging.basicConfig(level=logging.CRITICAL)
	
												
	xgml = xml_to_xgml(f_model)				# parse xml model to xgml image	

	#rough_print(xgml)						# print xgml
	#pretty_print(xgml)
	#rough_write(xgml, f_image)				# write xgml
	pretty_write(xgml, f_image)

if __name__ == '__main__':									
	main()

