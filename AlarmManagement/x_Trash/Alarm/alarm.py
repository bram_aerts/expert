""" E'XperT project - alarm.py: Handle the alarm data from the terminal
"""
__author__ = "Shana Van Dessel"

import logging as lg
import sys

import alarmCsv as ac
import alarmProblog as ap

logModes = {'debug': lg.DEBUG,'info': lg.INFO,'warning': lg.WARNING,'error': lg.ERROR,'critical': lg.CRITICAL}

alarmInputFile = "../../Data/alarm_testfile0.csv"	# input data alarm logging
alarmOutputFile = "evidence.prob"				# output data alarm logging 

def Alarm(fcsv=alarmInputFile,fproblog=alarmOutputFile):
	print("Import alarm logging data from csv file (" + fcsv.split('/')[-1] + ") ...")
	data = ReadAlarmFromCsv(fcsv)
	print("Export alarm logging data to problog file (" + fproblog.split('/')[-1] + ") ...")
	WriteAlarmToProblog(fproblog,data)
	return 

def ReadAlarmFromCsv(file):
	""" Get alarm logging data from csv file. """
	lg.info("\n---------------------\n| CSV ALARM LOGGING |\n---------------------")
	data = ac.MakeAlarmCsv(file)
	return data 

def WriteAlarmToProblog(file,alarms):
	""" Convert alarm logging data to problog."""
	lg.info("\n---------------------\n| PROBLOG EVIDENCE  |\n---------------------")
	ap.MakeAlarmProblog(file,alarms)
	return 

def Logging():
	if len(sys.argv) > 1: 
		lg.basicConfig(level=logModes.get(sys.argv[1], lg.NOTSET))
	else:
		lg.basicConfig(level=lg.CRITICAL)
	return

def main():
	Logging()
	Alarm()
	print("Done.")
	
if __name__ == "__main__":									
	main()
