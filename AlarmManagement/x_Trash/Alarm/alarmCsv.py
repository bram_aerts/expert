""" E'XperT project - alarmCsv.py: Read the alarm logging data from CSV
"""
__author__ = "Shana Van Dessel"

import logging as lg
import collections

def MakeAlarmCsv(fcsv): 
	""" Get alarm logging data from csv file as a dictionary 
		with for each timestamp (key) a list off occuring alarms.
	"""
	data = collections.OrderedDict()
	with open(fcsv,"r") as f:
		f.readline()											# skip first line
		
		for l in f:
			date, time, alarm, ack = l.strip().split(";")		# split alarm log in date, time, alarm and acknowledge

			alarm = alarm.split(":")							# split the specific alarm in id (if there is any) and message
			if len(alarm)>1:
				alarmId = alarm[0].strip()
				alarmMsg = alarm[1].strip()
			else:
				continue
				#alarmId = None
				#alarmMsg = alarm[0].strip()

			if (date,time) not in data:							# create new key
				data[(date,time)] = list()
				lg.debug(" " + date + " " + time)
			data[(date,time)].append((alarmMsg,alarmId,ack))	# add alarm (id,msg,ack) to dict (key=(date,time))
			lg.info("  \t" + (alarmId + " " if alarmId is not None else "") + alarmMsg + " (" + ack + ")")
	return data
