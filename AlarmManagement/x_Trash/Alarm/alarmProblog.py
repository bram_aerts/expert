""" E'XperT project - alarmProblog.py: Convert (and write) the alarm logging data to Problog
"""
__author__ = "Shana Van Dessel"

import logging as lg
import time
import sys
sys.path.insert(0, '../Imports')
import toProblog as toProb

def MakeAlarmProblog(fproblog,data): 
	""" ...
	"""
	with open(fproblog,"w") as f:
		f.write(GetHeader(f.name.split('/')[-1],[__author__],"E'XperT project","Alarm logging"))
		f.write(GetEvidence(data))
	return 

def GetHeader(filename,authors,project,title):
	""" Return a problog header and title. """
	return toProb.Header(filename,authors,project) + toProb.Title(title)

def GetEvidence(data):	
	""" Convert the alarm logging data to problog evidence.
	"""
	alarmList = GetAlarmList(data)
	evidenceList = list()
	for x in alarmList:
		element = [str(y) for y in x]
		element.append("true")
		evidenceList.append(tuple(element))
	evidence = toProb.ListToEvidence("Alarm",evidenceList)	
	return evidence

def GetAlarmList(data):
	""" Create for each timestamp a list off active alarms based on the alarm logging data.
	"""
	alarmList = list()
	alarmsPerTimestamp = list()

	alarm = list(data.items())[0] 		# Get first key and value from dict
	date,time = alarm[0]				# Get only the key from this (which is a tuple in this case)
	its = GetInitTimestamp(date,time) -1
	nts = GetTimestamp(date,time,its)
	lg.debug(" Timestamp " + str(nts))

	for key in data:
		date,time = key
		ts = GetTimestamp(date,time,its)
		
		for alarm in data[key]:
			while ts > nts:				# Timestamps in which nothing changes
				alarmList.extend([(alarm[0],alarm[1],nts) for alarm in alarmsPerTimestamp])
				lg.info("  Alarm list (timestamp " + str(nts) + "): " + str([x for x in alarmList if x[2]==nts]))
				nts += 1
				lg.debug(" Timestamp " + str(nts))

			alarmsPerTimestamp = addAlarmToList(alarmsPerTimestamp,alarm)
		
	alarmList.extend([(alarm[0],alarm[1],nts) for alarm in alarmsPerTimestamp])
	lg.info("  Alarm list (timestamp " + str(nts) + "): " + str([x for x in alarmList if x[2]==nts]))
	return alarmList

def addAlarmToList(list,alarm):
	id, msg, ack = alarm
		
	if ack.lower() == "off":
		list.remove((id,msg))
		lg.debug(" \tAlarm removed " + str(alarm))

	elif ack.lower() == "on":
		list.append((id,msg))
		lg.debug(" \tAlarm added " + str(alarm))
	
	else:
		lg.debug(" \tAlarm acknowledged " + str(alarm))

	return list

def GetInitTimestamp(date,time):	
	epoch = GetTimestamp(date,time,0)
	return epoch

def GetTimestamp(dmy,hms,initEpoch):	
	date = dmy + " " + hms
	epoch = int(time.mktime(time.strptime(date,"%d/%m/%y %H:%M:%S"))) - initEpoch
	#print(" \tAlarm timestamp is " + str(epoch))
	return epoch
