""" E'XperT project - OTSA.py: Assembly of all code parts 
"""
__author__ = "Shana Van Dessel"

import Alarm/alarm as alarm

logModes = {'debug': lg.DEBUG,'info': lg.INFO,'warning': lg.WARNING,'error': lg.ERROR,'critical': lg.CRITICAL}

def logging():
	""" get logging (different levels)"""
	if len(sys.argv) > 1: 
		lg.basicConfig(level=logModes.get(sys.argv[1], lg.NOTSET))
	else:
		lg.basicConfig(level=lg.CRITICAL)
	return

def main():
	logging()
	alarm.Alarm()

if __name__ == "__main__":									
	main()
