#!/usr/bin/env python

"""handlingFiles.py: collection of all kind of functions to handle files."""
__author__ = "Shana Van Dessel"

import xlrd
import csv
import glob

FNUMBER = 0
PRINTX = False

# Merge several files into one file 
def mergeFiles(sourceFiles,destinationFile):
	skipped =0;
	fsrc = getFiles(sourceFiles)
	printX('merging files:')

	with open(destinationFile, 'w') as fdst:									# Open destination file
		for fnext in fsrc:
			if fnext == destinationFile:										# Don't merge destination file itself
				skipped +=1
				continue
			printX(fnext)
			with open(fnext) as f:												# Open source file
				f.readline()
				for line in f:
					if all(map(lambda x: x is '', line.rstrip().split(','))):	# Skip empty lines
						continue
					fdst.write(line)
	printX('merged file:', destinationFile, '(merged '+str(len(fsrc)-skipped)+' files)\n')
	return None

# Get (all) file(s) in a directory
# - path: "path\filename.extention" / "path\*.*"
def getFiles(path):
	return glob.glob(path)

def getFiles2(directory,filename='*',extention=''):
	return glob.glob(directory + filename + extention)

# Generate different filenames based on a default
# - filename: "filename.extention"
def generateFileName(filename):
	global FNUMBER
	parts = filename.split('.')
	newFilename = parts[0] + str(FNUMBER) + '.' + parts[1] 
	FNUMBER +=1
	printX(filename+' -> '+newFilename)
	return newFilename

# Convert excel to csv
def excelToCsv(fsrc, fdst, sheets=['Sheet1']):
	wb = xlrd.open_workbook(fsrc)
	f = open(fdst, 'w')
	for s in sheets:
		sh = wb.sheet_by_name(s)
		wr = csv.writer(f, quoting=csv.QUOTE_ALL)
		for nr in xrange(sh.nrows):
			wr.writerow(sh.row_values(nr))
	f.close()
	printX(fsrc+' -> '+fdst)
	return None

def printX(*strings):
	global PRINTX
	if not PRINTX:
		return None
	for s in strings:
		print s
	return None



