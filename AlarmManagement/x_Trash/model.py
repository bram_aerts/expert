""" E'XperT project - model.py: Parse terminal model (XML) to Problog input 
"""
__author__ = "Shana Van Dessel"

from bs4 import BeautifulSoup

class XmlModel(object):
	def __init__(self,root):
		self.root = root
	def getComponents(self,kind,comp,attr=list()):
		components = dict()		
		if kind is not None:
			xmlComponents = self.root.find(kind).find(comp)
		else:
			xmlComponents = self.root.find(comp)	
		for x in xmlComponents:
			component = x.find('TechnicalId').text
			components[component] = None
			if len(attr) == 0:
				continue
			if len(attr) == 1:
				for y in x.find(attr):
					components[component].append(y.find('TechnicalId').text)
				continue
			for a in attr:
				components[component][a] = None
				for y in x.find(a):
					components[component][a].append(y.find('TechnicalId').text)	
		print components		
		return components
	def getTanks(self):
		tanks = getComponents('equipment','Tanks',['Connections'])
		return tanks
	def getValves(self):
		valves = getComponents('equipment','Valves')
		return valves
	def getQuays(self):
		quays = getComponents('equipment','ProductTransferConnections')
		return quays	
	def getPumps(self):	
		pumps = getComponents('equipment','Pumps',['In','Out'])
		return pumps
	def getPipelines(self):
		pipes = getComponents(None,'pipelines',['Connections'])
		return pipes
	def __str__(self):
		string = self.__class__.__name__ + ': \n' + str(self.root)
		return string

class Model(object):
	def __init__(self):
		self.tanks = Components()
		self.tankConnectors = Components()
		self.valves = Components()
		self.pumps = Components()
		self.quays = Components()
		self.lines = Components()	
		self.connections = Components()
	def getModelByIds(self):
		components = dict()
		components['tanks'] = self.tanks.getComponentIds()
		components['tankConnectors'] = self.tankConnectors.getComponentIds()
		components['valves'] = self.valves.getComponentIds()	
		components['pumps'] = self.pumps.getComponentIds()
		components['quays'] = self.quays.getComponentIds()
		components['lines'] = self.lines.getComponentIds()
		components['connections'] = self.connections.getComponentIds()		
		return components	
		
class Components(object):
	def __init__(self):						# just a list of components
		self.list = list()
	def getComponentIds(self):				# get all components by id's
		components = list()
		for x in self.list:
			components.append(x.getComponentId)
		return components
	def getComponentsByIds(self,ids):		# get components by id
		components = list()
		for i in ids:
			for j in self.list:
				if j.id == i:
					components.append(j)
		return components
	def __str__(self):
		string = self.__class__.__name__ +  ': \n'
		for x in self.list:
			string += '\t' + str(x) + '\n' 
		return string

class Component(object):
	def __init__(self,id):
		self.id = id
	def getComponentId(self):
		return self.id
	def __str__(self):
		return self.__class__.__name__ + ' ' + str(self.id)

class Pipeline(Component):
	def __init__(self,id):
		super(Pipeline,self).__init__(id)	

class Tank(Component):
	def __init__(self,id):
		super(Tank,self).__init__(id)	

class TankConnector(Component):
	def __init__(self,id):
		super(TankConnector,self).__init__(id)
	
class Quay(Component):
	def __init__(self,id):
		super(Quay,self).__init__(id)

class Valve(Component):
	def __init__(self,id):
		super(Valve,self).__init__(id)
	
class Pump(Component):
	def __init__(self,id,pin,pout):
		super(Pump,self).__init__(id)
		self.input = pin
		self.output = pout
	def getComponentId(self):
		return (self.id, self.input, self.output)
	
	def __str__(self):
		string = self.__class__.__name__ + ' ' + str(self.id) + ' (' + str(self.input) + '/' + str(self.output) + ')' 
		return string	
		
class Connection(Component):
	def __init__(self,id,c1,c2):
		super(Connection,self).__init__(id)
		self.connected1 = c1
		self.connected2 = c2
	def getComponentId(self):
		return (self.id, self.connected1, self.connected2)
	def __str__(self):
		string = self.__class__.__name__ + ' ' + str(self.id) + ' (' + str(self.connected1) + '--' + str(self.connected2) + ')' 
		return string		


















def id2prob(identifier):
	return identifier.lower().replace("","_")


def parse_xml(xml_file):
	soup = BeautifulSoup(open(xml_file), "xml")				# open xml met xml parser
	fout = open(prob_file, "w")						# open outputfile

	# TANKS ( <Tank> > tank(tid). tankpit(tid, pit). tankConnector(cid). connection(tid, cid). )
	fout.write("% Tanks\n")
	try:
		tanks = soup.terminal.Component.Tanks.findChildren("Tank")		
		for tank in tanks:					
			tid = tank.TechnicalId.text					
			pit = tank.PitName.text	
			string = "tank(" + id2prob(tid) + ").\n"
			string += "tankpit(" + id2prob(tid) + ',' + id2prob(pit) + ").\n"
			fout.write(string)

			try:
				cons = tank.Connections.findChildren("TankConnection")			
				for con in cons:
					cid = con.TechnicalId.text
					string = "tankConnector(" + id2prob(cid) + ").\n" 	
					fout.write(string)	
					string = "connection(" + id2prob(tid) + id2prob(cid) + ", " + id2prob(tid) + "," + id2prob(cid) + ").\n" 	
					fout.write(string)	

			except AttributeError:
				print "Warning: unable to parse tankconnection"
							
		fout.write("\n")

	except AttributeError:
		print "Warning: unable to parse tank"

	# PUMPS ( <Pump> > pump(pid, cidf, cidt). )
	fout.write("% Pumps\n")
	try:
		pumps = soup.terminal.Component.Pumps.findChildren("Pump")			
		for pump in pumps:					
			pid = pump.TechnicalId.text					
			cidf = pump.In.TechnicalId.text	
			cidt = pump.Out.TechnicalId.text	
			string = "pump(" + id2prob(pid) + "," + id2prob(cidf) + "," + id2prob(cidt) + ").\n"		
			fout.write(string)
		fout.write("\n")

	except AttributeError:
		print "Warning: unable to parse pump"

	# VALVES ( <Valve> > valve(vid). )
	fout.write("% Valves\n")
	try:
		valves = soup.terminal.Component.Valves.findChildren("Valve")	
		for valve in valves:			
			vid = valve.TechnicalId.text					
			string = "valve(" + id2prob(vid) + ").\n" 		
			fout.write(string)
		fout.write("\n")

	except AttributeError:
		print "Warning: unable to parse valve"

	# PRODUCTTRANSFERCONNECTIONS ( <ProductTransferConnection> > productTransferConnection(pid). )
	fout.write("% Product transfer connections\n")
	try:
		ptcs = soup.terminal.Component.ProductTransferConnections.findChildren("ProductTransferConnection")	
		for ptc in ptcs:			
			pid = ptc.TechnicalId.text					
			string = "ptc(" + id2prob(pid) + ").\n" 		
			fout.write(string)
		fout.write("\n")

	except AttributeError:
		print "Warning: unable to parse productTransferConnection"

	# INSTRUMENTS ( <MassflowMeter>, <PressureSensor>, ... > instrument(iid, type, cid). )	
	fout.write("% Instruments\n")
	instrumentList = list(set([x.name for x in soup.terminal.Component.Instruments.findChildren(recursive=False)]))
	for i in instrumentList:	
		try:
			instruments = soup.terminal.Component.Instruments.findChildren(i)		
	
			for instrument in instruments:		
				iid = instrument.TechnicalId.text
				cid = instrument.InstrumentConnection.TechnicalId.text
				string = "instrument(" + id2prob(iid) + "," + id2prob(i) + "," + id2prob(cid) + ").\n"		
				fout.write(string)

		except AttributeError:
			print "Warning: unable to parse instrument"

	fout.write("\n")
	
	# PIPELINES ( <Pipeline> > pipe(pid). )
	fout.write("% Pipelines\n")
	try:
		pipes = soup.terminal.pipelines.findChildren("Pipeline")	
		for pipe in pipes:			
			pid = pipe.TechnicalId.text					
			string = "pipe(" + id2prob(pid) + ").\n" 		
			fout.write(string)

			try:
				cons = pipe.Connections.findChildren("Connection")
			
				for con in cons:
					cid = con.TechnicalId.text	
					string = "connection(" + id2prob(pid) + "," + id2prob(cid) + ").\n" 	
					fout.write(string)
	
			except AttributeError:
				print "Warning: unable to parse pipe"

	except AttributeError:
		print "Warning: unable to parse pipes"

# MAIN
def main():																
	parse_xml(xml_file)

if __name__ == "__main__":									
	main()
	
