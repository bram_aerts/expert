""" E'XperT project - modelProblog.py: Convert (and write) the terminal model data to Problog
"""
__author__ = "Shana Van Dessel"

import logging as lg
import sys
sys.path.insert(0, '..')
import toProblog as toProb

def MakeModelProblog(fproblog,model): 
	""" First provide a header and title for the problog file and (respectively) write these to the file.
  		Then convert the terminal model to problog code based on the python representation (PythonModel())
		and writes these rules to the file per component type.
	"""
	with open(fproblog,"w") as f:
		f.write(GetHeader(f.name.split('/')[-1],[__author__],"E'XperT project","Terminal model"))

		f.write(GetTanks(model))
		f.write(GetTankConnectors(model))
		f.write(GetQuays(model))
		f.write(GetValves(model))
		f.write(GetPumps(model))
		f.write(GetPipes(model))
		f.write(GetConnections(model))
		f.write(GetInstruments(model))
	return 

def GetHeader(filename,authors,project,title):
	""" Return a problog header and title. """
	return toProb.Header(filename,authors,project) + toProb.Title(title)
	
def GetComponents(name,list):
	""" Return a string containing a list of components in the right (Problog) syntax.
	"""
	lg.info("  Set " + name + "s...")
	lg.debug("\t" + str(list))
	return toProb.DictToPredicates(dict({name:list})) + "\n"

def GetTanks(model):
	""" Return a string containing all tanks in Problog syntax.
 		--> tank(id).
	"""
	tanks = [list([x.id]) for x in model.tanks.list]
	return GetComponents("tank",tanks)

def GetTankConnectors(model):
	""" Return a string containing all tank connectors in Problog syntax.
 		--> tankConnector(id).
	"""
	tankConns = [list([x.id]) for x in model.tankconns.list]
	return GetComponents("tankConnector",tankConns)
	
def GetQuays(model):
	""" Return a string containing all quays in Problog syntax.
 		--> quay(id).
	"""
	quays = [list([x.id]) for x in model.quays.list]
	return GetComponents("quay",quays)

def GetValves(model):
	""" Return a string containing all valves in Problog syntax.
 		--> valve(id).
	"""
	valves = [list([x.id]) for x in model.valves.list]
	return GetComponents("valve",valves)

def GetPumps(model):
	""" Return a string containing all pumps in Problog syntax.
 		--> pump(id).
	"""
	pumps = list()
	for x in model.pumps.list:
		pump = [x.id]
		pump.extend([y.id for y in model.pumpconns.list if (x.id == y.parent)])
		pumps.append(pump)

	return GetComponents("pump",pumps)
	
def GetPipes(model):
	""" Return a string containing all pipes in Problog syntax.
 		--> pipe(id).
	"""	
	pipes = [list([x.id]) for x in model.pipes.list]
	return GetComponents("pipe",pipes)

def GetConnections(model):
	""" Return a string containing all connections in Problog syntax.
 		--> connection(id,firstComponent,secondComponent).
	"""
	y = [y.id for y in model.pumps.list]
	conns = [list([x.id,x.comp1,x.comp2]) for x in model.connections.list 
											if (x.comp1 not in y and x.comp2 not in y)]
	return GetComponents("connection",conns)

def GetInstruments(model):
	""" Return a string containing all instruments in Problog syntax.
 		--> instrument(id,type,component).
	"""
	lg.info("  Set instruments... (TODO)")	
	string = \
"""instrument(t1_s,sensor,t1).	
instrument(xv101_s,sensor,xv101).
instrument(xv102_s,sensor,xv102).
instrument(xv103_s,sensor,xv103).
instrument(xv104_s,sensor,xv104).
instrument(xv105_s,sensor,xv105).
instrument(xv301_s,sensor,xv301).
instrument(xv302_s,sensor,xv302).
instrument(xv303_s,sensor,xv303).
instrument(xv304_s,sensor,xv304).
instrument(xv305_s,sensor,xv305).
instrument(xv306_s,sensor,xv306).
instrument(xv307_s,sensor,xv307).
"""
	return string
