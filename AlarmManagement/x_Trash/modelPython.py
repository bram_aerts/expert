""" E'XperT project - modelPython.py: Create an object which contains all info about the terminal model
"""
__author__ = "Shana Van Dessel"

import logging as lg

import sys
sys.path.insert(0, '..')
import toProblog as toProb

uniqueId = 0

def MakeModelPython(data): 
	""" Create an object of the class ModelPython to hold all data from the XML terminal model.
		The class automatically fills in its structure based on the given XML model (if given).
	"""
	model = ModelPython(data)
	return model

def createId():
	""" return a unique number as string 
	"""
	global uniqueId 
	uniqueId += 1
	return str(uniqueId)
	
class ModelPython(object):
	""" ModelPython contains all terminal data in the form of a list of components per component type. 
	"""
	def __init__(self,xmlModel=None):
		self.tanks = Components()
		self.tankconns = Components()
		self.valves = Components()
		self.pumps = Components()
		self.pumpconns = Components()
		self.quays = Components()
		self.pipes = Components()	
		self.connections = Components()
		if xmlModel is not None:
			self.setModel(xmlModel)

	def setModel(self,allComponents):
		""" Add all terminal components to the model.
		"""
		lg.info("  Set Python model")
		self.setTanks(allComponents['tanks'])
		self.setValves(allComponents['valves'])
		self.setQuays(allComponents['quays'])
		self.setPumps(allComponents['pumps'])
		self.setPipes(allComponents['pipes'])
		return

	def setTanks(self,tanks):
		""" Add tanks, tankconnectors and connections to the model.
		"""
		lg.info("  Set tanks...")
		self.setComponents(tanks,self.tanks,['Connections'],self.tankconns)
		l = len(self.tanks.list)
		lg.info(" \t" + str(l) + " tank" + ("s" if l>1 else "") + " found")
		l = len(self.tankconns.list)
		lg.info(" \t" + str(l) + " tank connection" + ("s" if l>1 else "") + " found")
		return

	def setValves(self,valves):
		""" Add valves to the model.
		"""
		lg.info("  Set valves...")
		self.setComponents(valves,self.valves)
		l = len(self.valves.list)
		lg.info(" \t" + str(l) + " valve" + ("s" if l>1 else "") + " found")
		return

	def setPumps(self,pumps):
		""" Add pumps, pumpconnectors and connections to the model.
		"""
		lg.info("  Set pumps...")
		self.setComponents(pumps,self.pumps,['In','Out'],self.pumpconns)
		l = len(self.pumps.list)
		lg.info(" \t" + str(l) + " pump" + ("s" if l>1 else "") + " found")
		l = len(self.pumpconns.list)
		lg.info(" \t" + str(l) + " pump connection" + ("s" if l>1 else "") + " found")
		return

	def setQuays(self,quays):
		""" Add quays to the model.
		"""
		lg.info("  Set quays...")
		self.setComponents(quays,self.quays)
		l = len(self.quays.list)
		lg.info(" \t" + str(l) + " quay" + ("s" if l>1 else "") + " found")
		return

	def setPipes(self,pipelines):
		lg.info("  Set pipes...")
		""" Add pipes and connections to the model.
		"""
		self.setComponents(pipelines,self.pipes,['Connections'])
		l = len(self.pipes.list)
		lg.info(" \t" + str(l) + " pipe" + ("s" if l>1 else "") + " found")
		return

	def setComponents(self,comps,kind=None,items=list(),kindconn=None):
		""" Add components (and their connectors and connections) to the model.
		"""
		for x in comps:
			lg.debug(" \t" + x)
			if kind is not None: 
				kind.list.append(Component(x))
			for i in items:
				for y in comps[x][i]:
					if kindconn is not None: 
						lg.debug(" \t  " + y)
						kindconn.list.append(Connector(y,x))
					self.connections.list.append(Connection(x,y))
					lg.debug(" \t    " + self.connections.list[-1].id)
		return

	def __str__(self):
		return self.__class__.__name__ + ': \n' + str(self.tanks) + str(self.tankconns) + str(self.valves) + str(self.pumps) + str(self.pumpconns) + str(self.quays) + str(self.pipes)

class Components(object):
	""" Components is a list of items of the class Component.
	"""
		
	def __init__(self):
		self.list = list()

	def getComponentById(self,id):
		""" Get a component by its ID.
		"""
		return [x for x in self.list if x.id == id]

	def getComponentsById(self,ids):
		""" Get a list of components by their ID's.
		"""
		y = list()
		for x in ids:
			y.append(self.getComponentById(x))
		return y

	def __str__(self):
		string = self.__class__.__name__ +  ': \n'
		for x in self.list:
			string += '\t' + str(x) + '\n' 
		return string
		#return ",".join(self.list)

class Component(object):
	""" Component represents an item (from the terminal) by its name or ID.
	"""
	def __init__(self,id):
		self.id = toProb.StringToProblog2(id)
						
	def __str__(self):
		return self.__class__.__name__ + ': ' + str(self.id)
		#return str(self.id)

class Connector(Component):
	""" Connector represents an item which serves as connection point for a component.
		It contains an own ID and the ID of the component it serves.
	"""
	def __init__(self,id,pid):
		super(Connector,self).__init__(id)
		self.parent = toProb.StringToProblog2(pid)

	def __str__(self):
		return self.__class__.__name__ + ': ' + str(self.id) + ' (' + self.parent + ')'
		#return str(self.id) + "," + str(self.parent)
		
class Connection(Component):
	""" Component represents the physical connection between two components.
		It contains an ID and the two components it connects.
	"""
	def __init__(self,i,j):
		super(Connection,self).__init__(createId())
		self.comp1 = toProb.StringToProblog2(i)
		self.comp2 = toProb.StringToProblog2(j)

	def __str__(self):
		return self.__class__.__name__ + ': ' + str(self.id) + ' (' + self.comp1 + ',' + self.comp2 + ')'
		#return str(self.id) + "," + str(self.comp1) + "," + str(self.comp2)
