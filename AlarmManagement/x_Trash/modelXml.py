""" E'XperT project - modelXml.py: Read the terminal model data from XML
"""
__author__ = "Shana Van Dessel"

import logging as lg
import xml.etree.ElementTree as et

def MakeModelXml(fxml): 
	""" Create an object of the class ModelXml to hold the root of the xml tree in which all terminal data 
		is present. The class contains functions to retrieve different parts of the terminal data stored 
		in the xml tree and an overarching function to retrieve all data.
	"""
	model = ModelXml(et.parse(fxml).getroot())
	data = model.getData()
	return data

class ModelXml(object):
	""" ModelXml holds the root of the xml tree in which all terminal data is present 
		and allows to retrieve this data by component or all at once. 
	"""

	def __init__(self,root):
		self.root = root
	
	def getData(self):
		""" Get all components from the xml model.
		"""
		lg.info("  Get XML model")
		model = dict()
		model['tanks'] = self.getTanks()
		model['valves'] = self.getValves()
		model['quays'] = self.getQuays()
		model['pumps'] = self.getPumps()
		model['pipes'] = self.getPipelines()
		return model

	def getTanks(self):
		""" Get all tanks and their connections from the xml model.
		"""
		return self.getComponents('equipment','Tanks',['Connections'])

	def getValves(self):
		""" Get all valves from the xml model. 
		"""
		return self.getComponents('equipment','Valves')

	def getQuays(self):
		""" Get all quays (producttransferconnections) from the xml model. 
		"""
		return self.getComponents('equipment','ProductTransferConnections')	

	def getPumps(self):	
		""" Get all pumps and their in- and output from the xml model.
		"""
		return self.getComponents('equipment','Pumps',['In','Out'])

	def getPipelines(self):
		""" Get all pipelines and their connections from the xml model. 
		"""
		return self.getComponents(None,'pipelines',['Connections'])

	def getComponents(self,kind,comp,attr=list()):
		""" Get a specific type of component from the xml tree. 
		"""
		lg.info("  Get " + comp + "... ")
		if kind is not None:
			xmlComponents = self.root.find(kind).find(comp)
		else:
			xmlComponents = self.root.find(comp)

		if attr == []:
			lg.debug(" \tSearch for " + comp)
			components = self.getComponentsAsList(xmlComponents)
		else:
			lg.debug(" \tSearch for " + comp + "and " + ', '.join(attr))
			components = self.getComponentsAsDict(xmlComponents,attr)

		lg.info(" \t" + str(len(components)) + " " + comp +" found")
		return components
				
	def getComponentsAsList(self,xml):	
		""" Get components as a simple list. 
		"""	
		components = list()	
		for x in xml:
			c = x.find('TechnicalId').text
			components.append(c)
			lg.debug(" \t" + c)
		return components	
			
	def getComponentsAsDict(self,xml,attr):
		""" Get components as keys in a dict, with as value its attributes. 
			Which are again a dict with as key the name of the attributes and as value the attributes themselves. 
		"""
		components = dict()
		for x in xml:
			c = x.find('TechnicalId').text
			components[c] = dict()	
			lg.debug(" \t" + c)
			for y in attr:
				components[c][y] = list()	
				for z in x.findall(".//"+y):
					for u in z.findall('.//TechnicalId'):
						ca = u.text
						components[c][y].append(ca)
						lg.debug(" \t  " + ca)		
		return components

	def __str__(self):
		string = self.__class__.__name__ + ': \n' + str(self.root)
		return string

