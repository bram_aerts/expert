#########################################################

# E'XperT project					# 

#########################################################

# Description:   Terminal model (XML) to Problog parser	#

# Author(s): 	 Shana Van Dessel  Bram Aerts		#

# Document:	 model_parser.py			#

#########################################################



# IMPORTS

from bs4 import BeautifulSoup



# PREDEFINED VALUES

xml_file = "model.xml"

prob_file = "model.prob"



# FUNCTIONS

def id2prob(identifier):

	return identifier.lower().replace("","_")



def parse_xml(xml_file):

	soup = BeautifulSoup(open(xml_file), "xml")				# open xml met xml parser

	fout = open(prob_file, "w")						# open outputfile



	# TANKS ( <Tank> > tank(tid). tankpit(tid, pit). tankConnector(cid). connection(tid, cid). )

	fout.write("% Tanks\n")

	try:

		tanks = soup.terminal.equipment.Tanks.findChildren("Tank")		

		for tank in tanks:					

			tid = tank.TechnicalId.text					

			pit = tank.PitName.text	

			string = "tank(" + id2prob(tid) + ").\n"

			string += "tankpit(" + id2prob(tid) + ',' + id2prob(pit) + ").\n"

			fout.write(string)



			try:

				cons = tank.Connections.findChildren("TankConnection")			

				for con in cons:

					cid = con.TechnicalId.text

					string = "tankConnector(" + id2prob(cid) + ").\n" 	

					fout.write(string)	

					string = "connection(" + id2prob(tid) + id2prob(cid) + ", " + id2prob(tid) + "," + id2prob(cid) + ").\n" 	

					fout.write(string)	



			except AttributeError:

				print "Warning: unable to parse tankconnection"

							

		fout.write("\n")



	except AttributeError:

		print "Warning: unable to parse tank"



	# PUMPS ( <Pump> > pump(pid, cidf, cidt). )

	fout.write("% Pumps\n")

	try:

		pumps = soup.terminal.equipment.Pumps.findChildren("Pump")			

		for pump in pumps:					

			pid = pump.TechnicalId.text					

			cidf = pump.In.TechnicalId.text	

			cidt = pump.Out.TechnicalId.text	

			string = "pump(" + id2prob(pid) + "," + id2prob(cidf) + "," + id2prob(cidt) + ").\n"		

			fout.write(string)

		fout.write("\n")



	except AttributeError:

		print "Warning: unable to parse pump"



	# VALVES ( <Valve> > valve(vid). )

	fout.write("% Valves\n")

	try:

		valves = soup.terminal.equipment.Valves.findChildren("Valve")	

		for valve in valves:			

			vid = valve.TechnicalId.text					

			string = "valve(" + id2prob(vid) + ").\n" 		

			fout.write(string)

		fout.write("\n")



	except AttributeError:

		print "Warning: unable to parse valve"



	# PRODUCTTRANSFERCONNECTIONS ( <ProductTransferConnection> > productTransferConnection(pid). )

	fout.write("% Product transfer connections\n")

	try:

		ptcs = soup.terminal.equipment.ProductTransferConnections.findChildren("ProductTransferConnection")	

		for ptc in ptcs:			

			pid = ptc.TechnicalId.text					

			string = "ptc(" + id2prob(pid) + ").\n" 		

			fout.write(string)

		fout.write("\n")



	except AttributeError:

		print "Warning: unable to parse productTransferConnection"



	# INSTRUMENTS ( <MassflowMeter>, <PressureSensor>, ... > instrument(iid, type, cid). )	

	fout.write("% Instruments\n")

	instrumentList = list(set([x.name for x in soup.terminal.equipment.Instruments.findChildren(recursive=False)]))

	for i in instrumentList:	

		try:

			instruments = soup.terminal.equipment.Instruments.findChildren(i)		

	

			for instrument in instruments:		

				iid = instrument.TechnicalId.text

				cid = instrument.InstrumentConnection.TechnicalId.text

				string = "instrument(" + id2prob(iid) + "," + id2prob(i) + "," + id2prob(cid) + ").\n"		

				fout.write(string)



		except AttributeError:

			print "Warning: unable to parse instrument"



	fout.write("\n")

	

	# PIPELINES ( <Pipeline> > pipe(pid). )

	fout.write("% Pipelines\n")

	try:

		pipes = soup.terminal.pipelines.findChildren("Pipeline")	

		for pipe in pipes:			

			pid = pipe.TechnicalId.text					

			string = "pipe(" + id2prob(pid) + ").\n" 		

			fout.write(string)



			try:

				cons = pipe.Connections.findChildren("Connection")

			

				for con in cons:

					cid = con.TechnicalId.text	

					string = "connection(" + id2prob(pid) + "," + id2prob(cid) + ").\n" 	

					fout.write(string)

	

			except AttributeError:

				print "Warning: unable to parse pipe"



	except AttributeError:

		print "Warning: unable to parse pipes"



# MAIN

def main():																

	parse_xml(xml_file)



if __name__ == "__main__":									

	main()

