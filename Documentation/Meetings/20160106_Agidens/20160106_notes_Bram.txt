Het alarm management/operator advice zoals het nu besproken is, bestaat uit de volgende delen.
1) Standaardiseren van acties ten gevolge van alarmen
	- Dit is moeilijk te leren want er is heel veel data voor nodig
	- Stel dat er iets geleerd is, en dit is fout, is dit de fout van het systeem (bijvoorbeeld als management iets afraadt, maar operator doet het toch)
	=> moet uit OTSA zelf procedures per fout worden opgesteld
2) Het aantal alarmen beheersbaar maken
	- Alles dat met UI (WINCC) te maken heeft ligt buiten onze scope (volgorde alarmen, prioriteiten)
	- Aantal alarmen verminderen
		
Algemene opmerkingen:
1) voor wie is het operator advice bedoeld? 
	- operator: nu is dit alarm aan de gang => los het zo op 
	- proces ingenieur: hier zit een groot probleem, fouten van het systeem in de long run proberen op te sporen
		* Grotere patronen in de data proberen te vinden.
		* vb: als deze pomp altijd in overdrive wordt gebruikt gaat ze sneller kapot
2) er moet een onderscheid gemaakt worden tussen 
	- veranderende delen van de terminal:
		* de operator weet niet goed wat hij met dit deel van de terminal moet
		* bovendien is het zeer moeilijk om hier te gaan leren, net omdat het zo veranderend is
	- statische delen van de terminal:
		* de (geoefende) operator weet wat hier gebeurt, hoe hij ermee moet omgaan
		* hierop is het echter wel mogelijk om te leren 


concrete zaken wat we zullen/moeten doen zijn:
1) predictief alarmen gaan opsporen
	- Wat wordt ermee gedaan: 
		* linken aan actie die gedaan moet worden 
		* weet gewoon wat er zal gebeuren en ben daarop voorbereid
	- Voorspelling op basis van METINGEN(procesvariabelen) op bepaalde ROUTES
	- Kanttekening: wanneer de operator predictieve alarmen krijgt, wordt het aantal alarmen juist groter, maar het aantal "ECHTE" alarmen dat gegenereerd wordt ligt lager, aangezien ze daartegen misschien al opgelost werden.

2) Een lijst opstellen van alle routes (niet alle technisch mogelijke, maar enkel die ooit worden gebruikt) en daarbij welke alarmen er op die route zouden kunnen voorkomen. 
Als er dan alarmen voorkomen die niet mogelijk zijn op de huidige routes, zijn die alarmen don't care. 
(Een uitbreiding hierbij zou kunnen zijn het product dat er verpompt wordt hier mee in rekening te brengen, bijvoorbeeld cavitatie in de pomp treedt niet op bij product X of iets dergelijks)

(3) Er bestaat een document (S18-02) waarin beschreven staat hoe met alarm management om te gaan, het is misschien handig dit eens te bekijken, aangezien hier al experts hebben over zitten nadenken over hoe het zou moeten.
