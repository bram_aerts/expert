#!/usr/bin/env python
# encoding: utf-8
"""
learnnoisyregex.py

Learn and perform inference on noisy regexes.

Created by Wannes Meert, Anton Dries.
Copyright (c) 2014 KU Leuven. All rights reserved.
"""

from __future__ import print_function
import sys
import re
from collections import defaultdict

def main(argv=None):
  import argparse

  params_str = """Possible parameters:
m_m, m_i, m_d, i_m, i_i, i_d, d_m, d_i, d_d
s_i, s_d, i_e, d_e
insertion_prob, deletion_prob, extrafix_prob, startenddeletion_prob
min_subseq_length, max_subseq_length
rm_perfect_subsets, rm_perfect_supersets
"""

  parser = argparse.ArgumentParser(\
            formatter_class=argparse.RawDescriptionHelpFormatter,
            description='Learn noisy regexes and match the patterns to data.',\
            epilog=params_str+'\n\nCopyright (c) 2013-2014 DTAI, KU Leuven.\nhttp://dtai.cs.kuleuven.be')
  parser.add_argument('--prune', type=int, default=0, help="Prune expression (experimental)")
  parser.add_argument('--noexternallib', action='store_true', help="Do not use external libraries stored in site-packages (slower)")
  parser.add_argument('--sort', action='store_true', help="Sort output according to probability")
  parser.add_argument('--subsequences', action='store_true', help="Compute probability for all subsequences")
  parser.add_argument('--mergeresults', action='store_true', help="Merge results for all test sequences")
  parser.add_argument('--testdfa', action='store_true', help="Test DFA on given test data")
#  parser.add_argument('--strings', action='store_true', help="Consider inputs as strings instead of files")
  parser.add_argument('-k', metavar='INT', type=int, default=2, help="k (for k-testable learning algorithm)")
  parser.add_argument('data_files', nargs='*', help="Data files (STDIN if no files given)")
  parser.add_argument('--testfile', metavar="FILE", action='append', help="Test data file (multiple allowed)")
  parser.add_argument('-i', '--ignore', metavar='EVENT', action='append', default=[], help="Ignore this event (multiple allowed)")
  parser.add_argument('--verbose', '-v', action='count', help='Verbose output (-vv for visualizing lattice, -vvv for datastructures)')
  parser.add_argument('--fromhmmfile', metavar='FILE', help='Match given HMM to test data')
  parser.add_argument('--fromregex', metavar='REGEX', help='Build a DFA from a given regex. NOTE: Incompatible with --testdfa')
  parser.add_argument('--fromregexfile', metavar='FILE', help='Build a DFA from the regex in a given file.')
  parser.add_argument('--params', help='Comma-separated list of transition probabilities (e.g. m_i=0.6,m_d=0.3).')
  parser.add_argument('--paramsfile', metavar='FILE', help='File with --params')
  parser.add_argument('--saveregex', action='store_true', help='Save regex to regex.out file (can be exponentially larger than DFA)')
  parser.add_argument('--freqtransprob', action='store_true', help='Transition probabilities based on frequencies')
  parser.add_argument('--onlytranscounts', action='store_true', help='Compute transition counts and stop')
  parser.add_argument('--drawoutliers', type=float, default=0, help='When outgoing/incomming edge < factor: indicate outlier in red')
  parser.add_argument('--outputfile', type=str, default="states_counts.dot", help="name of dot output file")
  args = parser.parse_args(sys.argv[1:])

  if args.noexternallib:
    sys.path = [p for p in sys.path if not "site-packages" in p]

  try:
    import logging
    logger = logging.getLogger("noisyregex")
  except ImportError:
    import noisyregex.simplelogging as logging
    logger = logging.getLogger("noisyregex")
    logger.warning('Using simple logging replacement')

  verbose = args.verbose
  logger = logging.getLogger("noisyregex")
  ch = logging.StreamHandler(sys.stdout)
  #ch.setLevel(logging.DEBUG)
  #formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
  formatter = logging.Formatter('[%(levelname)s] %(message)s')
  ch.setFormatter(formatter)
  logger.addHandler(ch)
  if args.verbose == None:
      logger.setLevel(logging.WARNING)
  elif args.verbose == 1:
      logger.setLevel(logging.INFO)
  else:
      logger.setLevel(logging.DEBUG)


  from noisyregex.util import learn_dfa, unknown_symbol, test_hmm, test_regex, read_data, read_hmm
  from noisyregex.re2dfa import convert
  from noisyregex.dfa2hmm import dfa2hmm, to_dot, computeCounts

  params = dict()
  if args.paramsfile:
    with open(args.paramsfile, 'r') as ifile:
      args.params = "".join(ifile.readlines()).strip()
      logger.debug("Read params from file: {}".format(args.params))
  if args.params:
    for kv in args.params.split(','):
      kvs = kv.split('=')
      if len(kvs) != 2:
        raise Exception("params should be comma-separated list of equalities, not '{}'".format(kv))
      params[kvs[0]] = float(kvs[1])

  fromregex = None
  if args.fromregex:
    fromregex = args.fromregex
  if args.fromregexfile:
    fromregex = ''
    with open(args.fromregexfile, 'r') as ifile:
      fromregex = ifile.read()
      #fromregex = re.sub(r"""[" \n\t]""", "", fromregex)

  if not args.fromhmmfile:
    if fromregex:
        ## Load a Regex from file and build a DFA
        print('Building DFA from regular expressions {}'.format(fromregex))
        dfa = convert(fromregex)
        dfa.unknown_symbol = unknown_symbol
        alphabet = list(dfa.alphabet)
        # print('Using alphabet: {}'.format(', '.join(alphabet)))
    else:
        ## Learn DFA from data
        logger.info('Learning DFA ...')
        dfa = learn_dfa(data_files = args.data_files,
                        ignore     = args.ignore,
                        k          = args.k)
        dfa.unknown_symbol = unknown_symbol
        alphabet = list(dfa.alphabet)

    with open('dfa.dot', 'w') as ofile:
      logger.info("DFA diagram: see {}".format(ofile.name))
      ofile.write(dfa.to_dot())

    if ((args.testfile and args.testdfa) or verbose > 0) and not fromregex and args.saveregex:
      # Convert to regex
      logger.info('Converting DFA to RegEx ...')
      import copy
      dfa2 = copy.deepcopy(dfa)
      regex = dfa2.solve(None)
      regex = regex.simplify()
      regexstr = regex.toString() # with additional whitespace
      logger.debug('Regular expression:\n{}'.format(regexstr))
      with open('regex.out', 'w') as ofile:
        ofile.write(regexstr)
        print('Regex: {}'.format(ofile.name))

      ## Test DFA
      if args.testfile and args.testdfa:
        test_regex(regex      = regex,
                   test_files = args.testfile,
                   prune      = (args.prune > 0),
                   alphabet   = dfa.alphabet,
                   unknown_symbol = dfa.unknown_symbol)
        print('... Done testing DFA\n')

    ## Conversion: DFA -> HMM
    if args.onlytranscounts:
        logger.info('Count DFA transitions ...')
        data = read_data(args.data_files)
        matrix, labels = dfa.get_transition_matrix()
        counts = computeCounts(matrix, labels, data, prune=args.prune)
        with open(args.outputfile, 'w') as f:
            print('State counts diagram: {}'.format(f.name))
            print(to_dot(counts, labels, draw_outliers=args.drawoutliers), file=f)
    else:
        logger.info('Conversion from DFA to HMM ...')
        data = None
        if args.freqtransprob:
            data = read_data(args.data_files)
        hmm = dfa2hmm(dfa          = dfa,
                      prune        = args.prune,
                      params       = params,
                      computefreqs = args.freqtransprob,
                      data         = data)

        ## Write HMM to parsable file
        with open('hmm.out','w') as ofile:
          hmm.dump(ofile)
          print("HMM values: {}".format(ofile.name))
  else:
    ## Skip learning and start from given HMM
    print('Using HMM stored in {}'.format(args.fromhmmfile))
    hmm, alphabet  = read_hmm(args.fromhmmfile)

  ## Test HMM
  if args.testfile:
    logger.info('Testing HMM ...')
    test_hmm(hmm          = hmm,
             dfa          = dfa,
             test_files   = args.testfile,
             sort         = args.sort,
             subsequences = args.subsequences,
             params       = params,
             merge        = args.mergeresults)


if __name__ == "__main__":
  sys.exit(main())
