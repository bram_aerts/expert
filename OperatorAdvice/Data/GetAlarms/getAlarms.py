""" Project: 	 E'XperT project
	File: 	 	 getAlarms.py
	Description: Convert the alarm data from the database into a usable format.
"""
__author__ = "Shana Van Dessel"

import sys, getopt, logging
loggingModes = {'debug': logging.DEBUG,'info': logging.INFO,'warning': logging.WARNING,'error': logging.ERROR,'critical': logging.CRITICAL}
import pandas as pd
import xml.etree.ElementTree as et
from datetime import datetime, timedelta

InputFile = '../dbAlarms.csv'		# Raw alarms (from database)
OutputFile = '../alarms.csv' 		# Processed alarms 

def GetAlarms(fin=InputFile, fout=OutputFile):
	''' Change the format of the alarms. '''
	logging.info("  Reading raw alarm data (from database)...")			# Read data
	df = pd.read_csv(fin,sep=',',usecols=['Timestamp','State','MessageTexts','CustomData'],encoding='utf-8-sig')
	logging.debug("\n{}".format(df[:10]))
	logging.info("  #alarms: {}".format(len(df)))
		
	logging.info("  Processing alarm data ...")							# Process data
	df = ConvertColumns(df)												# - Convert alarms
	logging.debug(" (convert columns)\n{}".format(df[:10]))
	df = FilterColumns(df)												# - Filter alarms
	logging.debug(" (filter columns)\n{}".format(df[:10]))
	logging.info("  #alarms: {}".format(len(df)))
	
	logging.info("  Writing alarm data ...")							# Write data
	df.to_csv(fout,index=False,encoding='utf-8')
	return df
		
def FilterColumns(df):
	df = df[df['Class'].map(FilterClass)]								# Only keep alarms of class 'error' and 'tolerance'
	df = df[df['Type'] != 'Commentaar']									# Remove alarms of type 'commentaar'
	df = df[df['State'] <= 2]											# Only keep states 1 ('on') and 2 ('off')
	return df
	
def FilterClass(s):
	return s in ['Error', 'Tolerance'] 
		
def ConvertColumns(df):
	df['Timestamp'] = df['Timestamp'].map(ConvertTimestamp)
	
	df['Message'] = df['MessageTexts'].map(GetMessage)					# MessageTexts (xml) --> Message
	df['Id'] = df['Message'].map(GetId)									# Message -> Id
	df.drop('MessageTexts', axis=1, inplace=True)
	
	df['Class'] = df['CustomData'].map(GetClass)						# CustomData (xml) --> Class
	df['Type'] = df['CustomData'].map(GetType)							# CustomData (xml) --> Type
	df.drop('CustomData', axis=1, inplace=True)
	return df

def ConvertTimestamp(s):
    d,t,x = s.split(' ')
    t = t.split('.')[0]										# Drop miliseconds
    dt = datetime.strptime(d+' '+t,'%Y-%m-%d %H:%M:%S')
    x = [-int(x) for x in x.split(':')]		
    tz_dst = timedelta(hours=x[0],minutes=x[1])				# Timezone and daylight savnings time
    dt += tz_dst                 							# Lose timezone and daylight savings time
    dt += timedelta(hours=1)   	 							# Add our timezone again (+1:00)
    return dt
	
def GetId(msg):
    parts = msg.split(':')
    if len(parts) > 1:
        return parts[0]
    return 'unknown'
	
def GetClass(customData):
	root = et.fromstring(customData)
	cd_class = root.find('CNS').find("CN[@LCID='1033']").text
	#logging.debug(" " + cd_class)
	return cd_class
	
def GetType(customData):
	root = et.fromstring(customData)
	cd_type = root.find('TNS').find("TN[@LCID='1033']").text
	#logging.debug(" " + cd_type)
	return cd_type
	
def GetMessage(messageText):
	root = et.fromstring(messageText)
	message = root.find("TB").text
	#logging.debug(" " + message)
	return message

def main(argv):
	inputFile = InputFile
	outputFile = OutputFile
	loggingMode = 'critical'
	try:
		opts, args = getopt.getopt(argv,"hi:o:l:",["help","ifile=","ofile=","lmode="])
	except getopt.GetoptError:
		print('getAcetateIds.py -i <inputfile> -o <outputfile> -l <loggingmode>')
		sys.exit(2)	
	for opt, arg in opts:
		if opt in ("-h","--help"):
			print('getAcetateIds.py -i <inputfile> -o <outputfile> -l <loggingmode>')
			sys.exit()
		elif opt in ("-i", "--ifile"):
			inputFile = arg
		elif opt in ("-o", "--ofile"):
			outputFile = arg
		elif opt in ("-l", "--lmode"):
			loggingMode = arg
	logging.basicConfig(level=loggingModes.get(loggingMode, logging.NOTSET))
	logging.info("  IN:  {}".format(inputFile))
	logging.info("  OUT: {}\n".format(outputFile))
	
	GetAlarms(inputFile,outputFile)
	logging.info("  Done.")
	
if __name__ == "__main__":
   main(sys.argv[1:])

