# Project:		E'XperT project
# Author:		S. Van Dessel
# File: 		getAcetateAlarms.ps1

echo "Get acetate alarm data ..."
$sw = [Diagnostics.Stopwatch]::StartNew()																	

[string]$fin = "C:\Users\shana\Documents\Expert\OperatorAdvice\Data_Acetate\GetAcetateAlarms\dbAlarms.csv"
[string]$fout = "C:\Users\shana\Documents\Expert\OperatorAdvice\Data_Acetate\acetateAlarms.csv"
[string]$fid = "C:\Users\shana\Documents\Expert\OperatorAdvice\Data_Acetate\acetateIds.csv"
#[string]$fout = "C:\Users\shana\Documents\Expert\OperatorAdvice\Data_Acetate\acetateAlarmsTr.csv"
#[string]$fid = "C:\Users\shana\Documents\Expert\OperatorAdvice\Data_Acetate\acetateIdsTr.csv"

echo "- Run SQL"
echo "Query database for acetate alarms ..."
#Invoke-Sqlcmd -ServerInstance DESKTOP-9HP69K3\HISTORIANSTORAGE -Database HistorianStorage -Query "SELECT [Timestamp],[State],[MessageTexts],[CustomData]  FROM [ALG].[VMessage]" | Export-Csv -NoType $fin
echo ''

echo "- Run PYTHON"
python C:\Users\shana\Documents\Expert\OperatorAdvice\Data_Acetate\GetAcetateAlarms\getAcetateAlarms.py -i $fin -o $fout -d $fid -l 'info' #'debug'

$sw.Stop()																										
echo "Retrieved acetate alarm data in $($sw.Elapsed.Days)d $($sw.Elapsed.Hours)h $($sw.Elapsed.Minutes)m." ""	
