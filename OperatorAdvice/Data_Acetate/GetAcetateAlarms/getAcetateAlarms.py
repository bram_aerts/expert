''' Project: 	 E'XperT project
	File: 	 	 getAcetateAlarms.py
	Description: Combine the alarm data with the component ID's of the acetate installation.
'''
__author__ = 'Shana Van Dessel'

import sys, getopt, logging
loggingModes = {'debug': logging.DEBUG,'info': logging.INFO,'warning': logging.WARNING,'error': logging.ERROR,'critical': logging.CRITICAL}
import pandas as pd
import xml.etree.ElementTree as et
from datetime import datetime, timedelta


IdFile = '../acetateIds.csv' 			# ID data
InputFile = 'dbAlarms.csv'				# alarm data
OutputFile = '../acetateAlarms.csv' 	# Combined data

def GetData(fid=IdFile,fin=InputFile,fout=OutputFile):
	''' Get the alarm data and combine with the ID data. '''
	logging.info("  Reading raw alarm data (from database output file)...")		# Read raw alarm data
	logging.debug(" {}".format(fin))
	df_alarm = pd.read_csv(fin,sep=',',usecols=['Timestamp','State','MessageTexts','CustomData'])
	logging.debug("\n{}".format(df_alarm.head(5)))
	logging.info("  #alarms: {}\n".format(len(df_alarm)))
	
	logging.info("  Processing alarm data ...")									# Process alarm data
	df_alarm = ConvertColumns(df_alarm)											# - Convert alarms
	logging.debug(" (convert columns)\n{}".format(df_alarm.head(5)))
	df_alarm = FilterColumns(df_alarm)											# - Filter alarms
	logging.debug(" (filter columns)\n{}".format(df_alarm.head(5)))
	logging.info("  #alarms: {}\n".format(len(df_alarm)))
	
	logging.info("  Reading ID data ...")										# Read ID data
	logging.debug(" {}".format(fid))
	df_id = pd.read_csv(fid,sep=',',usecols=['Id','Group'],encoding='utf-8-sig')			
	logging.debug("\n{}".format(df_id.head(5)))
	logging.info("  #ID's: {}\n".format(len(df_id)))
		
	logging.info("  Combining alarm and ID data...")							# Combine alarm and id data
	df = CombineData(df_alarm,df_id)		
	logging.debug("\n{}".format(df.head(5)))
	logging.info("  #alarms: {}\n".format(len(df)))
	
	logging.info("  Writing alarm data...")										# Write alarm data
	logging.debug(" {}\n".format(fout))
	df.to_csv(fout,index=False,encoding='utf-8')
	return 

# PROCESS DATA -----------------------------------------------------------------------------------------------------------------
def ConvertColumns(df):
	df['Timestamp'] = df['Timestamp'].map(ConvertTimestamp)
	df['Message'] = df['MessageTexts'].map(lambda x: et.fromstring(x).find("TB").text)							# MessageTexts (xml) -> Message
	df.drop('MessageTexts', axis=1, inplace=True)
	df['Id'] = df['Message'].map(lambda x: x.split(':')[0] if len(x.split(':')) > 1 else 'unknown')				# Message -> Id
	df['Class'] = df['CustomData'].map(lambda x: et.fromstring(x).find('CNS').find("CN[@LCID='1033']").text)	# CustomData (xml) --> Class
	df['Type'] = df['CustomData'].map(lambda x: et.fromstring(x).find('TNS').find("TN[@LCID='1033']").text)		# CustomData (xml) --> Type
	df.drop('CustomData', axis=1, inplace=True)
	return df	
	
def ConvertTimestamp(s):
	x = s.split(' ')
	x[-1] = ''.join(x[-1].split(':'))
	dt = datetime.strptime(' '.join(x),'%m/%d/%Y %I:%M:%S %p %z')
	return dt
	
def FilterColumns(df):
	df = df[df['Class'].map(lambda x: x in ['Error', 'Tolerance'] )]	# Only keep alarms of class 'error' and 'tolerance'
	df = df[df['Type'] != 'Commentaar']									# Remove alarms of type 'commentaar'
	df = df[df['State'] <= 2]											# Only keep states 1 ('on') and 2 ('off')
	return df
	
# COMBINE DATA -----------------------------------------------------------------------------------------------------------------	
def CombineData(df_alarm,df_id):
	''' Filter the alarm data on component ID's of the acetate installation. '''
	df = df_alarm[df_alarm['Id'].map(lambda x: x in list(df_id['Id']))]
	df['Group'] = df['Id'].map(lambda x: list(df_id[df_id['Id']==x]['Group'])[0])
	return df

def main(argv):
	idFile = IdFile
	inputFile = InputFile
	outputFile = OutputFile
	loggingMode = 'critical'
	try:
		opts, args = getopt.getopt(argv,"hi:o:d:l:",["help","ifile=","ofile=","idfile=","lmode="])
	except getopt.GetoptError:
		print('getAcetateAlarms.py -i <inputfile> -o <outputfile> -d <idfile> -l <loggingmode>')
		sys.exit(2)	
	for opt, arg in opts:
		if opt in ("-h","--help"):
			print('getAcetateAlarms.py -i <inputfile> -o <outputfile> -d <idfile> -l <loggingmode>')
			sys.exit()
		elif opt in ("-d", "--idfile"):
			idFile = arg
		elif opt in ("-i", "--ifile"):
			inputFile = arg
		elif opt in ("-o", "--ofile"):
			outputFile = arg
		elif opt in ("-l", "--lmode"):
			loggingMode = arg
	logging.basicConfig(level=loggingModes.get(loggingMode, logging.NOTSET))
	GetData(idFile,inputFile,outputFile)
	logging.info("  Done.")
	
if __name__ == "__main__":
   main(sys.argv[1:])

