# Project:		E'XperT project
# Author:		S. Van Dessel
# File: 		getAcetateIds.ps1

echo "Get acetate ID data ..."
$sw = [Diagnostics.Stopwatch]::StartNew()																	

[string]$fin = "C:\Users\shana\Documents\Expert\OperatorAdvice\Data_Acetate\GetAcetateIds\dbAcetateIds.csv"
[string]$fout = "C:\Users\shana\Documents\Expert\OperatorAdvice\Data_Acetate\acetateIds.csv"
#[string]$fin = "C:\Users\shana\Documents\Expert\OperatorAdvice\Data_Acetate\GetAcetateIds\dbAcetateIdsTr.csv"
#[string]$fout = "C:\Users\shana\Documents\Expert\OperatorAdvice\Data_Acetate\acetateIdsTr.csv"

echo "- Run SQL"
echo "Query database for acetate ID's ..."
Invoke-Sqlcmd -ServerInstance DESKTOP-9HP69K3\HISTORIANSTORAGE -Database HistorianStorage -Query "SELECT [Id],[GroupId] as [Group],[Min],[Max] FROM [VAcetateId]" | Export-Csv -NoType $fin
#Invoke-Sqlcmd -ServerInstance DESKTOP-9HP69K3\HISTORIANSTORAGE -Database HistorianStorage -Query "SELECT [Id],[GroupId] as [Group] FROM [VAcetateIdTr]" | Export-Csv -NoType $fin
echo ''

echo "- Run PYTHON"
python C:\Users\shana\Documents\Expert\OperatorAdvice\Data_Acetate\GetAcetateIds\getAcetateIds.py -i $fin -o $fout -l 'info' #'debug'

$sw.Stop()																										
echo "Retrieved acetate ID data in $($sw.Elapsed.Days)d $($sw.Elapsed.Hours)h $($sw.Elapsed.Minutes)m." ""	
