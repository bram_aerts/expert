''' Project: 	 E'XperT project
	File: 	 	 getAcetateIds.py
	Description: Convert the ID data from the database into a usable format.
'''
__author__ = "Shana Van Dessel"

import sys, getopt, logging, os
loggingModes = {'debug': logging.DEBUG,'info': logging.INFO,'warning': logging.WARNING,'error': logging.ERROR,'critical': logging.CRITICAL}
import pandas as pd

InputFile = "dbAcetateIds.csv"		# Raw ID's (from database)
OutputFile = "../acetateIds.csv" 	# Processed ID's

def GetIds(fin=InputFile,fout=OutputFile):
	''' Get the acetate ID data from the database output file. '''
	fn = os.path.splitext(fout)
	fout0 = fn[0].replace('Ids','IdTags') + fn[1]
	
	logging.info("  Reading raw ID data (from database output file)...")						# Read data
	logging.debug(" {}".format(fin))
	df = pd.read_csv(fin,sep=',',encoding='utf-8-sig')	
	logging.debug("\n{}".format(df.head(5)))
	logging.info("  #ID's: {}\n".format(len(df)))
		
	logging.info("  Processing ID data ...")													# Process data
	#df = df[df['Group'].map(lambda x: x in ['Bestaat niet (meer)','Geen Azijnzuur'])]			# - Filter ID's 
	#logging.debug(" (filter groups)\n{}".format(df.head(5)))
	df['Id'] = df['Id'].map(lambda x: x.replace('_','-'))										# - Convert ID's
	logging.debug(" (convert ID's)\n{}".format(df.head(5)))
	logging.info("  #ID's: {}\n".format(len(df)))
	
	logging.info("  Writing ID data ...")														# Write data
	logging.debug(" {}\n".format(fout0))
	df.to_csv(fout0,index=False,encoding='utf-8')
	
	logging.info("  Further processing ID data ...")											# Process data 2
	df['Id'] = df['Id'].map(lambda x: '-'.join(x.split('-')[:-1]))								# - Convert ID's
	if 'Min'in df.columns:
		df.drop(['Min','Max'],axis=1,inplace=True)
	logging.debug(" (convert ID's)\n{}".format(df.head(5)))
	df.drop_duplicates(inplace=True)
	logging.debug(" (remove duplicates)\n{}".format(df.head(5)))								# - Remove duplicates
	logging.info("  #ID's: {}\n".format(len(df)))
	
	logging.info("  Writing ID data ...")														# Write data 2
	logging.debug(" {}\n".format(fout))
	df.to_csv(fout,index=False,encoding='utf-8')
	return df

def main(argv):
	inputFile = InputFile
	outputFile = OutputFile
	loggingMode = 'critical'
	try:
		opts, args = getopt.getopt(argv,"hi:o:l:",["help","ifile=","ofile=","lmode="])
	except getopt.GetoptError:
		print('getAcetateIds.py -i <inputfile> -o <outputfile> -l <loggingmode>')
		sys.exit(2)	
	for opt, arg in opts:
		if opt in ("-h","--help"):
			print('getAcetateIds.py -i <inputfile> -o <outputfile> -l <loggingmode>')
			sys.exit()
		elif opt in ("-i", "--ifile"):
			inputFile = arg
		elif opt in ("-o", "--ofile"):
			outputFile = arg
		elif opt in ("-l", "--lmode"):
			loggingMode = arg
	logging.basicConfig(level=loggingModes.get(loggingMode, logging.NOTSET))
	GetIds(inputFile,outputFile)
	logging.info("  Done.")
	
if __name__ == "__main__":
   main(sys.argv[1:])
