# Project:		E'XperT project
# Author:		S. Van Dessel
# File: 		getProcessVariables.ps1

echo "Query database for process variables..."
$sw = [Diagnostics.Stopwatch]::StartNew()																	

#[string]$fin = "C:\Users\shana\Documents\Expert\OperatorAdvice\Data_Acetate\GetProcessVariables\dbProcessVariablesOneId.csv"
#[string]$fout = "C:\Users\shana\Documents\Expert\OperatorAdvice\Data_Acetate\processVariables.csv"
#$tags = sqlcmd -S DESKTOP-9HP69K3\HISTORIANSTORAGE -d HistorianStorage -Q "SELECT [VTag].[TagUid], [VTag].[TagName] FROM [VTag], [VAcetateId] WHERE [VTag].[TagName] = [VAcetateId].[Id]"

[string]$fin = "C:\Users\shana\Documents\Expert\OperatorAdvice\Data_Acetate\GetProcessVariables\dbProcessVariablesNextId.csv"
[string]$fout = "C:\Users\shana\Documents\Expert\OperatorAdvice\Data_Acetate\processVariablesTr.csv"
$tags = sqlcmd -S DESKTOP-9HP69K3\HISTORIANSTORAGE -d HistorianStorage -Q "SELECT [VTag].[TagUid], [VTag].[TagName] FROM [VTag], [VAcetateIdTr] WHERE [VTag].[TagName] = [VAcetateIdTr].[Id]"

$tags = $tags[2..($tags.length-3)]
$tags2 = [System.Collections.Generic.List[System.Object]] $tags
foreach($tag in $tags)
{
	if ($tag.split(' ')[1].EndsWith('EU'))
	{
		$tags2.Remove($tag)
	}
}
$tags = $tags2
foreach($tag in $tags)
{
	[int]$i = ([array]::indexof($tags,$tag))+1
	if($i -lt 27) { continue }

	[string]$i = $i
	[string]$j = $tags.length
	$i = $i.insert(0,' '*($j.length-$i.length))
	$id = $tag.split(' ')[0]
	echo "$i/$($tags2.length): $($tag.split(' ')[1])"
	Invoke-Sqlcmd -ServerInstance DESKTOP-9HP69K3\HISTORIANSTORAGE -Database HistorianStorage -Query "SELECT [Id], [GroupId], [Timestamp], [Value] FROM [VAcetateTag] WHERE [TagID] = '$id'" | Export-Csv -NoType $fin
	python C:\Users\shana\Documents\Expert\OperatorAdvice\Data_Acetate\GetProcessVariables\getProcessVariables.py -i $fin -o $fout
}

$sw.Stop()																										
echo "Retrieved and compressed process variables in $($sw.Elapsed.Days)d $($sw.Elapsed.Hours)h $($sw.Elapsed.Minutes)m." ""	
