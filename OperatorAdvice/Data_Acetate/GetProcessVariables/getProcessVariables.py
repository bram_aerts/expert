''' Project: 	 E'XperT project
	File: 	 	 getProcessVariablesOnChange.py
	Description: convert the database output to an on-change format (ID per ID, append to file).
'''
__author__ = 'Shana Van Dessel'

import sys, getopt, logging, os
loggingModes = {'debug': logging.DEBUG,'info': logging.INFO,'warning': logging.WARNING,'error': logging.ERROR,'critical': logging.CRITICAL}

import pandas as pd
from datetime import datetime, timedelta

InputFile = 'dbProcessVariablesOneId.csv'
OutputFile = '../processVariables.csv'
IdFile = "../acetateIdTags.csv"

def GetProcessVariables (fin=InputFile,fid=IdFile,fout=OutputFile):
	if os.stat(fin).st_size == 0:
		logging.info('  No input.')
		exit()
	
	fn = os.path.splitext(fout)
	fout = fn[0] + '_VISU' + fn[1]
	
	df = pd.read_csv(fin, parse_dates={'Time':['Timestamp']}, date_parser=DateParser)					# Read process variable data
	df.sort_values('Time',inplace=True)		
	logging.debug(' Process variables: \n{} {} \n'.format(df[:10],('\n...' if len(df)>10 else '') ))
	id = df['Id'][0]																					# Threshold
	logging.info('  ID: {}'.format(id))
	logging.debug(' Process variables: \n{} {} \n'.format(df[:10],('\n...' if len(df)>10 else '') ))
	
	df = df[df['Value'].diff() != 0]																	# On change	
	logging.debug(' On change: \n{} {} \n'.format(df[:10],('\n...' if len(df)>10 else '') ))
	
	
	if id.endswith('-EU'):	
		fout = fn[0] + '_EU' + fn[1]
		df_id = pd.read_csv(fid, header=None, usecols=[0,2,3], names=['Id','Min','Max'])				# - Read ID data
		df_id = df_id[df_id['Id']==id]
		df_id['Threshold'] = (df_id['Max']-df_id['Min'])/100
		logging.debug(" Component ID: \n{}\n".format(df_id[:10]))
		
		
		df['Threshold'] = df['Value'].apply(lambda x: int(x)//df_id['Threshold'] * df_id['Threshold'])	# - Threshold
		df = df[df['Threshold'].diff() != 0]
		df['Threshold'] = df['Threshold'].apply(lambda x: int(x/df_id['Threshold']))
		logging.debug(' On threshold: \n{} {} \n'.format(df[:10],('\n...' if len(df)>10 else '') ))
	
	with open(fout, 'a') as f:																			# Write data
		df.to_csv(f,index=False,encoding='utf-8',header=False) #,columns=['Time','Id','GroupId','Value'])
	return 
		
def DateParser(s):
    d,t,a,x = s.split(' ')
    t = t.split('.')[0]										# Drop miliseconds
    dt = datetime.strptime(d+' '+t+' '+a,'%m/%d/%Y %I:%M:%S %p')
    x = [-int(x) for x in x.split(':')]		
    tz_dst = timedelta(hours=x[0],minutes=x[1])				# Timezone and daylight savnings time
    dt += tz_dst                 							# Lose timezone and daylight savings time
    dt += timedelta(hours=1)   	 							# Add our timezone again (+1:00)
    return dt
	
def main(argv):
	inputFile = InputFile
	idFile = IdFile
	outputFile = OutputFile
	loggingMode = 'critical'
	try:
		opts, args = getopt.getopt(argv,"hi:d:o:l:",["ifile=","idfile=","ofile=","lmode="])
	except getopt.GetoptError:
		print('getProcessVariables.py -i <inputfile> -d <idfile> -o <outputfile> -l <logging mode>')
		sys.exit(2)	
	for opt, arg in opts:
		if opt == '-h':
			print('getProcessVariables.py -i <inputfile> -d <idfile> -o <outputfile> -l <logging mode>')
			sys.exit()
		elif opt in ("-i", "--ifile"):
			inputFile = arg
		elif opt in ("-d", "--idfile"):
			idFile = arg
		elif opt in ("-o", "--ofile"):
			outputFile = arg
		elif opt in ("-l", "--lmode"):
			loggingMode = arg
	logging.basicConfig(level=loggingModes.get(loggingMode, logging.NOTSET))
	logging.info("  IN:  {}, {}".format(inputFile,idFile))
	logging.info("  OUT: {}\n".format(outputFile))
	
	GetProcessVariables(inputFile,idFile,outputFile)
	logging.info('Done.')
	
if __name__ == "__main__":
   main(sys.argv[1:])
