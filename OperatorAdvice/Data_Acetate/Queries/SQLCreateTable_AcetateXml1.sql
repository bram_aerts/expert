/*
 * Create tables 'AcetateXML' and 'Acetate'
 */
 
USE [HistorianStorage]									-- select database
GO

/* Store xml tree in table 'AcetateXML' */

IF OBJECT_ID('[AcetateXML]') IS NOT NULL				-- remove table (if any)
    DROP TABLE [AcetateXML]

CREATE TABLE [AcetateXML]								-- create new table (to store XML tree)
(
	Id INT IDENTITY PRIMARY KEY,
	XMLData XML
)

INSERT INTO [AcetateXML](XMLData)						-- fill table (no clue how this works...)
	SELECT CONVERT(XML, BulkColumn) AS BulkColumn
	FROM OPENROWSET(BULK 'C:\Users\Shana\Documents\Expert\xml1.xml', SINGLE_BLOB) AS x

GO

SELECT * FROM [AcetateXML]								-- print content of new table

/* Store data (ID's) from xml tree in table 'Acetate' */

DECLARE @XML AS XML, @hDoc AS INT, @SQL NVARCHAR (MAX)	-- declare some variables

SELECT @XML = XMLData FROM [AcetateXML]					-- get full xml tree from table 

IF OBJECT_ID('[Acetate]') IS NOT NULL
    DROP TABLE [Acetate]

CREATE TABLE [Acetate]									-- create new table (to store the different id's)
(
	Id VARCHAR(32) PRIMARY KEY
)	

EXEC sp_xml_preparedocument @hDoc OUTPUT, @XML			-- store xml data in cache

INSERT INTO [Acetate]									-- fill table with id's
	SELECT Id
	FROM OPENXML(@hDoc, 'ConnectionInfo/equipment/items/item')
	WITH 
	(
		Id [varchar](32) 'Id'
	)

EXEC sp_xml_removedocument @hDoc						-- release xml data from cache

GO

SELECT Top 1000 FROM [Acetate]									-- print content of new table