/*
 * Create tables 'AcetateXML2' and 'Acetate2'
 */
 
USE [HistorianStorage]									-- select database
GO

/* Store xml tree in table 'AcetateXML2' */

IF OBJECT_ID('[AcetateXML2]') IS NOT NULL				-- remove table (if any)
    DROP TABLE [AcetateXML2]

CREATE TABLE [AcetateXML2]								-- create new table (to store XML tree)
(
	Id INT IDENTITY PRIMARY KEY,
	XMLData XML
)

INSERT INTO [AcetateXML2](XMLData)						-- fill table (no clue how this works...)
	SELECT CONVERT(XML, BulkColumn) AS BulkColumn
	FROM OPENROWSET(BULK 'C:\Users\Shana\Documents\Expert\xml2.xml', SINGLE_BLOB) AS x

GO

SELECT * FROM [AcetateXML2]								-- print content of new table

/* Store data (ID's) from xml tree in table 'Acetate2' */

DECLARE @XML AS XML, @hDoc AS INT, @SQL NVARCHAR (MAX)	-- declare some variables

SELECT @XML = XMLData FROM [AcetateXML2]				-- get full xml tree from table 

IF OBJECT_ID('[Acetate2]') IS NOT NULL
    DROP TABLE [Acetate2]
	
CREATE TABLE [Acetate2]									-- create new table (to store the different id's)
(
	Id VARCHAR(32) PRIMARY KEY,
	GroupId VARCHAR(32)
)	
	
EXEC sp_xml_preparedocument @hDoc OUTPUT, @XML			-- store xml data in cache

INSERT INTO [Acetate2]									-- fill table with id's and group id's
	SELECT Id, GroupId
	FROM OPENXML(@hDoc, 'ConnectionInfo/equipment/items/item')
	WITH 
	(
		Id [varchar](32) 'Id',
		GroupId [varchar](32) 'Group'
	)

EXEC sp_xml_removedocument @hDoc						-- release xml data from cache

GO

SELECT Top 1000 FROM [Acetate2]								-- print content of new table