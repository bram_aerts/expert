/* 
 * Create view 'VAcetateId' 
 */
 
USE [HistorianStorage]							-- select database
GO

IF OBJECT_ID('[VAcetateId]') IS NOT NULL		-- remove old view (if any)
    DROP VIEW [VAcetateId]
GO

CREATE VIEW [VAcetateId] AS						-- create view 'VAcetateId' from table 'AcetateX'
	--SELECT * FROM [Acetate]	
	SELECT * FROM [Acetate2]	
	--SELECT * FROM [Acetate3]
GO

SELECT * FROM [VAcetateId]						-- print content of new view