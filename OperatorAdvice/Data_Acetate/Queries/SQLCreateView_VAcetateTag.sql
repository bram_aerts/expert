/* 
 * Create view 'VAcetateTag' 
 */

USE [HistorianStorage]													-- select database
GO

IF OBJECT_ID('[VAcetateTag]') IS NOT NULL								-- remove old view (if any)
    DROP VIEW [VAcetateTag]
GO

CREATE VIEW [VAcetateTag] AS											-- create view 'VAcetateTag' from 'IS.VTagBrowsing' and 'VTag'
	SELECT z.[Id], z.[GroupId], y.[Timestamp], y.[ValueFloat] as Value
	FROM [VTag] as x, [TLG].[VTagValue] as y, [VAcetateId] as z							
	WHERE x.[TagUID] = y.[TagUID] and x.[TagName] = z.[Id]
GO

--SELECT TOP 1000 FROM [VAcetateTag]									-- print content of new view