/*
 * Create view 'VTag'
 */

CREATE VIEW [VTag] AS									-- create view 'VTag' from view 'TLG.VTagValue'
	SELECT DISTINCT [TagUID],[TagName]
	FROM [HistorianStorage].[IS].[VTagBrowsing]
GO

--SELECT * FROM [VTag]							-- print content of new view