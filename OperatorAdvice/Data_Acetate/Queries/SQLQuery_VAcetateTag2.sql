USE [HistorianStorage]									-- select database
GO

SELECT [Id],[GroupId] INTO #Table FROM [VAcetateId]		-- create temporary table with id's

DECLARE @CurrentId varchar(32)							-- declare variables
DECLARE @CurrentGroup varchar(32)

WHILE exists (SELECT * FROM #Table)						-- loop over id's (using the temporary table)
BEGIN
    SELECT @CurrentId = (SELECT TOP 1 [Id] FROM #Table)
	SELECT @CurrentGroup = (SELECT TOP 1 [GroupId] FROM #Table)

	SELECT @CurrentId, @CurrentGroup, x.[Timestamp], x.[ValueFloat] as Value
	FROM [TLG].[VTagValue] as x, [VTag] as y						
	WHERE x.[TagUID] = y.[TagUID] 
		and y.[TagName] = @CurrentId
		and x.[Timestamp] between '2015-12-13 00:00:00.2460000 +01:00' and '2015-12-14 00:00:00.2460000 +01:00' -- XXXXXXXXXXXXXXXXXXXXX

    DELETE #Table WHERE [Id] = @CurrentID
END

DROP TABLE #Table