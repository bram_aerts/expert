# from __future__ import division, print_function
# import wx
# import json
# import os
# import glob
# import re
#
#
# # class SearchableComboBox(wx.ComboBox):
# #     def __init__(self, parent, choices=(), style=0, **par):
# #         wx.ComboBox.__init__(self, parent, wx.ID_ANY, style=style | wx.CB_DROPDOWN, choices=choices, **par)
# #         self.choices = choices
# #         self.Bind(wx.EVT_TEXT, self.EvtText)
# #         self.Bind(wx.EVT_CHAR, self.EvtChar)
# #         self.Bind(wx.EVT_COMBOBOX, self.EvtCombobox)
# #         self.ignoreEvtText = False
# #
# #     def EvtCombobox(self, event):
# #         self.ignoreEvtText = True
# #         event.Skip()
# #
# #     def EvtChar(self, event):
# #         if event.GetKeyCode() == 8:
# #             self.ignoreEvtText = True
# #         event.Skip()
# #
# #     def EvtText(self, event):
# #         if self.ignoreEvtText:
# #             self.ignoreEvtText = False
# #             return
# #         currentText = event.GetString()
# #         print(currentText)
# #         found = False
# #         for choice in self.choices:
# #             if choice.lower().startswith(currentText.lower()):
# #                 # self.ignoreEvtText = True
# #                 self.SetValue(choice)
# #                 self.SetInsertionPoint(len(currentText))
# #                 self.SetMark(len(currentText), len(choice))
# #                 found = True
# #                 break
# #         if not found:
# #             event.Skip()
#
#
# class AlarmSelector(wx.Panel):
#     def __init__(self, parent, alarms):
#         self.parent = parent
#         super(AlarmSelector, self).__init__(parent, style=wx.BORDER_RAISED, size=(900, -1))
#
#         title_grid = wx.GridSizer(cols=2, rows=1)
#         title_grid.SetMinSize((self.GetSize()[0], -1))
#         title = wx.StaticText(self, label='Alarm')
#         title.SetFont(wx.Font(18, wx.DEFAULT, wx.NORMAL, wx.NORMAL))
#         save_button = wx.Button(self, label='Save')
#         save_button.Bind(wx.EVT_BUTTON, self.on_save)
#         title_grid.Add(title)
#         title_grid.Add(save_button, flag=wx.ALIGN_RIGHT)
#
#         self.alarm_list = alarms
#
#         self.alarm = None
#         self.preset = None
#
#         self.alarm_component = wx.ComboBox(self, choices=self.alarm_list, size=(250, -1))
#         self.alarm_component.Bind(wx.EVT_COMBOBOX, self.on_select_alarm)
#         self.alarm_component.Bind(wx.EVT_TEXT, self.filter_alarms)
#
#         self.preset_component = wx.ComboBox(self, choices=[], size=(250, -1))
#         self.preset_component.Bind(wx.EVT_COMBOBOX, self.on_select_preset)
#
#         self.balanced = wx.ComboBox(self, size=(150, -1), choices=['unbalanced', 'balanced'], value='unbalanced')
#         self.learner = wx.ComboBox(self, size=(150, -1), choices=['weka', 'scikit'], value='weka')
#
#         self.grid = wx.FlexGridSizer(rows=4, cols=6)
#
#         self.grid.Add(wx.StaticText(self, label='Alarm:', size=(75, -1)))
#         self.grid.Add(self.alarm_component)
#         self.grid.Add(wx.StaticText(self, size=(150, -1)))
#         self.grid.Add(wx.StaticText(self, size=(120, -1)))
#         self.grid.Add(self.balanced)
#         self.grid.Add(self.learner)
#
#         self.grid.Add(wx.StaticText(self, label='Preset:'))
#         self.grid.Add(self.preset_component)
#
#         whole_grid = wx.BoxSizer(wx.VERTICAL)
#         whole_grid.Add(title_grid)
#         whole_grid.Add(wx.StaticLine(self, style=wx.LI_HORIZONTAL, size=(self.GetSize()[0], 20)))
#         whole_grid.Add(self.grid)
#
#         self.SetSizer(whole_grid)
#         whole_grid.Fit(self)
#
#     def filter_alarms(self, event):
#         # options = sorted([option for option in self.alarm_list
#         #                   if re.findall(self.alarm_filter.Value, option, re.IGNORECASE)])
#         options = sorted([option for option in self.alarm_list
#                           if re.findall(self.alarm_component.Value, option, re.IGNORECASE)])
#         self.alarm_component.Set(options)
#
#     def update_choices_component(self):
#         path = os.path.join(os.getcwd(), 'presets', self.alarm)
#         choices = [x.replace(path + '/', '') for x in glob.glob(path + '/*')]
#
#         preset_component = wx.ComboBox(self, choices=choices, size=(250, -1))
#         preset_component.Bind(wx.EVT_COMBOBOX, self.on_select_preset)
#         self.grid.Replace(self.preset_component, preset_component)
#         self.preset_component = preset_component
#         self.grid.Layout()
#
#     def on_select_alarm(self, event):
#         self.preset = None
#         self.alarm = self.alarm_component.Value
#         self.update_choices_component()
#
#         self.parent.load_preset()
#
#     def on_select_preset(self, event):
#         self.preset = self.preset_component.Value
#         self.parent.load_preset()
#
#     def on_save(self, event):
#         name = self.parent.save_preset()
#         if not name:
#             return
#         self.update_choices_component()
#
#         self.preset = name
#         self.preset_component.SetValue(name)
#         self.parent.load_preset()
#
#     def get_balanced(self):
#         return self.balanced.GetValue()
#
#     def get_learner(self):
#         return self.learner.GetValue()
#
#     def get_alarm(self):
#         return self.alarm_component.GetValue()
#
#     def load_preset(self, path):
#         if path:
#             with open(path) as f:
#                 preset = json.load(f)
#             self.learner.SetValue(preset['learner']['learner'])
#             self.balanced.SetValue(preset['learner']['balanced'])