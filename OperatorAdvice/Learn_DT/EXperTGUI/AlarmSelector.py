from __future__ import division, print_function
import wx
import json
import os
import glob
import re


# class SearchableComboBox(wx.ComboBox):
#     def __init__(self, parent, choices=(), style=0, **par):
#         wx.ComboBox.__init__(self, parent, wx.ID_ANY, style=style | wx.CB_DROPDOWN, choices=choices, **par)
#         self.choices = choices
#         self.Bind(wx.EVT_TEXT, self.EvtText)
#         self.Bind(wx.EVT_CHAR, self.EvtChar)
#         self.Bind(wx.EVT_COMBOBOX, self.EvtCombobox)
#         self.ignoreEvtText = False
#
#     def EvtCombobox(self, event):
#         self.ignoreEvtText = True
#         event.Skip()
#
#     def EvtChar(self, event):
#         if event.GetKeyCode() == 8:
#             self.ignoreEvtText = True
#         event.Skip()
#
#     def EvtText(self, event):
#         if self.ignoreEvtText:
#             self.ignoreEvtText = False
#             return
#         currentText = event.GetString()
#         print(currentText)
#         found = False
#         for choice in self.choices:
#             if choice.lower().startswith(currentText.lower()):
#                 # self.ignoreEvtText = True
#                 self.SetValue(choice)
#                 self.SetInsertionPoint(len(currentText))
#                 self.SetMark(len(currentText), len(choice))
#                 found = True
#                 break
#         if not found:
#             event.Skip()


class AlarmPanel(wx.Panel):
    def __init__(self, parent, alarms, selected_alarm=None, **kwargs):
        self.parent = parent
        super(AlarmPanel, self).__init__(parent, style=wx.BORDER_RAISED, **kwargs)

        title = wx.StaticText(self, label='Alarm')
        title.SetFont(wx.Font(18, wx.DEFAULT, wx.NORMAL, wx.NORMAL))

        self.alarm_list = alarms

        self.alarm = selected_alarm

        self.alarm_component = wx.ComboBox(self, choices=self.alarm_list, size=(240, -1))
        if self.alarm is not None:
            self.alarm_component.SetValue(self.alarm)

        self.alarm_component.Bind(wx.EVT_TEXT, self.dropdown_handler)

        self.grid = wx.BoxSizer(wx.VERTICAL)
        self.grid.Add(title)
        self.grid.Add(wx.StaticLine(self, style=wx.LI_HORIZONTAL, size=(self.GetSize()[0], 20)))
        self.grid.Add(self.alarm_component)

        self.SetSizer(self.grid)
        self.grid.Fit(self)

    def dropdown_handler(self, event):
        if self.alarm_component.Value in self.alarm_list:
            self.alarm = self.alarm_component.Value
        else:
            options = sorted([option for option in self.alarm_list
                              if re.findall(self.alarm_component.Value, option, re.IGNORECASE)])
            self.alarm_component.Set(options)

    def get_alarm(self):
        return self.alarm

    def load_preset(self, path):
        if path:
            with open(path) as f:
                preset = json.load(f)
            self.alarm_component.SetValue(preset['alarm'])