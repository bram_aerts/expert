from __future__ import division, print_function

import re
import wx


class Selector(wx.Panel):
    def __init__(self, parent, ignored_options, selected_options, height=150):
        self.parent = parent
        width = self.parent.GetSize()[0]
        self.listboxwidth = (width - 45) / 2
        super(Selector, self).__init__(self.parent, size=(width, height))
        self.grid = wx.FlexGridSizer(rows=2, cols=3, hgap=5, vgap=5)

        self.ignored_options = sorted(ignored_options)
        self.selected_options = sorted(selected_options)

        self.ignored = wx.ListBox(self, choices=self.ignored_options, size=(self.listboxwidth, height),
                                  style=wx.LB_MULTIPLE)
        self.selected = wx.ListBox(self, choices=self.selected_options, size=(self.listboxwidth, height),
                                   style=wx.LB_MULTIPLE)

        self.ignored.Bind(wx.EVT_LISTBOX_DCLICK, lambda x: self.on_double_click(self.ignored.Items[x.GetSelection()]))
        self.selected.Bind(wx.EVT_LISTBOX_DCLICK, lambda x: self.on_double_click(self.selected.Items[x.GetSelection()]))

        left_right = wx.BoxSizer(wx.VERTICAL)
        move_right = wx.Button(self, label='->', size=(30, -1))
        move_left = wx.Button(self, label='<-', size=(30, -1))
        move_right.Bind(wx.EVT_BUTTON, lambda x: self.apply_move('right'))
        move_left.Bind(wx.EVT_BUTTON, lambda x: self.apply_move('left'))
        left_right.Add(move_right)
        left_right.Add(move_left)

        self.grid.Add(self.ignored)
        self.grid.Add(left_right)
        self.grid.Add(self.selected)

        self.SetSizer(self.grid)

    def on_double_click(self, component):
        pass

    def apply_move(self, where):
        if where == 'right':
            _from_options = self.ignored_options
            _to_options = self.selected_options
            _from = self.ignored
            _to = self.selected
        else:
            _from_options = self.selected_options
            _to_options = self.ignored_options
            _from = self.selected
            _to = self.ignored

        selected = reversed(_from.GetSelections())
        for s in selected:
            _to_options.append(_from.Items[s])
            _from_options.remove(_from.Items[s])
        _to_options = sorted(_to_options)
        _from_options = sorted(_from_options)

        _from.Set(_from_options)
        _to.Set(_to_options)
        self.parent.refresh_ignored()


class FilterableSelector(Selector):
    def __init__(self, parent, ignored_options, selected_options):
        super(FilterableSelector, self).__init__(parent,
                                                 ignored_options=ignored_options,
                                                 selected_options=selected_options,
                                                 height=300)

        self.filter1 = wx.TextCtrl(self, value='', size=(self.listboxwidth, -1))
        self.filter2 = wx.TextCtrl(self, value='', size=(self.listboxwidth, -1))
        self.filter1.Bind(wx.EVT_KEY_UP, lambda x: self.apply_filter('left'))
        self.filter2.Bind(wx.EVT_KEY_UP, lambda x: self.apply_filter('right'))

        self.grid.Insert(0, self.filter2)
        self.grid.Insert(0, wx.Frame(None))
        self.grid.Insert(0, self.filter1)

    def apply_filter(self, which):
        if which == 'left':
            _filter = self.filter1
            _options = self.ignored_options
            _box = self.ignored
        else:
            _filter = self.filter2
            _options = self.selected_options
            _box = self.selected

        options = sorted([option for option in _options if re.findall(_filter.Value, option, re.IGNORECASE)])
        _box.Set(options)

    def apply_move(self, where):
        super(FilterableSelector, self).apply_move(where)
        self.apply_filter('left')
        self.apply_filter('right')

    def on_double_click(self, component):
        self.parent.on_double_click(component)


class DetailComponent(wx.Panel):
    def __init__(self, parent, options, deselected, deactivated):
        self.parent = parent
        self.options = options
        self.deselected = deselected
        super(DetailComponent, self).__init__(parent)

        self.grid = None
        self.check_boxes = None
        self.check_labels = None
        self.set_checkboxes(options, deselected=deselected, deactivated=deactivated)

    def set_checkboxes(self, options, deselected, deactivated):
        self.grid = wx.GridSizer(rows=3, cols=3, hgap=5, vgap=5)
        self.grid.SetMinSize((self.parent.GetSize()[0], -1))
        self.check_boxes = dict([(f, wx.CheckBox(self)) for f in options])
        self.check_labels = dict([(f, wx.StaticText(self, label=f)) for f in options])

        for f in set(deactivated) & set(options):
            self.check_boxes[f].Disable()

        for f in set(options) - set(deselected):
            self.check_boxes[f].SetValue(True)

        for f in options:
            entry = wx.BoxSizer(wx.HORIZONTAL)
            entry.Add(self.check_boxes[f])
            entry.Add(self.check_labels[f])
            self.grid.Add(entry)

        for f in options:
            self.check_boxes[f].Bind(wx.EVT_CHECKBOX, self.checkbox_clicked)

        self.SetSizer(self.grid)

    def checkbox_clicked(self, event):
        self.deselected = [option for option in self.options if not self.check_boxes[option].Value]
        self.parent.refresh_detail()


class FeatureFilter(wx.Panel):
    def __init__(self,
                 parent,
                 component_features=None,
                 selected_components=None,
                 selected_features=None,
                 ignored_detail=None,
                 **args
                 ):
        super(FeatureFilter, self).__init__(parent, style=wx.BORDER_RAISED, **args)

        self.parent = parent
        title_grid = wx.GridSizer(cols=2, rows=1)
        title_grid.SetMinSize((self.GetSize()[0], -1))
        title = wx.StaticText(self, label='Filter')
        title.SetFont(wx.Font(18, wx.DEFAULT, wx.NORMAL, wx.NORMAL))
        # button = wx.Button(self, label='learn tree')
        title_grid.Add(title)
        # title_grid.Add(button, flag=wx.ALIGN_RIGHT)

        self.component_features = component_features if component_features is not None else {}
        self.selected_components = selected_components if selected_components is not None else []
        self.selected_features = selected_features if selected_features is not None \
            else sorted(set(item for sublist in self.component_features.values() for item in sublist))
        self.ignored_detail = ignored_detail if ignored_detail is not None \
            else dict((x, []) for x in self.component_features.keys())

        line = wx.StaticLine(self, style=wx.LI_HORIZONTAL, size=(self.GetSize()[0], 20))

        width = self.GetSize()[0]
        ignore_selected_grid = wx.GridSizer(rows=1, cols=2)
        ignore_selected_grid.SetMinSize((width, -1))
        ignore_selected_grid.Add(wx.StaticText(self, label='Ignored'), flag=wx.ALIGN_LEFT)
        ignore_selected_grid.Add(wx.StaticText(self, label='Selected'), flag=wx.ALIGN_RIGHT)

        self.components = FilterableSelector(self,
                                             ignored_options=self.ignored_components,
                                             selected_options=self.selected_components)
        try:
            self.detail_component = sorted(self.component_features.keys())[0]
        except IndexError:
            pass
        self.detail = DetailComponent(self, options=[], deselected=[], deactivated=[])

        self.features = Selector(self,
                                 ignored_options=self.ignored_features,
                                 selected_options=self.selected_features)

        self.show_features = wx.StaticText(self, label='features >')
        self.show_features.Bind(wx.EVT_LEFT_DOWN, lambda x: self.feature_folder())

        self.show_detail = wx.StaticText(self, label='detail >')
        self.show_detail.Bind(wx.EVT_LEFT_DOWN, lambda x: self.detail_folder())

        self.grid = wx.BoxSizer(wx.VERTICAL)
        self.grid.Add(title_grid)
        self.grid.Add(line)
        self.grid.Add(ignore_selected_grid)
        self.grid.Add(self.components)
        self.grid.AddSpacer(30)
        self.grid.Add(self.show_detail)
        self.grid.Add(self.detail)
        self.grid.Add(self.show_features)
        self.grid.Add(self.features)

        self.SetSizer(self.grid)

        self.grid.Fit(self)

        self.grid.Hide(self.features)
        self.grid.Hide(self.detail)

    @property
    def ignored_features(self):
        return sorted(
            set(item for sub in self.component_features.values() for item in sub) - set(self.selected_features))

    @property
    def ignored_components(self):
        return sorted(set(self.component_features.keys()) - set(self.selected_components))

    def feature_folder(self):
        if self.show_features.GetLabel() == 'features >':
            self.show_features.SetLabel('features v')
            self.grid.Show(self.features)
        else:
            self.show_features.SetLabel('features >')
            self.grid.Hide(self.features, recursive=True)
            # self.SetSizer(self.grid)
        self.grid.Layout()

    def detail_folder(self):
        label = self.show_detail.GetLabel()
        if len(label) == 0 or label[-1] == 'v':
            self.show_detail.SetLabel(label[:-1] + '>')
            self.grid.Hide(self.detail, recursive=True)
        else:
            self.show_detail.SetLabel(label[:-1] + 'v')
            self.grid.Show(self.detail)
        self.grid.Layout()
        # self.SetSizer(self.grid)
        # self.grid.Fit(self)

    def on_double_click(self, component):
        self.detail_component = component
        self.show_detail.SetLabel('detail (' + self.detail_component + ') v')
        self.refresh_detail_window()

    def refresh_detail_window(self):
        try:
            ignored_detail_component = self.ignored_detail[self.detail_component]
        except KeyError:
            ignored_detail_component = []

        detail = DetailComponent(self,
                                 self.component_features[self.detail_component],
                                 ignored_detail_component,
                                 self.ignored_features)
        self.grid.Replace(self.detail, detail)
        self.detail = detail

        self.grid.Layout()
        # self.SetSizer(self.grid)
        # self.grid.Fit(self)

    def refresh_detail(self):
        self.ignored_detail[self.detail_component] = self.detail.deselected

    def refresh_ignored(self):
        self.selected_features = self.features.selected_options
        self.selected_components = self.components.selected_options
        if self.grid.IsShown(self.detail):
            self.refresh_detail_window()

    def get_all_features(self):
        all_features = []
        for component in self.selected_components:
            for feature in self.component_features[component]:
                if feature not in self.ignored_features and feature not in self.ignored_detail[component]:
                    if feature == 'value':
                        all_features.append(component)
                    else:
                        all_features.append(component + '__' + feature)
        return all_features
