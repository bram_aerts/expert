from __future__ import print_function, division

import wx
import wx.lib.scrolledpanel
import os


class MovableBitmap(wx.StaticBitmap):
    def __init__(self, parent, bitmap, size):
        super(MovableBitmap, self).__init__(parent, size=size)
        self.SetBitmap(bitmap)
        self.parent = parent
        self.bitmapsize = bitmap.GetSize()
        self.mouse_position = None
        self.movable = False
        self.Bind(wx.EVT_LEFT_DOWN, self.start_move)
        self.Bind(wx.EVT_MOTION, self.move)

    @property
    def size(self):
        return self.bitmapsize - self.parent.GetSize()

    def start_move(self, event):
        self.mouse_position = event.GetPosition()

    def move(self, event):
        if wx.GetMouseState().LeftIsDown():
            mouse_position = event.GetPosition()
            delta = self.mouse_position - mouse_position
            self.mouse_position = wx.Point(*[int(0.9*x+0.1*y) for x, y in zip(self.mouse_position, mouse_position)])
            self.fit_image(delta)

    def fit_image(self, delta=None):
        if delta is None:
            delta = wx.Point(0, 0)
        pos = self.GetPosition()
        new_pos = wx.Point(*[-max(0, min(x, y)) for x, y in zip(delta - pos, self.size)])

        self.SetPosition(new_pos)


class ImageDisplayer(wx.Frame):
    def __init__(self, parent, image_location, **kwargs):
        if not os.path.exists(image_location):
            print('image not found!')
            return
        super(ImageDisplayer, self).__init__(parent=parent, **kwargs)
        scroll_window = wx.Panel(self)

        png = wx.Image(image_location, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        mbm = MovableBitmap(scroll_window, png, (png.GetWidth(), png.GetHeight()))

        scroll_window.Bind(wx.EVT_SIZE, lambda x: mbm.fit_image())

        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(scroll_window, wx.EXPAND)

        sizer.Layout()
        self.SetSizer(sizer)

        self.SetTitle('Decision Tree')
        self.Maximize()
        self.Show()

def main():
    filename = '/home/bae/Pictures/Screenshot from 2016-10-11 13-42-33.png'
    if not os.path.exists(filename):
        filename = 'w3KlCm7.jpg'
    app = wx.App()
    frame = ImageDisplayer(None, size=(500,500), image_location=filename)

    app.MainLoop()

if __name__ == '__main__':
    main()
