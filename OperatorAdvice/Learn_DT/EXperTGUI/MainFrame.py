from __future__ import print_function, division, generators

import json
import os
import subprocess
import wx

import FeatureFilter
import PostPruner
import ImageDisplayer
import PresetSelector
import AlarmSelector
import OptionSelector
import TreeLearner


def to_file_name(input_string):
    """
    :param input_string: str, string to make
    :return:
    """
    if input_string is None:
        return ""
    return input_string.replace("/", "_")


def read_alarms(input_directory):
    os.path.join(input_directory, 'label_feature_columns')
    with open(os.path.join(input_directory, 'label_feature_columns')) as f:
        lcols, fcols = json.load(f)
    alarms = []
    for label in lcols:
        if os.path.exists(os.path.join(input_directory, to_file_name(label))):
            alarms.append(label)
    return alarms


def read_component_features(input_directory):
    with open(os.path.join(input_directory, 'label_feature_columns')) as f:
        lcols, fcols = json.load(f)
    component_features = {}
    for x in fcols:
        splitted = x.split('__')
        component = splitted[0]
        if len(splitted) == 1:
            feature = 'value'
        else:
            feature = splitted[1]

        if component not in component_features.keys():
            component_features[component] = []
        component_features[component].append(feature)
    return component_features


class LearnTreeFrame(wx.Frame):
    def __init__(self, parent):
        super(LearnTreeFrame, self).__init__(parent)
        self.grid = wx.BoxSizer(wx.VERTICAL)

        self.display_window = None
        self.input_directory = '/home/bae/Documents/EXperT/learned_trees_burst_2'

        self.component_features = read_component_features(self.input_directory)
        self.alarms = read_alarms(self.input_directory)

        self.preset_panel = PresetSelector.PresetPanel(self, size=(260, 120))
        self.alarm_panel = AlarmSelector.AlarmPanel(self, self.alarms, size=(250, 120))
        self.option_panel = OptionSelector.OptionPanel(self, size=(170, 120))
        self.tree_panel = TreeLearner.TreePanel(self, size=(200, 120))

        self.filter_panel = FeatureFilter.FeatureFilter(self,
                                                        size=(600, 650),
                                                        component_features=self.component_features)
        self.pruner_panel = PostPruner.PostPruner(self, size=(300, 650))

        self.top_grid = wx.BoxSizer(wx.HORIZONTAL)
        self.top_grid.Add(self.preset_panel)
        self.top_grid.Add(self.alarm_panel)
        self.top_grid.Add(self.option_panel)
        self.top_grid.Add(self.tree_panel)

        self.bottom_grid = wx.BoxSizer(wx.HORIZONTAL)
        self.bottom_grid.Add(self.filter_panel)
        self.bottom_grid.Add(self.pruner_panel)

        self.grid.Add(self.top_grid)
        self.grid.Add(self.bottom_grid)

        self.SetSizerAndFit(self.grid)
        self.Show()

    def load_preset(self):
        preset_name = self.preset_panel.preset
        if not preset_name:
            return
        path = os.path.join(os.getcwd(), 'presets', preset_name)
        with open(path) as f:
            preset = json.load(f)

        alarm_panel = AlarmSelector.AlarmPanel(self, self.alarms, selected_alarm=preset['alarm'])
        option_panel = OptionSelector.OptionPanel(self, **preset['options'])
        filter_panel = FeatureFilter.FeatureFilter(self,
                                                   component_features=self.component_features,
                                                   size=(600, 820),
                                                   **preset['filter'])
        pruner_panel = PostPruner.PostPruner(self, size=(300, 820), **preset['pruner'])

        self.top_grid.Replace(self.alarm_panel, alarm_panel)
        self.top_grid.Replace(self.option_panel, option_panel)
        self.bottom_grid.Replace(self.filter_panel, filter_panel)
        self.bottom_grid.Replace(self.pruner_panel, pruner_panel)

        self.alarm_panel = alarm_panel
        self.option_panel = option_panel
        self.filter_panel = filter_panel
        self.pruner_panel = pruner_panel
        self.Layout()
        # self.load_preset(self.component_features, path)
        # self.alarm_panel.load_preset(path)

    def save_preset(self, path):
        # alarm_name = self.alarm_panel.alarm

        preset = self.get_preset()
        with open(path, 'w') as f:
            json.dump(preset, f, indent=4)

    def get_preset(self):
        preset = {
            'filter': {
                'selected_components': self.filter_panel.selected_components,
                'selected_features': self.filter_panel.selected_features,
                'ignored_detail': self.filter_panel.ignored_detail,
            },
            'pruner': {
                'min_pos': self.pruner_panel.min_pos,
                'highlight': self.pruner_panel.highlight,
                'min_rat': self.pruner_panel.min_rat,
                'max_rat': self.pruner_panel.max_rat
            },
            'options': {
                'balanced': self.option_panel.get_balanced(),
                'learner': self.option_panel.get_learner()
            },
            'alarm': self.alarm_panel.alarm
        }
        return preset

    def prune_tree(self):
        min_pos = self.pruner_panel.min_pos
        highlight = self.pruner_panel.highlight
        min_rat = self.pruner_panel.min_rat
        max_rat = self.pruner_panel.max_rat
        command = ' '.join([
            'python3',
            '/home/bae/Documents/EXperT/Git/expert/OperatorAdvice/Learn_DT/pretty_tree.py',
            '-i tmp',
            '-o pretty_tree.dot',
            '-minpos ' + str(min_pos),
            '-highlight ' + str(highlight),
            '-minrat ' + str(min_rat),
            '-maxrat ' + str(max_rat)
        ])
        print(command)
        subprocess.call(command, shell=True)

    def learn_tree(self):
        feature_file = os.path.join('/', 'tmp', 'selected_features')
        selected_features = self.filter_panel.get_all_features()
        with open(feature_file, 'w') as f:
            json.dump(selected_features, f)

        alarm = self.alarm_panel.get_alarm()
        balanced = self.option_panel.get_balanced()
        learner = self.option_panel.get_learner()

        command = ' '.join(['python3',
                            '/home/bae/Documents/EXperT/Git/expert/OperatorAdvice/Learn_DT/decision_tree_learner.py',
                            '"' + to_file_name(alarm) + '"',
                            '-o tmp',
                            '-feature_file ' + feature_file,
                            '-idir ' + self.input_directory,
                            '--scikit' if learner == 'scikit' else '--weka',
                            '--balanced' if balanced == 'balanced' else '--unbalanced'])
        print(command)
        subprocess.call(command, shell=True)

    def learn_or_prune(self):
        self.learn_tree()
        self.prune_tree()
        self.show_tree()

    def save_tree(self):
        directory = os.path.join(os.getcwd(), 'trees')
        try:
            os.makedirs(directory)
        except OSError:
            pass
        dialog = wx.FileDialog(self, defaultDir=directory, style=wx.FD_SAVE, wildcard='*.png')
        dialog.Show()

        if dialog.ShowModal() == wx.ID_OK:
            pass
        if not dialog.GetPath():
            return
        tree_path = dialog.GetPath() + '.png'

        subprocess.call('cp pretty_tree.png ' + tree_path, shell=True)

    def show_tree(self):
        command = ' '.join([
            'dot -Tpng',
            'pretty_tree.dot',
            '-o pretty_tree.png'
        ])

        subprocess.call(command, shell=True)
        try:
            self.display_window.Close()
        except AttributeError:
            pass
        self.display_window = ImageDisplayer.ImageDisplayer(parent=self, image_location='pretty_tree.png')


app = wx.App(False)
frame = LearnTreeFrame(None)

app.MainLoop()


def a_valid_preset():
    component_features = {
        'component1': ['feature0', 'feature1', 'feature2', 'feature3'],
        'component2': ['feature0', 'feature1', 'feature2', 'feature3'],
        'component3': ['feature0', 'feature1', 'feature2', 'feature3'],
        'component4': ['feature0', 'feature1', 'feature2', 'feature3'],
        'component5': ['feature0', 'feature10', 'feature20', 'feature30'],
        'component6': ['feature0', 'feature10', 'feature20', 'feature30'],
        'component7': ['feature0', 'feature10', 'feature20', 'feature30'],
        'component8': ['feature0', 'feature10', 'feature20', 'feature30'],
        'component10': ['feature0', 'feature1', 'feature2', 'feature3'],
        'component20': ['feature0', 'feature1', 'feature2', 'feature3'],
        'component30': ['feature0', 'feature1', 'feature2', 'feature3'],
        'component40': ['feature0', 'feature1', 'feature2', 'feature3'],
        'component50': ['feature0', 'feature10', 'feature20', 'feature30'],
        'component60': ['feature0', 'feature10', 'feature20', 'feature30'],
        'component70': ['feature0', 'feature10', 'feature20', 'feature30'],
        'component800000000000000000000000000000000000000000000': ['feature0', 'feature10', 'feature20',
                                                                   'feature30'],
    }
    ignored_components = list(component_features.keys())
    selected_components = list()

    selected_features = list(set([item for sublist in component_features.values() for item in sublist]))
    ignored_features = list()

    ignored_detail = {
        'component1': [],
        'component2': [],
        'component3': [],
        'component4': [],
        'component5': [],
        'component6': [],
        'component7': [],
        'component8': [],
    }

    preset = {
        'filter': {
            'component_features': component_features,
            'selected_components': selected_components,
            'selected_features': selected_features,
            'ignored_detail': ignored_detail,
        },
        'pruner': {
            'min_pos': 0,
            'highlight': 0,
            'min_rat': 0,
            'max_rat': 1
        }

    }

    with open(os.path.join('.', 'presets', 'alarm2', 'preset 1'), 'w') as f:
        json.dump(preset, f, indent=4)
