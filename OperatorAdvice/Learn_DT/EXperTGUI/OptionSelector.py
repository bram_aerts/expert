from __future__ import division, print_function
import wx
import json
import os
import glob
import re


# class SearchableComboBox(wx.ComboBox):
#     def __init__(self, parent, choices=(), style=0, **par):
#         wx.ComboBox.__init__(self, parent, wx.ID_ANY, style=style | wx.CB_DROPDOWN, choices=choices, **par)
#         self.choices = choices
#         self.Bind(wx.EVT_TEXT, self.EvtText)
#         self.Bind(wx.EVT_CHAR, self.EvtChar)
#         self.Bind(wx.EVT_COMBOBOX, self.EvtCombobox)
#         self.ignoreEvtText = False
#
#     def EvtCombobox(self, event):
#         self.ignoreEvtText = True
#         event.Skip()
#
#     def EvtChar(self, event):
#         if event.GetKeyCode() == 8:
#             self.ignoreEvtText = True
#         event.Skip()
#
#     def EvtText(self, event):
#         if self.ignoreEvtText:
#             self.ignoreEvtText = False
#             return
#         currentText = event.GetString()
#         print(currentText)
#         found = False
#         for choice in self.choices:
#             if choice.lower().startswith(currentText.lower()):
#                 # self.ignoreEvtText = True
#                 self.SetValue(choice)
#                 self.SetInsertionPoint(len(currentText))
#                 self.SetMark(len(currentText), len(choice))
#                 found = True
#                 break
#         if not found:
#             event.Skip()


class OptionPanel(wx.Panel):
    def __init__(self, parent, balanced=None, learner=None, **kwargs):
        self.parent = parent
        super(OptionPanel, self).__init__(parent, style=wx.BORDER_RAISED, **kwargs)

        title = wx.StaticText(self, label='Options')
        title.SetFont(wx.Font(18, wx.DEFAULT, wx.NORMAL, wx.NORMAL))
        self.balanced = wx.ComboBox(self, size=(150, -1), choices=['unbalanced', 'balanced'], value='unbalanced')
        self.learner = wx.ComboBox(self, size=(150, -1), choices=['weka', 'scikit'], value='weka')
        if balanced is not None:
            self.balanced.SetValue(balanced)
        if learner is not None:
            self.learner.SetValue(learner)

        self.grid = wx.BoxSizer(wx.VERTICAL)
        self.grid.Add(title)
        self.grid.Add(wx.StaticLine(self, style=wx.LI_HORIZONTAL, size=(self.GetSize()[0], 20)))
        self.grid.Add(self.balanced)
        self.grid.Add(self.learner)

        self.SetSizer(self.grid)
        self.grid.Fit(self)

    def get_balanced(self):
        return self.balanced.GetValue()

    def get_learner(self):
        return self.learner.GetValue()

    def load_preset(self, path):
        if path:
            with open(path) as f:
                preset = json.load(f)
            self.learner.SetValue(preset['learner']['learner'])
            self.balanced.SetValue(preset['learner']['balanced'])