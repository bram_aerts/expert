from __future__ import division, print_function
import wx


class PostPruner(wx.Panel):
    def __init__(self,
                 parent,
                 min_pos=0,
                 highlight=0,
                 min_rat=0,
                 max_rat=1,
                 **args):
        super(PostPruner, self).__init__(parent, style=wx.BORDER_RAISED, **args)

        self.parent = parent
        title_grid = wx.GridSizer(cols=2, rows=1)
        title_grid.SetMinSize((self.GetSize()[0], -1))
        title = wx.StaticText(self, label='Prune')
        title.SetFont(wx.Font(18, wx.DEFAULT, wx.NORMAL, wx.NORMAL))
        # button = wx.Button(self, label='prune tree')
        # button.Bind(wx.EVT_BUTTON, self.on_button)
        title_grid.Add(title)
        # title_grid.Add(button, flag=wx.ALIGN_RIGHT)

        self.min_pos_component = TitledSlider(self, 'Minimal positives', 25, min_pos)
        self.highlight_component = TitledSlider(self, 'Highlight nodes', 50, highlight)
        self.ratio_component = MinMaxPanel(self, 'Ratio of interest', min_rat, max_rat)
        # self.button = wx.Button(self, label='prune tree')
        # self.button.Bind(wx.EVT_BUTTON, self.on_button)

        self.grid = wx.BoxSizer(wx.VERTICAL)
        self.grid.Add(title_grid)
        self.grid.Add(wx.StaticLine(self, style=wx.LI_HORIZONTAL, size=(self.GetSize()[0], 20)))
        self.grid.Add(self.min_pos_component)
        self.grid.Add(self.highlight_component)
        self.grid.Add(self.ratio_component)
        # self.grid.Add(self.button)

        self.grid.Layout()
        self.SetSizer(self.grid)

    @property
    def min_pos(self):
        return self.min_pos_component.GetValue()

    @property
    def highlight(self):
        return self.highlight_component.GetValue()

    @property
    def min_rat(self):
        return self.ratio_component.GetMinValue()

    @property
    def max_rat(self):
        return self.ratio_component.GetMaxValue()


class TitledSlider(wx.Panel):
    def __init__(self, parent, title, max_value, value):
        super(TitledSlider, self).__init__(parent)
        self.title = wx.StaticText(self, label=title)
        self.slider = wx.Slider(self, maxValue=max_value, value=value, size=(200, -1), style=wx.SL_VALUE_LABEL)
        self.grid = wx.BoxSizer(wx.VERTICAL)
        self.grid.Add(self.title)
        self.grid.Add(self.slider)

        self.grid.Layout()
        self.SetSizer(self.grid)

    def GetValue(self):
        return self.slider.GetValue()


class MinMaxPanel(wx.Panel):
    def __init__(self,
                 parent,
                 title,
                 min_val,
                 max_val):
        super(MinMaxPanel, self).__init__(parent)
        self.title = MinMaxTitle(self, title, min_val, max_val)

        self.grid = wx.BoxSizer(wx.VERTICAL)
        self.grid.Add(self.title)

        self.tics = 150
        self.low_ti = 50
        self.high_ti = 140
        self.low_te = 0.05
        self.high_te = 0.95

        self.slider = MinMaxSlider(self, self.tics, self.map_value_ei(min_val), self.map_value_ei(max_val))
        self.grid.Add(self.slider)
        self.grid.Layout()
        self.SetSizer(self.grid)

    def set_range(self, min, max):
        self.title.set_range(min, max)

    def GetMinValue(self):
        return self.map_value_ie(self.slider.minSlider.GetValue())

    def GetMaxValue(self):
        return self.map_value_ie(self.slider.maxSlider.GetValue())

    def map_value_ie(self, internal_value):
        if internal_value < self.low_ti:
            return internal_value / 1000
        elif internal_value > self.high_ti:
            return self.high_te + (internal_value - self.high_ti) / 200
        return self.low_te + (internal_value - self.low_ti) / 100

    def map_value_ei(self, external_value):
        if external_value < self.low_te:
            return external_value * 1000
        elif external_value > self.high_te:
            return (external_value - self.high_te) * 200 + self.high_ti
        return (external_value - self.low_te) * 100 + self.low_ti


class MinMaxTitle(wx.Panel):
    def __init__(self,
                 parent,
                 title,
                 min_val,
                 max_val):
        super(MinMaxTitle, self).__init__(parent)
        self.parent = parent
        self.grid = wx.BoxSizer(wx.HORIZONTAL)
        self.label = wx.StaticText(self, label=title + ':')
        self.range = wx.StaticText(self, label='{:2}-{:2}'.format(min_val, max_val), size=(100, -1))
        self.grid.Add(self.label)
        self.grid.Add(self.range)
        self.grid.Layout()
        self.SetSizer(self.grid)

    def set_range(self, min, max):
        self.grid.Hide(self.range)
        self.grid.Remove(self.range)
        label = '{:2}-{:2}'.format(self.parent.map_value_ie(min), self.parent.map_value_ie(max))
        self.range = wx.StaticText(self, label=label)
        self.grid.Add(self.range)
        self.grid.Layout()


class MinMaxSlider(wx.Panel):
    def __init__(self,
                 parent,
                 tics,
                 min_val,
                 max_val):
        super(MinMaxSlider, self).__init__(parent)
        self.parent = parent
        self.grid = wx.FlexGridSizer(cols=2, rows=2)
        self.minSlider = wx.Slider(self, minValue=0, maxValue=tics, size=(200,-1), value=min_val)
        self.maxSlider = wx.Slider(self, minValue=0, maxValue=tics, size=(200,-1), value=max_val)
        self.minSlider.Bind(wx.EVT_SLIDER, self.respond_slider_change_over)
        self.maxSlider.Bind(wx.EVT_SLIDER, self.respond_slider_change_under)

        self.grid.Add(self.minSlider)
        # self.grid.Add(wx.StaticText(self, label='min'))
        self.grid.Add(wx.StaticText(self))
        self.grid.Add(self.maxSlider)
        # self.grid.Add(wx.StaticText(self, label='max'))
        self.grid.Add(wx.StaticText(self))

        self.grid.Layout()
        self.SetSizer(self.grid)

    def respond_slider_change_over(self, event):
        if self.minSlider.GetValue() >= self.maxSlider.GetValue():
            self.maxSlider.SetValue(self.minSlider.GetValue() + 1)
        self.parent.set_range(self.minSlider.GetValue(), self.maxSlider.GetValue())

    def respond_slider_change_under(self, event):
        if self.maxSlider.GetValue() <= self.minSlider.GetValue():
            self.minSlider.SetValue(self.maxSlider.GetValue() - 1)
        self.parent.set_range(self.minSlider.GetValue(), self.maxSlider.GetValue())