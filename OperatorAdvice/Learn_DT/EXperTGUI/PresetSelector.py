from __future__ import division, print_function
import wx
import json
import os
import glob
import re


class PresetPanel(wx.Panel):
    def __init__(self, parent, **kwargs):
        self.parent = parent
        super(PresetPanel, self).__init__(parent, style=wx.BORDER_RAISED, **kwargs)

        self.preset = None

        self.title = wx.StaticText(self, label='Preset')
        self.title.SetFont(wx.Font(18, wx.DEFAULT, wx.NORMAL, wx.NORMAL))

        self.save_button = wx.Button(self, label='Save preset', size=(100, -1))
        self.save_button.Bind(wx.EVT_BUTTON, self.on_save)

        self.load_button = wx.Button(self, label='Load preset', size=(100, -1))
        self.load_button.Bind(wx.EVT_BUTTON, self.on_load)

        self.recent_presets = wx.ComboBox(self, choices=[], size=(150, -1))
        self.recent_presets.Bind(wx.EVT_COMBOBOX, self.on_select_preset)

        self.grid = wx.FlexGridSizer(rows=2, cols=2)

        self.grid.Add(self.load_button)
        self.grid.Add(self.recent_presets)
        self.grid.Add(self.save_button)

        whole_grid = wx.BoxSizer(wx.VERTICAL)
        whole_grid.Add(self.title)
        whole_grid.Add(wx.StaticLine(self, style=wx.LI_HORIZONTAL, size=(self.GetSize()[0], 20)))
        whole_grid.Add(self.grid)

        self.SetSizer(whole_grid)
        whole_grid.Fit(self)

        self.update_most_recent()

    def update_most_recent(self):
        files = glob.glob('presets/*')
        files.sort(key=os.path.getmtime)
        self.recent_presets.Clear()
        for f in files[::-1][:5]:
            self.recent_presets.Append(f.replace('presets/', ''))

    # def update_choices_component(self):
    #     path = os.path.join(os.getcwd(), 'presets', self.alarm)
    #     choices = [x.replace(path + '/', '') for x in glob.glob(path + '/*')]
    #
    #     preset_component = wx.ComboBox(self, choices=choices, size=(250, -1))
    #     preset_component.Bind(wx.EVT_COMBOBOX, self.on_select_preset)
    #     self.grid.Replace(self.preset_component, preset_component)
    #     self.preset_component = preset_component
    #     self.grid.Layout()

    def on_select_preset(self, event):
        self.preset = self.recent_presets.Value
        self.parent.load_preset()

    def on_save(self, event):
        directory = os.path.join(os.getcwd(), 'presets')
        try:
            os.makedirs(directory)
        except OSError:
            pass
        dialog = wx.FileDialog(self, defaultDir=directory, style=wx.FD_SAVE)
        dialog.Show()

        if dialog.ShowModal() == wx.ID_OK:
            pass
        if not dialog.GetPath():
            return
        preset_path = dialog.GetPath()
        preset_name = preset_path.replace(directory + '/', '')

        self.parent.save_preset(preset_path)

        self.update_most_recent()
        self.preset = preset_name
        self.recent_presets.SetValue(preset_name)
        self.parent.load_preset()

    def on_load(self, event):
        directory = os.path.join(os.getcwd(), 'presets')

        dialog = wx.FileDialog(self, defaultDir=directory, style=wx.FD_SAVE)
        dialog.Show()

        if dialog.ShowModal() == wx.ID_OK:
            pass
        if not dialog.GetPath():
            return
        preset_path = dialog.GetPath()
        preset_name = preset_path.replace(directory + '/', '')

        self.parent.load_preset(preset_path)
        if not preset_name:
            return
        self.update_most_recent()
        self.preset = preset_name
        self.recent_presets.SetValue(preset_name)
        self.parent.load_preset()
