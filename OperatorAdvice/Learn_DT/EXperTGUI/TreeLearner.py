import wx


class TreePanel(wx.Panel):
    def __init__(self, parent, **args):
        self.parent = parent
        super(TreePanel, self).__init__(parent, style=wx.BORDER_RAISED, **args)

        learn = wx.Button(self, label='learn tree')
        learn.Bind(wx.EVT_BUTTON, lambda x: self.parent.learn_or_prune())
        save = wx.Button(self, label='save tree')
        save.Bind(wx.EVT_BUTTON, lambda x: self.parent.save_tree())
        show = wx.Button(self, label='show tree')
        show.Bind(wx.EVT_BUTTON, lambda x: self.parent.show_tree())

        self.grid = wx.GridSizer(rows=3, cols=1)

        self.grid.Add(learn)
        self.grid.Add(show)
        self.grid.Add(save)


        self.SetSizer(self.grid)