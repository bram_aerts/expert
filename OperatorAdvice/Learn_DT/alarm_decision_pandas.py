from __future__ import print_function

import logging
import os
import shutil

import status_space_creator as ssc
import numpy as np
import pandas as pd

import feature_space_creator as fsc
from config.config import config
import create_examples


def read_alarms(conf, subsystem=None):
    df_status = ssc.create_status_space_from_csv(conf.alarm_location,
                                                 "alarm",
                                                 add_negatives=True,
                                                 columns=conf.alarm_columns,
                                                 subsystem=subsystem)
    logging.info("Adding labels...")
    df_edges = df_status.diff()
    df_same = df_status == df_edges
    df_labels = df_status / df_same
    # noinspection PyUnresolvedReferences
    df_labels = df_labels.replace([np.inf, -np.inf], np.nan)
    logging.info("Labels added!")
    df_features = fsc.find_features_alarm(df_status)

    return df_labels, df_features


def read_visu_sensors(time_series, conf, subsystem=None):
    df_status = ssc.create_status_space_from_csv(conf.visu_location,
                                                 "visu",
                                                 add_negatives=False,
                                                 columns=conf.visu_columns,
                                                 subsystem=subsystem)
    df_features = fsc.find_features_sensor(df_status, time_series, analog=False)

    return df_features


def read_eu_sensors(time_series, conf, subsystem=None):
    df_status = ssc.create_status_space_from_csv(conf.eu_location,
                                                 'eu',
                                                 add_negatives=False,
                                                 columns=conf.eu_columns,
                                                 subsystem=subsystem)
    if not df_status.empty:
        df_features = fsc.find_features_sensor(df_status, time_series, analog=True)
    else:
        df_features = pd.DataFrame()
    return df_features


def create_labels_features(conf, subsystem=None):
    storage = pd.HDFStore('/tmp/dataframes')
    logging.info("""
    ==============
    Reading Alarms
    ==============""")
    storage['df_labels'], storage['df_alarm_features'] = read_alarms(conf, subsystem=subsystem)
    logging.info("""
    ====================
    reading VISU sensors
    ====================""")
    storage['df_visu_features'] = read_visu_sensors(storage['df_labels'].index, conf, subsystem=subsystem)
    logging.info("""
    ==================
    reading EU sensors
    ==================""")
    storage['df_eu_features'] = read_eu_sensors(storage['df_labels'].index, conf, subsystem=subsystem)
    storage['df_features'] = pd.concat([storage['df_alarm_features'],
                                        storage['df_visu_features'],
                                        storage['df_eu_features']], axis=1)

    return storage['df_labels'], storage['df_features']


def execute(configuration, read_csvs=True):
    if read_csvs:
        try:
            shutil.rmtree(configuration.output_directory)
        except FileNotFoundError:
            pass
        os.makedirs(configuration.output_directory)
        df_labels, df_features = create_labels_features(configuration)
        create_examples.write_subsystem_mapping(configuration, configuration.output_directory)
        create_examples.make_features_labels(df_labels, df_features, configuration.output_directory)


def main():
    logging.basicConfig(level=logging.DEBUG, format="%(asctime)s (%(levelname)s): \t %(message)s")

    logging.info('start')
    execute(config, read_csvs=True)
    logging.info('einde ;-)')


if __name__ == "__main__":
    main()
