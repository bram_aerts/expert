class Configuration(object):
    def __init__(self,
                 alarm_location,
                 alarm_columns,
                 visu_location,
                 visu_columns,
                 eu_location,
                 eu_columns,
                 output_directory,
                 visu_bits,
                 visu_type_mapping):
        self.visu_type_mapping = visu_type_mapping
        self.visu_bits = visu_bits
        self.output_directory = output_directory
        self.eu_columns = eu_columns
        self.eu_location = eu_location
        self.visu_columns = visu_columns
        self.visu_location = visu_location
        self.alarm_columns = alarm_columns
        self.alarm_location = alarm_location
