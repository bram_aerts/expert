# noinspection PyUnresolvedReferences
import configuration_format


config = configuration_format.Configuration(
    alarm_location="/home/bae/Documents/EXperT/testData_csvs/alarms.csv",
    alarm_columns=["timestamp", "status", "component", "_id", "_class", "_type", "_group",
                   'burst'],
    visu_location="/home/bae/Documents/EXperT/testData_csvs/processVariables_VISU.csv",
    visu_columns=["timestamp", "component", "_group", "status"],
    eu_location="/home/bae/Documents/EXperT/testData_csvs/processVariables_EU.csv",
    eu_columns=["timestamp", "component", "_group", "_status", "status"],
    output_directory='/home/bae/Documents/EXperT/test_learned_trees',
    visu_bits={
        'TYPE_VISU': {
            'commentaar': 9,
            'buiten_dienst': 10,
            'in_hand': 25,
            'in_simulatie': 27,
            'FB_on': 19
        },
        'TYPE_VISU_VLV1': {
            'commentaar': 29,
            'buiten_dienst': 30,
            'in_hand': 25,
            'in_simulatie': 27,
            'FB_open': 19,
            'FB_closed': 20
        },
        'TYPE_VISU_DI': {
            'commentaar': 16,
            'buiten_dienst': 17,
            'in_simulatie': 27,
            'bypass': 18,
            'ON': 24
        },
        'TYPE_VISU_ANA': {
            'commentaar': 18,
            'buiten_dienst': 19,
            'in_simulatie': 27,
            'bypass': 20
        }
    },
    visu_type_mapping={'VISU: 1': 'TYPE_VISU_VLV1', 'VISU: 2': 'TYPE_VISU', 'VISU: 3': 'TYPE_VISU_VLV1', 'VISU: 4': 'TYPE_VISU'}
)
