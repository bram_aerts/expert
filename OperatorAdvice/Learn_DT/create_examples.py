import json
import logging
import os

import numpy as np
import pandas as pd
import datetime

from utils import to_file_name


def create_examples(df_labels, df_features, alarmtext, min_time_between=datetime.timedelta(hours=1)):
    labels = []
    features = []
    prev_time = datetime.datetime.fromtimestamp(0)
    for l, f in zip(df_labels[alarmtext].iteritems(), df_features.itertuples()):  # potential aligning data
        cur_time = l[0]
        lab = l[1]
        feat = f[1:]
        if np.isnan(lab) or any(map(np.isnan, feat)):  # remove any entry with nans
            continue
        elif cur_time - prev_time > min_time_between or (lab == labels[-1] == 1):
            labels.append(float(lab))
            features.append(feat + (cur_time.value // 10**9,))

        elif lab == 1:
            labels[-1] = float(lab)
            features[-1] = feat + (cur_time.value // 10**9,)
        else:
            continue
        prev_time = cur_time
    return labels, features


def find_active(labels, number=None, minimal_occurence=5):
    """
    :param labels: dataframe with changes (1: activated, 0 no action, -1: deactivated)
    :param number: int the number of components to select
    :param minimal_occurence: int the minimal of occurences of an alarm
    :return: the X most active component ids
    """
    action = dict([(x, np.nansum(np.abs(labels[x][1:]))) for x in labels])
    action2 = dict(sorted(action.items(), key=lambda x: x[1], reverse=True)[:number])
    action2 = [k for k, v in action2.items() if v >= minimal_occurence]
    return action2


def find_subsystems(conf):
    """finds subsystems and corresponding components"""

    def update_subsystems(loc, col):
        df = pd.read_csv(loc, header=None)
        df.columns = col
        subsystems.update(set(df['_group']))
        for s in subsystems:
            if s not in subsystem_mapping:
                subsystem_mapping[s] = set()
            subsystem_mapping[s].update(set(df[df['_group'] == s]['component']))

    logging.info("searching subsystems (and components)...")
    subsystems = set()
    subsystem_mapping = dict()
    update_subsystems(conf.alarm_location, conf.alarm_columns)
    update_subsystems(conf.visu_location, conf.visu_columns)
    update_subsystems(conf.eu_location, conf.eu_columns)

    subsystem_mapping[None] = set()
    for x in subsystem_mapping.values():
        subsystem_mapping[None].update(x)

    for i in subsystem_mapping.keys():
        subsystem_mapping[i] = list(subsystem_mapping[i])

    return subsystem_mapping


def write_subsystem_mapping(conf, output_dir):
    subsystem_mapping = find_subsystems(conf)
    with open(os.path.join(output_dir, 'subsystem_mapping'), 'w') as f:
        json.dump(subsystem_mapping, f)


def make_features_labels(df_labels, df_features, output_dir):
    alarms = find_active(df_labels)
    with open(os.path.join(output_dir, 'label_feature_columns'), 'w') as f:
        json.dump((list(df_labels.columns), list(df_features.columns) + ['epoch']), f)
    logging.info('create examples...')
    for i, alarm in enumerate(alarms):
        logging.info(str(i) + '/' + str(len(alarms)) + '...')
        labels, features = create_examples(df_labels, df_features, alarm)

        with open(os.path.join(output_dir, to_file_name(alarm)), 'w') as f:
            json.dump((labels, features), f, default=to_int)


def to_int(a):
    if type(a) == np.int64:
        return int(a)
    else:
        raise TypeError()
