import argparse
import json
import logging
import os

import numpy as np
import pandas as pd

import scikit_learn
import weka_learn
from utils import to_file_name


class DecisionTreeLearner(object):
    def __init__(self, output_file, label_columns, feature_columns, subsystems, learner=None, balanced=False):
        self.balanced = balanced
        self.learner = learner
        self.output_file = output_file
        self.label_columns = label_columns
        self.feature_columns = feature_columns
        self.subsystems = subsystems

    def learn_dt(self, alarm_name, labels, features, selected_features):
        if alarm_name not in self.label_columns:
            for alarm in self.label_columns:
                if to_file_name(alarm) == alarm_name:
                    alarm_name = alarm

        columns_to_keep = [x in selected_features for x in self.feature_columns]
        if self.balanced:
            opts = {'class_weight': 'balanced'}
        else:
            opts = {'class_weight': None}

        features2 = [[x for i, x in enumerate(y) if columns_to_keep[i]] for y in features]

        col_names = [x for i, x in enumerate(self.feature_columns) if columns_to_keep[i]]

        if self.learner == 'scikit':
            scikit_learn.ScikitLearner(alarm_name, col_names, labels, features2, self.output_file, opts)
        elif self.learner == 'weka':
            weka_learn.WekaLearner(alarm_name, col_names, labels, features2, output_file=self.output_file, options=opts)
        elif self.learner == 'forest':
            import scikit_forest
            scikit_forest.ScikitLearner(alarm_name, col_names, labels, features2, self.output_file)
        else:
            import sys
            sys.exit(-1)


def analyze_some_features(labels, features, fcol):
    def compare(indices, string):
        no_alarm = np.setdiff1d(indices, indicesl)
        no_pump = np.setdiff1d(indicesl, indices)
        at_same_time = np.intersect1d(indices, indicesl)
        print('{:36}|{:12}|{:12}|{:12}|{:12}'.format(string[:36], len(indices), len(at_same_time), len(no_alarm),
                                                     len(no_pump)))

    nplabels = np.array(labels)
    npfeatures = np.array(features)

    indicesl = np.argwhere(nplabels == 1)

    print('number of labels:                 ', len(indicesl))
    print('{:36}|{:12}|{:12}|{:12}|{:12}'.format('', '#occurences', '#concurrent', 'feature only', 'alarm only'))

    features = sorted([x for x in fcol if ('TCVWW_420_02_003-VISU' in x or 'PWW_420_02-VISU' in x) and '__' not in x])
    # features = sorted([x for x in fcol if 'VISU' in x])
    for feature in features:
        column_to_select = fcol.index(feature)
        indicesf = np.argwhere(npfeatures[:, column_to_select])

        compare(indicesf, feature)


def find_active(labels, number=None, minimal_occurence=5):
    """
    :param labels: dataframe with changes (1: activated, 0 no action, -1: deactivated)
    :param number: int the number of components to select
    :param minimal_occurence: int the minimal of occurences of an alarm
    :return: the X most active component ids
    """
    action = dict([(x, np.nansum(np.abs(labels[x][1:]))) for x in labels])
    action2 = dict(sorted(action.items(), key=lambda x: x[1], reverse=True)[:number])
    action2 = [k for k, v in action2.items() if v >= minimal_occurence]
    return action2


def find_subsystems(conf):
    """finds subsystems and corresponding components"""

    def update_subsystems(loc, col):
        df = pd.read_csv(loc, header=None)
        df.columns = col
        subsystems.update(set(df['_group']))
        for s in subsystems:
            if s not in subsystem_mapping:
                subsystem_mapping[s] = set()
            subsystem_mapping[s].update(set(df[df['_group'] == s]['component']))

    logging.info("searching subsystems (and components)...")
    subsystems = set()
    subsystem_mapping = dict()
    update_subsystems(conf.alarm_location, conf.alarm_columns)
    update_subsystems(conf.visu_location, conf.visu_columns)
    update_subsystems(conf.eu_location, conf.eu_columns)

    subsystem_mapping[None] = set()
    for x in subsystem_mapping.values():
        subsystem_mapping[None].update(x)

    for i in subsystem_mapping.keys():
        subsystem_mapping[i] = list(subsystem_mapping[i])

    return subsystem_mapping


def main():
    parser = argparse.ArgumentParser(description='learn a decision tree')
    parser.add_argument('alarm', type=str, metavar='alarm', help='learn this alarm')
    parser.add_argument('-o', type=str, required=True, metavar='OutputFile', help='location of the output file')
    parser.add_argument('-idir', type=str, required=True, metavar='InputDirectory', help='location of input files')
    parser.add_argument('-feature_file', type=str, metavar='feature_file', help='location of feature file')
    parser.add_argument('--weka', default=False, action='store_true', help='specify learner(weka/scikit)')
    parser.add_argument('--scikit', default=False, action='store_true', help='specify learner(weka/scikit)')
    parser.add_argument('--forest', default=False, action='store_true', help='forest features')
    parser.add_argument('--balanced', dest='balanced', action='store_true', help='make a balanced tree')
    parser.add_argument('--unbalanced', dest='balanced', action='store_false', help='make an unbalanced tree')
    parser.set_defaults(balanced=False)

    args = parser.parse_args()
    if args.weka:
        learner = 'weka'
    elif args.scikit:
        learner = 'scikit'
    elif args.forest:
        learner = 'forest'
    else:
        learner = 'scikit'

    with open(os.path.join(args.idir, 'label_feature_columns')) as f:
        label_columns, feature_columns = json.load(f)
    with open(os.path.join(args.idir, 'subsystem_mapping')) as f:
        subsystem_mapping = json.load(f)
    with open(os.path.join(args.idir, args.alarm)) as f:
        labels, features = json.load(f)

    with open(args.feature_file) as f:
        selected_features = json.load(f)

    learner = DecisionTreeLearner(args.o,
                                  label_columns,
                                  feature_columns,
                                  subsystem_mapping,
                                  learner=learner,
                                  balanced=args.balanced)
    learner.learn_dt(args.alarm,
                     labels,
                     features,
                     selected_features)


if __name__ == '__main__':
    main()
