#!/usr/bin/env python3
# encoding: utf-8
"""
dt2pf.py

Created by Wannes Meert on 30-11-2016.
Copyright (c) 2016 KU Leuven. All rights reserved.
"""

import argparse
import logging
import re
import sys
from itertools import product

import numpy as np

logger = logging.getLogger(__name__)

re_natural_language = re.compile(r"[ .()]")


def clean_natural_language(name):
    if type(name) in [float, np.float64]:
        return "{:.2f}".format(name)
    return re_natural_language.sub("_", name)


def to_natural_language(feature_name, val, higher):
    pieces = feature_name.split('__')
    feat = pieces[0]
    if len(pieces) == 2:
        cat = pieces[1]
    else:
        if feat[-2:] == 'EU':
            cat = 'EU'
        elif feat[-4:] == 'VISU':
            cat = 'VISU'
        else:
            cat = 'alarm'

    if cat == 'VISU':
        return '{} {}actief is'. \
            format(feat, '' if higher else 'in')
    if cat == 'alarm':
        return '{} {}actief is'. \
            format(feat, '' if higher else 'in')
    elif cat == 'activated':
        return '{} {} dan {:.0f} minuten geleden actief werd'. \
            format(feat, 'langer' if higher else 'minder', val)
    elif cat == 'deactivated':
        return '{} {} dan {:.0f} minuten geleden inactief werd'. \
            format(feat, 'langer' if higher else 'minder', val)
    elif cat == 'num_actions':
        return '{} het laatste half uur {} dan {:.0f} keer wisselde tussen actief en inactief'. \
            format(feat, 'meer' if higher else 'minder', val)
    elif cat == 'EU':
        return 'de analoge meetwaarde van {} {} is dan {}'. \
            format(feat, 'groter' if higher else 'kleiner', val)
    elif cat == 'mean':
        return 'het laatste half uur de gemiddelde waarde van {} {} was dan {}'. \
            format(feat, 'groter' if higher else 'kleiner', val)
    elif cat == 'min':
        return 'het laatste half uur de minimale waarde van {} {} was dan {}'. \
            format(feat, 'groter' if higher else 'kleiner', val)
    elif cat == 'max':
        return 'het laatste half uur de maximale waarde van {} {} was dan {}'. \
            format(feat, 'groter' if higher else 'kleiner', val)
    elif cat == 'std':
        return 'het laatste half uur de standaarddeviatie van {} {} was dan {}'. \
            format(feat, 'groter' if higher else 'kleiner', val)
    elif cat == 'slope':
        return 'het laatste half uur de waarde van {} {} {} dan {}'. \
            format(feat, 'harder' if ((val > 0) == higher) else 'minder', 'steeg' if val > 0 else 'daalde', abs(val))


class Tree(object):
    def __init__(self, file=None):
        self.feature_names = []
        self._node_ids = []
        self.feature = []
        self.threshold = []
        self.children_left = []
        self.children_right = []
        self.value = []

        if file is not None:
            self.load_tree(file)

    def load_tree(self, file):
        with open(file) as f:
            string = f.read()
            lines = [x.strip() for x in string.split(';')]

        for line in lines:
            match = re.match(r'^(\d+) \[.+\](?:\s+)?$', line)
            if match:

                print(match.group(1))
                if int(match.group(1)) == 1000000000:
                    continue
                self._node_ids.append(match.group(1))
                condition = re.findall('label(?:\s)+?=(?:\s+)?"([^"]+)"', line)[0].split(r'\n')[0]
                feature, threshold = re.split(' [<=>]+ ', condition)
                value = re.findall('value(?:\s)+?=(?:\s+)?\[(\d+),(?:\s+)?(\d+)\]', line)[0]

                if feature not in self.feature_names:
                    self.feature_names.append(feature)

                self.feature.append(self.feature_names.index(feature))
                self.threshold.append(float(threshold))
                self.value.append([[int(x) for x in value]])
                self.children_left.append(-1)
                self.children_right.append(-1)
                continue

            match = re.match(r'^(\d+)(?:\s+)?->(?:\s+)?(\d+).*$', line)
            if match:
                parent = match.group(1)
                child = match.group(2)
                if self.children_left[self._node_ids.index(parent)] == -1:
                    self.children_left[self._node_ids.index(parent)] = self._node_ids.index(child)
                else:
                    self.children_right[self._node_ids.index(parent)] = self._node_ids.index(child)


class TreeWrapper(object):
    def __init__(self, dot_file=None):
        self.n_classes_ = 2
        self.tree_ = Tree(dot_file)
        self.n_features_ = len(self.tree_.feature_names)

        pass

    def load_tree(self, file):
        self.tree_.load_tree(file)
        self.n_features_ = len(self.tree_.feature_names)
        self.n_classes_ = 2


class Rules:
    def __init__(self, decision_tree: TreeWrapper, feature_names: list, class_names: list):
        """Create a set of rules from a given decision tree.

        :param decision_tree: Scikit-learn DecisionTreeClassifier
        :param feature_names: List of feature names
        :param class_names: List of class names
        """
        self.clf = decision_tree
        self.f_names = feature_names
        self.c_names = class_names
        self.bins = self._compute_bins()

    def to_natural_language(self, use_comparison=False, min_prob=0.0):
        """Translate the decision tree to a set of rules representing the tree.

        :param use_comparison:
        :param min_prob: Minimum probability. Reduce all probabilities lower
            than this value to 0.0 (to in effect ignore them).
        """
        parts = self._node_to_natural_language(0, [],
                                               use_comparison=use_comparison, min_prob=min_prob)

        parts.sort(key=lambda x: int(re.findall('(\d+) (?:/ \d+ )?keer', x)[0]), reverse=True)
        return '\n'.join(parts)

    def to_natural_language_facts(self):
        r, _ = self._node_to_natural_language_facts(0, [], 0)
        return r

    def _compute_bins(self):
        bins = {}
        for f in range(self.clf.n_features_):
            bins[f] = []
        for f, t in zip(self.clf.tree_.feature, self.clf.tree_.threshold):
            if f == -2:
                continue
            bins[f].append(t)
        for ts in bins.values():
            ts.sort()
        return bins

    def _get_true_bins(self, feature_idx, t_min, t_max, ex_i=None):
        bins = self.bins[feature_idx]
        # print("_get_bins {}, {}, {}, {}".format(feature_idx, t_min, t_max, bins))
        bins_str = []
        for b_min, b_max in zip([-np.inf] + bins, bins + [np.inf]):
            if b_min >= t_min and b_max <= t_max:
                if b_min == -np.inf:
                    b_min = "ninf"
                if ex_i is None:
                    ex_i_s = ""
                else:
                    ex_i_s = ",{}".format(ex_i)
                bins_str.append("f_{}({},{}{})".format(clean_natural_language(self.f_names[feature_idx]),
                                                       clean_natural_language(b_min),
                                                       clean_natural_language(b_max), ex_i_s))
        # print(bins_str)
        return bins_str

    def _node_stack_to_body(self, node_stack, ex_i=None, use_comparison=False):
        thresholds = {}
        for f in range(len(self.f_names)):
            thresholds[f] = [-np.inf, np.inf]
        for is_smaller, node_idx in node_stack:
            t = self.clf.tree_.threshold[node_idx]
            f = self.clf.tree_.feature[node_idx]
            ts = thresholds[f]
            if is_smaller:
                if t < ts[1]:
                    ts[1] = t
            else:
                if t > ts[0]:
                    ts[0] = t
            thresholds[f] = ts
        body = []
        for f, (tmin, tmax) in thresholds.items():
            if use_comparison:
                cond = None
                if tmin == -np.inf and tmax == np.inf:
                    continue
                if tmin != -np.inf:
                    cond = to_natural_language(self.f_names[f], tmin, True)
                if tmax != np.inf:
                    cond = to_natural_language(self.f_names[f], tmax, False)
                bins = [cond]
            else:
                bins = self._get_true_bins(f, tmin, tmax, ex_i)
            body.append(bins)
        return body

    def _node_to_natural_language(self, node_idx, node_stack, use_comparison=False, min_prob=0.0):
        r = []
        if self.clf.tree_.children_left[node_idx] == -1 and self.clf.tree_.children_right[node_idx] == -1:
            # Leaf
            values = self.clf.tree_.value[node_idx][0]
            total = sum(values)
            initial_values = self.clf.tree_.value[0][0]
            # initial_total = sum(initial_values)

            body = self._node_stack_to_body(node_stack, use_comparison=use_comparison)
            body_str = '\t'
            body_str += " en \n\t".join(" of ".join(bins) for bins in body)

            if values[1] / initial_values[0] <= min_prob:
                return ''
            # likelyhood = (values[1] * initial_total) / (total * initial_values[1])
            # if likelyhood <= min_prob:
            #     return ''
            #             r += '''Het alarm komt {:.0f} keer vaker voor wanneer
            # {}
            # dit is {:.0f} / {:.0f} keer (het alarm kwam {:.0f} keer op in totaal)
            #
            # '''.format(likelyhood, body_str, values[1], total, initial_values[1])
            if values[1] == total:
                r.append('''Het alarm trad steeds ({:.0f} keer) op wanneer
{}
dit is {:.0f}% van de {:.0f} voorkomende alarmen

'''.format(values[1], body_str, (100 * values[1]) // initial_values[1], initial_values[1]))
            else:
                r.append('''Het alarm trad {:.0f} / {:.0f} keer op wanneer
{}
dit is {:.0f}% van de {:.0f} voorkomende alarmen

'''.format(values[1], total, body_str, (100 * values[1]) // initial_values[1], initial_values[1]))

        else:
            # Inner node
            r += self._node_to_natural_language(self.clf.tree_.children_left[node_idx],
                                                node_stack + [(True, node_idx)],
                                                use_comparison, min_prob)
            r += self._node_to_natural_language(self.clf.tree_.children_right[node_idx],
                                                node_stack + [(False, node_idx)],
                                                use_comparison, min_prob)

        return r

    def _node_to_natural_language_facts(self, node_idx, node_stack, ex_i, only_class_idx=None, min_prob=0.0):
        r = ""
        if self.clf.tree_.children_left[node_idx] == -1 and self.clf.tree_.children_right[node_idx] == -1:
            # Leaf
            values = self.clf.tree_.value[node_idx][0]
            total = sum(values)
            # print("Leaf: {} : {} <- {}".format(node_idx, values, node_stack))
            body = self._node_stack_to_body(node_stack, "{ex_i}")
            for cur_body in product(*body):
                include = False
                for i_class in range(self.clf.n_classes_):
                    # if values[i_class] == 0:
                    #     continue
                    value = values[i_class] / total
                    if only_class_idx is not None and \
                            (i_class != only_class_idx or value <= min_prob):
                        continue
                    if value == 1.0:
                        # value = ""
                        value = "{}::".format(value)
                    else:
                        value = "{}::".format(value)
                    r += "{}{}({}).  % {}/{}\n".format(
                        value,
                        clean_natural_language(self.c_names[i_class]), ex_i,
                        values[i_class], total)
                    include = True
                if include:
                    for conj_elmt in cur_body:
                        r += "{}.\n".format(conj_elmt.format(ex_i=ex_i))
                        # for disj_elmt in conj_elmt:
                        #     r += "{}.\n".format(disj_elmt)
                    r += "%%\n"
                    ex_i += 1
        else:
            # Inner node
            r1, ex_1 = self._node_to_natural_language_facts(self.clf.tree_.children_left[node_idx],
                                                            node_stack + [(True, node_idx)], ex_i, only_class_idx,
                                                            min_prob=min_prob)
            r2, ex_2 = self._node_to_natural_language_facts(self.clf.tree_.children_right[node_idx],
                                                            node_stack + [(False, node_idx)], ex_1, only_class_idx,
                                                            min_prob=min_prob)
            r += r1 + r2
            ex_i = ex_2
        return r, ex_i


def main(argv=None):
    parser = argparse.ArgumentParser(description='Perform some task')
    parser.add_argument('--verbose', '-v', action='count', help='Verbose output')
    # parser.add_argument('--flag', '-f', action='store_true', help='Flag help')
    # parser.add_argument('--output', '-o', required=True, help='Output file')
    # parser.add_argument('--version', action='version', version='%(prog)s 1.0')
    # parser.add_argument('input', nargs='+', help='List of input files')
    args = parser.parse_args(argv)

    logger.setLevel(logging.ERROR - 10 * (0 if args.verbose is None else args.verbose))
    logger.addHandler(logging.StreamHandler(sys.stdout))

    pass


if __name__ == "__main__":
    sys.exit(main())
