import collections
import glob
import json
import os
import shutil
import subprocess

from IPython.display import Image, display

import pretty_tree

def show_dotfile(filename):
    os.system('dot -O -Tpng ' + filename)
    display(Image(filename=filename + ".png", unconfined=True))


def show_dotstring(string):
    make_dotfile(string, 'tmp')
    show_dotfile('tmp')


def make_dotfile(string, file_name='tmp'):
    with open(file_name, 'w') as f:
        f.write(string)


def find_alarms_with_best(path):
    alarm_list = [x.replace(path, '').replace('.dot', '') for x in glob.glob(path + '*')]
    best_list = [get_best_tree(x)[1] for x in alarm_list]
    return sorted(list(zip(best_list, alarm_list)))


def find_alarms(path):
    alarm_list = [['', x.replace(path, '')] for x in glob.glob(path + '*')
                  if x.replace(path, '') not in ['trees', 'subsystem_mapping', 'label_feature_columns']]
    return sorted(alarm_list)


def get_best_tree(alarm):
    def read(file):
        with open(file) as f:
            string = f.read()
        return string

    files = glob.glob('learned_output/*/*/*/' + alarm + '*')
    trees = [read(x) for x in files]
    nodes = [pretty_tree.PrettyTreeSK(tree).get_best_node() for tree in trees]
    best = files[nodes.index(max(nodes, key=lambda x:x[1]))]
    return best.split('/')[1:-1], max([x[1] for x in nodes])


def get_alarms_options(path):
    alarms = find_alarms(path)

    alarms_options = collections.OrderedDict(zip([" ".join([str(y) for y in x]).strip() for x in alarms],
                                                 [x[1] for x in alarms]))
    alarms_options = list(alarms_options.items())
    alarms_options.reverse()
    alarms_options = collections.OrderedDict(alarms_options)
    return alarms_options


def prettytree(other_components):
    minp = other_components['min_pos'].value
    node = other_components['node_threshold'].value
    
    with open('scikit_output/scikit_tree') as f:
        tree = f.read()    
    pret = pretty_tree.PrettyTreeSK(tree).get_pretty_tree(minp, node)
    make_dotfile(pret, 'scikit_output/scikit_tree_pretty')

    with open('weka_output/weka_tree') as f:
        tree = f.read()
    pret = pretty_tree.PrettyTreeWK(tree).get_pretty_tree(minp, node)
    make_dotfile(pret, 'weka_output/weka_tree_pretty')


def relearn_tree(decision_trees_path, learner, alarm, other_components, ignore_features, ignore_components,
                 selected_features):
    glo = (other_components['loc_glob'].value == 'global')
    wit = (other_components['w_wo_alarms'].value == 'with_alarms')
    bal = (other_components['un_balanced'].value == 'balanced')
    with open(os.path.join(decision_trees_path, alarm)) as f:
        labels, features = json.load(f)
    learner.learn_dt(alarm, labels, features, ignore_components=ignore_components, ignore_features=ignore_features,
                     specific=[glo, wit, bal], selected_features=selected_features)

    shutil.copy('scikit_output/scikit_tree', 'scikit_output/scikit_tree_pretty')
    shutil.copy('weka_output/weka_tree', 'weka_output/weka_tree_pretty')


def show_trees():
    subprocess.Popen('xdot scikit_output/scikit_tree_pretty', shell=True)
    subprocess.Popen('xdot weka_output/weka_tree_pretty', shell=True)


def get_used_features():
    with open('scikit_output/scikit_tree_pretty') as f:
        tree = f.read()
    sk_features = pretty_tree.PrettyTreeSK(tree).all_features()
    with open('weka_output/weka_tree_pretty') as f:
        tree = f.read()
    wk_features = pretty_tree.PrettyTreeWK(tree).all_features()

    sk_components = [x.split('__')[0] for x in sk_features]
    wk_components = [x.split('__')[0] for x in wk_features]

    used_features = sorted((set(sk_components + wk_components)))

    return used_features
