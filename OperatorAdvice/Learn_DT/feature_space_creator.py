import logging

import numpy as np
import pandas as pd
from rolling_time import rolling_time_apply as rt_apply
from rolling_time import rolling_time_apply_time_series as rt_time_series
from rolling_time import time_since as rt_time_since

from utils import add_examples


def find_features_alarm(df_status, minutes=30):
    """
    :param df_status: chronological dataframe with state of each component: active/passive (1/0)
    :param minutes: int time period considered
    :return: chronological dataframe with features
             - status of each component
             - whether a component has been activated in the last x minutes
             - whether a component has been deactivated in the last x minutes
             - the amount of changes of a component in the last x minutes
    """
    logging.info("Adding features...")
    df_edges = df_status.diff()
    t_window = str(minutes) + "min"
    logging.debug("number of actions...")

    df_actions = rt_apply(df_edges, t_window, lambda x: np.sum(np.abs(x)), exclusive=False)
    # df_activated = rolling_time.rolling_time_apply(df_edges, t_window, lambda x: np.abs(np.max(x)), exclusive=False)
    # df_deactivated = rolling_time.rolling_time_apply(df_edges, t_window, lambda x: np.abs(np.min(x)), exclusive=False)
    logging.debug("time since activated...")

    df_activated = rt_time_since(df_edges, df_edges.index, 1)
    logging.debug("time since deactivated...")

    df_deactivated = rt_time_since(df_edges, df_edges.index, -1)
    df_actions.columns = [x + "__num_actions" for x in df_status.columns]
    df_activated.columns = [x + "__activated" for x in df_status.columns]
    df_deactivated.columns = [x + "__deactivated" for x in df_status.columns]
    df_status.index = pd.to_datetime(df_status.index)

    features_l = [df_status, df_activated, df_deactivated, df_actions]

    features = pd.concat(features_l, axis=1)
    logging.info("Features added!")
    return features


def find_features_sensor(df_status, time_series, analog=False, minutes=30):
    """
    :param df_status: chronological dataframe with state of each component: active/passive (1/0)
    :param minutes: int time period considered
    :param time_series: time stamps at which to generate features
    :param analog: whether or not the read values are
    :return: chronological dataframe with features
             - status of each component
             - whether a component has been activated in the last x minutes
             - whether a component has been deactivated in the last x minutes
             - the amount of changes of a component in the last x minutes
    """
    t_window = str(minutes) + "min"
    logging.info("Adding features...")

    if not analog:
        df_edges = df_status.diff()
        df_state = add_examples(df_status, time_series, is_list=True, delete_rest=True)
        df_state.columns.name = None
        df_actions = rt_time_series(df_edges, t_window, lambda x: np.sum(np.abs(x)), time_series)
        df_activated = rt_time_since(df_edges, time_series, 1)
        df_deactivated = rt_time_since(df_edges, time_series, -1)

        df_actions.columns = [x + "__num_actions" for x in df_status.columns]
        df_activated.columns = [x + "__activated" for x in df_status.columns]
        df_deactivated.columns = [x + "__deactivated" for x in df_status.columns]

        features_l = [df_state, df_activated, df_deactivated, df_actions]
    else:
        df_state = add_examples(df_status, time_series, is_list=True, delete_rest=True)
        df_state.columns.name = None

        df_min = rt_time_series(df_status, t_window, lambda x: np.min(x), time_series)
        df_max = rt_time_series(df_status, t_window, lambda x: np.max(x), time_series)
        df_mean = rt_time_series(df_status, t_window, lambda x: np.mean(x), time_series)
        df_slope = rt_time_series(df_status, t_window, lambda x: x.iloc[-1] - x.iloc[0], time_series)
        df_std = rt_time_series(df_status, t_window, lambda x: np.std(x), time_series)

        df_min.columns = [x + "__min" for x in df_status.columns]
        df_max.columns = [x + "__max" for x in df_status.columns]
        df_mean.columns = [x + "__mean" for x in df_status.columns]
        df_slope.columns = [x + "__slope" for x in df_status.columns]
        df_std.columns = [x + "__std" for x in df_status.columns]

        features_l = [df_state, df_min, df_max, df_mean, df_slope, df_std]

    df_state.index = pd.to_datetime(df_state.index)

    features = pd.concat(features_l, axis=1)
    logging.info("Features added!")

    return features
