import re


class Itemset(object):
    def __init__(self, size=None, items=None, support=None, line=None):
        if line:
            items = set(re.findall(r'[^ ][^=]+=[^ ]*', line))
            size = len(items)
            support = int(re.findall(r'[ ][^ ]*[ ]*$', line)[0])

        self.size = size
        self.items = items
        self.support = support

    def overlap(self, other_set, threshold):
        items = self.items - other_set.items
        if len(items) == 0:
            return self.support < threshold * other_set.support
        else:
            return False

    def __str__(self):
        return 'itemset of size ' + str(self.size) + ', with support ' + str(self.support) + ' \nitems:\n' + '\n'.join(self.items)


class ItemsetGroup(object):
    def __init__(self, filename=None, string=None):
        if not string:
            with open(filename) as f:
                string = f.read()
        self.sets = self.parse_string(string)

    @staticmethod
    def parse_string(string):
        delimiter1 = r'Generated sets of large itemsets:'
        delimiter2 = r'Best rules found:'
        delimiter3 = r'Size of set of large itemsets L\([0-9]+\):.+[\n]+Large Itemsets L\([0-9]+\):[\n]+'
        interesting_part = re.findall(delimiter1 + r'([\s\S]+)' + delimiter2, string)[0]
        regex = delimiter3 + r'([\s\S]+?)' + r'\n\n'
        itemset_groups = re.findall(r'(?=' + regex + r')', interesting_part)

        sets = set()
        for group in itemset_groups:
            for line in group.split('\n'):
                sets.add(Itemset(line=line))
        return sets

    def compare(self, other_group, threshold):
        unique_sets = set()
        for x in self.sets:
            if not any([x.overlap(y, threshold) for y in other_group.sets]):
                unique_sets.add(x)
        print(len(unique_sets))
        return

    def __str__(self):
        return 'itemset of ' + str(len(self.sets)) + ' sets'


def main():
    pos = ItemsetGroup(filename='/home/bae/Documents/EXperT/Weka_input/association_set_positives.model')
    neg = ItemsetGroup(filename='/home/bae/Documents/EXperT/Weka_input/association_set_negatives.model')

    print(pos)
    print(neg)
    number_positives = 132
    number_negatives = 1790
    threshold = 1 * number_positives / number_negatives
    pos.compare(neg, threshold)


if __name__ == '__main__':
    main()