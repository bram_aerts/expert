import json
import os
import re
import subprocess

import pandas as pd
import itertools


class Itemset(object):
    def __init__(self, line=None, support=None, items=None):
        if support is not None and items is not None:
            self.support = support
            self.items = items
            return
        self.support = int(re.findall(r'[ ][^ ]*[ ]*$', line)[0])
        items_l = set(re.findall(r'[^ ][^=]+=[^ ]*', line))
        items_d = dict()
        for item in items_l:
            feature = re.findall(r"^.+(?==)", item)[0]
            item = item.replace(feature + '=', '')
            try:
                found = re.findall(r"(?<='\()(?:[-\d.]+|-inf)(?=-)", item)[0]

                low = float(found)
                high = float(re.findall(r"(?<=-)(?:[-\d.]+|inf)(?=(?:\]|\))')", item.replace(found, ''))[0])
            except IndexError:
                low = high = float(item.replace("'", ''))

            items_d[feature] = (low, high)

        self.items = items_d

    @property
    def set(self):
        return self.items

    def overlap(self, other_set):
        return set(self.set) == set(other_set.set)

    def __str__(self):
        return 'itemset of size ' + str(len(self.items)) + ', with support ' + str(
            self.support) + ' \nitems:\n' + '\n'.join(self.items)


class ItemsetGroup(object):
    def __init__(self, filename=None, string=None):
        if filename is not None:
            with open(filename) as f:
                string = f.read()
        if string is not None:
            self.sets = self.parse_string(string)
        else:
            self.sets = []

    def add_itemset(self, itemset):
        self.sets.append(itemset)

    @property
    def itemsets(self):
        return [iset.set for iset in self.sets]

    @staticmethod
    def parse_string(string):
        delimiter1 = r'Generated sets of large itemsets:'
        delimiter2 = r'Best rules found:'
        delimiter3 = r'Size of set of large itemsets L\([0-9]+\):.+[\n]+Large Itemsets L\([0-9]+\):[\n]+'
        interesting_part = re.findall(delimiter1 + r'([\s\S]+)' + delimiter2, string)[0]
        regex = delimiter3 + r'([\s\S]+?)' + r'\n\n'
        itemset_groups = re.findall(r'(?=' + regex + r')', interesting_part)

        sets = list()
        for group in itemset_groups:
            for line in group.split('\n'):
                if re.match(r'[ \d]+', line):
                    continue
                sets.append(Itemset(line=line))
        return sets

    def compare(self, other_group, total_pos, total_neg):
        unique_sets = list()
        for pos, neg in itertools.product(self.sets, other_group.sets):
            if pos.overlap(neg):
                unique_sets.append((pos, neg, pos.support / total_pos - neg.support / total_neg))

        print(len(unique_sets))
        return unique_sets

    def __str__(self):
        return 'itemsetgroup of ' + str(len(self.sets)) + ' sets'


def column_name_mapper(column_name):
    return re.sub('^[^a-zA-Z_]', '', re.sub('[^a-zA-Z0-9_]', '', column_name))


def combination_in_negatives(df, combination_to_investigate):
    query = '(label == 0) &'
    query += ' & '.join(['(' + str(low) + '<=' + column_name_mapper(feature) + '<=' + str(high) + ')'
                        for feature, (low, high) in combination_to_investigate.items()])
    dfs = df.query(query)
    number = dfs.shape[0]
    return number

    # number_of_occurences = 0
    # for lab, feat_row in zip(labels, features):
    #     if lab == 1:
    #         continue
    #     if all([low <= feat_row[fcols.index(feature)] <= high
    #             for feature, (low, high) in combination_to_investigate.items()]):
    #         number_of_occurences += 1
    # return number_of_occurences


def main():
    path = os.path.join('/', 'home', 'bae', 'Documents', 'EXperT', 'learned_trees_burst_2')
    with open(os.path.join(path, 'label_feature_columns')) as f:
        lcols, fcols = json.load(f)

    with open(os.path.join(path, 'TTWW-420-02-003: TEMPERATUURMETING WARMWATER UNIT WW-T420-02 HOOG ALARM')) as f:
        labels, features = json.load(f)

    command = r'''java -classpath ~/Programs/weka-3-8-0/weka.jar weka.associations.FilteredAssociator -t /tmp/weka_input.arff \
-F "weka.filters.unsupervised.attribute.NumericCleaner \
    -min -1.7976931348623157E308 -min-default -1.7976931348623157E308 \
    -max 1.7976931348623157E308 -max-default 1.7976931348623157E308 \
    -closeto 0.0 -closeto-default 0.0 -closeto-tolerance 1.0E-6 -R first-last -decimals -1" -c -1 \
    -W weka.associations.FilteredAssociator -- \
-F "weka.filters.unsupervised.attribute.Discretize -B 10 -M -1.0 -R first-last" -c -1 \
    -W weka.associations.FilteredAssociator -- \
-F "weka.filters.supervised.attribute.AttributeSelection \
    -E \"weka.attributeSelection.CfsSubsetEval -P 1 -E 1\" -S \"weka.attributeSelection.BestFirst -D 1 -N 5\"" -c -1 \
    -W weka.associations.FilteredAssociator -- \
-F "weka.filters.unsupervised.instance.RemoveWithValues -S 0.5 -C last -L first" -c -1 \
    -W weka.associations.Apriori -- \
-I -N 100 -T 0 -C 0.9 -D 0.05 -U 1.0 -M 0.1 -S -1.0 -A -c -1 '''

#     command = r'''java -classpath ~/Programs/weka-3-8-0/weka.jar weka.associations.FilteredAssociator -t /tmp/weka_input.arff \
# -F "weka.filters.supervised.attribute.Discretize
#     -R first-last -precision 6" -c 3780 \
#     -W weka.associations.FilteredAssociator -- \
# -F "weka.filters.supervised.attribute.AttributeSelection
#     -E \"weka.attributeSelection.InfoGainAttributeEval \"
#     -S \"weka.attributeSelection.Ranker -T -1.7976931348623157E308 -N 100\"" -c -1 \
#     -W weka.associations.FilteredAssociator -- \
# -F "weka.filters.unsupervised.instance.RemoveWithValues -S 0.5 -C last -L first" -c -1 \
#     -W weka.associations.Apriori -- \
# -I -N 10 -T 0 -C 0.9 -D 0.05 -U 1.0 -M 0.1 -S -1.0 -A -c -1 '''

    file_location = os.path.join('/', 'tmp', 'temporary_weka_output')
    subprocess.call(command + ' > ' + file_location, shell=True)

    itemsetgroup = ItemsetGroup(filename=file_location)
    df = pd.DataFrame(features)
    df.columns = map(column_name_mapper, fcols)
    df['label'] = pd.Series(labels)

    number_set = dict()
    for i, itemset in enumerate(itemsetgroup.itemsets):
        number = combination_in_negatives(df, itemset)
        if number in number_set:
            number_set[number].append(itemset)
        else:
            number_set[number] = [itemset]

    def component(feature):
        return feature.split('__')[0]

    negative_itemset_group = ItemsetGroup()
    for number, sets in sorted(number_set.items()):
        set_of_sets = set()
        for s in sets:
            new_set = frozenset((x[0], tuple(x[1])) for x in s.items())

            set_of_sets.add(new_set)
        # print(number, '(' + str(len(set_of_sets)) + ' sets)')

        for x in set_of_sets:
            if len(set_of_sets) > 1 and any(x < y for y in set_of_sets if x != y, ):
                continue
            negative_itemset_group.add_itemset(Itemset(items=dict(y for y in list(x)), support=number))
            # pprint.pprint(x)
    x = itemsetgroup.compare(negative_itemset_group, 136, 3114)
    x = sorted(x, key=lambda x: -x[-1])
    print(x)


if __name__ == '__main__':
    main()

