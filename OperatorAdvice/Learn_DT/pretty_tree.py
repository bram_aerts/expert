import argparse
import re
import sys
from ast import literal_eval

import pydot


#
# class Node(object):
#     def __init__(self, criterion, positives, negatives, id):
#         self._criterion = criterion
#         self.id = id
#         self._positives = positives
#         self._negatives = negatives
#         self._left_child = None
#         self._right_child = None
#
#     @property
#     def criterion(self):
#         return self._criterion
#
#     @property
#     def positives(self):
#         return self._positives
#
#     @property
#     def negatives(self):
#         return self._negatives
#
#     @property
#     def left_child(self):
#         return self._left_child
#
#     @left_child.setter
#     def left_child(self, node):
#         self._left_child = node
#
#     @left_child.deleter
#     def left_child(self):
#         del self._left_child
#
#     @property
#     def right_child(self):
#         return self._right_child
#
#     @right_child.setter
#     def right_child(self, node):
#         self._right_child = node
#
#     @right_child.deleter
#     def right_child(self):
#         del self._right_child
#
#     @property
#     def samples(self):
#         return self.positives + self.negatives
#
#     @property
#     def gini(self):
#         return 1 - (self.positives**2 + self.negatives**2)/ self.samples**2
#
#     @property
#     def is_class(self):
#         return self.positives > self.negatives
#
#     def _to_string_node(self):
#         if self.is_class:
#             color = 'e58139'
#         else:
#             color = '399de5'
#         factor = abs((self.positives - self.negatives) / self.samples)
#         saturation = hex(int(round(factor * 255)))[-2:].replace('x', '0')
#         fillcolor = '#' + color + saturation
#         string = ' '.join([self.id,
#                            'fillcolor="' + fillcolor + '",',
#                            'label="' + self.criterion,
#                            '\nsamples=' + self.samples,
#                            '\nvalue=[' + self.negatives + ',' + self.positives + ']', ','
#                            'penwidth = '])
#
#
#
#
# class Tree(object):
#     def __init__(self, string):


class PrettyTree(object):
    def __init__(self, string):
        self.tree = pydot.graph_from_dot_data(string)
        self.pos_factor, self.neg_factor = 1, 1
        self.pos_factor, self.neg_factor = self.find_pos_neg_factor()
        self.positives, self.negatives = self.find_pos_neg()
        self.ginis = {}

    def find_ginis(self, node_id):
        children = self.find_children(node_id)
        if children:
            feature = self.get_feature_text(node_id)
            if feature not in self.ginis:
                self.ginis[feature] = 0

            self.ginis[feature] += self.get_gini_gain(node_id, children)
            self.find_ginis(children[0])
            self.find_ginis(children[1])

    def get_pretty_tree(self, min_pos, best_nodes_threshold, maxrat=None, minrat=None):
        self.find_ginis('0')
        self.iterate_tree('0', min_pos, best_nodes_threshold, maxrat, minrat)
        return self.tree.to_string()

    def get_gini(self, node):
        pos, neg = self.get_pos_neg(node)
        poss = pos / self.positives * (self.positives + self.negatives) / 2
        negg = neg / self.negatives * (self.positives + self.negatives) / 2
        return 1 - (poss ** 2 + negg ** 2) / (poss + negg) ** 2

    def get_feature_text(self, node_id):
        node = self.get_node(node_id)
        matches = re.search(r'^"([^<>=]+)(?:[<>=])', node[0]['attributes']['label'])
        return matches.groups(0)[0]

    def get_samples(self, node_id):
        node = self.get_node(node_id)
        matches = re.search(r'samples = ([0-9]+)', node[0]['attributes']['label'])
        return float(matches.groups(0)[0])

    def get_class(self, node_id):
        pos, neg = self.get_pos_neg(node_id)
        return pos > neg

    def get_pos_neg(self, node_id):
        node = self.get_node(node_id)
        matches = re.search(r'value = (\[[0-9,\. ]+\])', node[0]['attributes']['label'])
        neg, pos = eval(matches.groups(0)[0])
        pos, neg = int(round(pos * self.pos_factor)), int(round(neg * self.neg_factor))
        return pos, neg

    def get_node(self, node_id):
        try:
            return self.tree.obj_dict['nodes'][node_id]
        except KeyError:
            return None

    def find_children(self, node_id):
        node_ids = [edge[1] for edge in self.get_out_edges(node_id)]
        return node_ids

    def get_out_edges(self, node_id):
        return [edge for edge in self.tree.obj_dict['edges'].keys() if edge[0] == node_id]

    def get_in_edges(self, node_id):
        return [edge for edge in self.tree.obj_dict['edges'].keys() if edge[1] == node_id]

    def set_border_thickness(self, node_id, thickness):
        self.get_node(node_id)[0]['attributes']['penwidth'] = "%1.3f" % thickness

    def set_node_color(self, node_id, color):
        self.get_node(node_id)[0]['attributes']['fillcolor'] = color

    def set_edge_thickness(self, to_node_id, thickness):
        edges = [edge for edge in self.tree.obj_dict['edges'].keys() if edge[1] == to_node_id]
        for edge in edges:
            self.tree.obj_dict['edges'][edge][0]["attributes"]["penwidth"] = "%1.3f" % thickness

    def get_gini_gain(self, node_id, children):
        gini = self.get_gini(node_id)
        samples = self.get_samples(node_id)
        child_ginis = [self.get_gini(child_id) for child_id in children]
        child_samples = [self.get_samples(child_id) for child_id in children]
        gini_gain = gini - sum([x[0] * x[1] / samples for x in zip(child_ginis, child_samples)])
        return gini_gain

    def thicken_border(self, node_id):
        feature = self.get_feature_text(node_id)
        gini_gain = self.ginis[feature]
        self.set_border_thickness(node_id, min(6, max(1, (gini_gain + 0.1) * 10)))

    def thicken_edge(self, node_id, children):
        pos, _neg = self.get_pos_neg(node_id)
        child_positives = [self.get_pos_neg(child_id)[0] for child_id in children]
        for child, child_pos in zip(children, child_positives):
            self.set_edge_thickness(to_node_id=child, thickness=max(1, 4 * child_pos / pos))

    def color_node(self, node_id, threshold, minrat):
        positives, negatives = self.get_pos_neg(node_id)
        if not self.find_children(node_id) and positives / (positives + negatives) > minrat \
                and positives >= threshold > 0:
            self.set_node_color(node_id, "#22CC33")

    def recursive_delete(self, node_id):
        children = self.find_children(node_id)
        edges = self.get_out_edges(node_id)
        if children:
            for child in children:
                self.recursive_delete(child)
        del self.tree.obj_dict['nodes'][node_id]
        for edge in edges:
            del self.tree.obj_dict['edges'][edge]

    def prune_tree(self, node_id, min_pos):
        pos, _neg = self.get_pos_neg(node_id)
        if pos < min_pos:
            self.recursive_delete(node_id)
            return True
        return False

    def recursive_delete_ratio(self, node_id):
        children = self.find_children(node_id)
        edges = self.get_out_edges(node_id)
        if children:
            for child in children:
                self.recursive_delete_ratio(child)
        del self.tree.obj_dict['nodes'][node_id]
        for edge in edges:
            del self.tree.obj_dict['edges'][edge]

    def prune_tree_ratio(self, node_id, maxrat=None, minrat=None):
        if maxrat is None:
            maxrat = 1
        if minrat is None:
            minrat = 0
        pos, neg = self.get_pos_neg(node_id)
        if minrat > pos / (pos + neg) or pos / (pos + neg) > maxrat:
            for child_id in self.find_children(node_id):
                self.recursive_delete_ratio(child_id)
            for edge in self.get_out_edges(node_id):
                del self.tree.obj_dict['edges'][edge]

    def iterate_tree(self, node_id, min_pos, best_nodes_threshold, maxrat=None, minrat=None):
        self.prune_tree_ratio(node_id, maxrat, minrat)
        pruned = self.prune_tree(node_id, min_pos)

        children = self.find_children(node_id)

        if not children:
            if not pruned:
                self.color_node(node_id, best_nodes_threshold, minrat)
            return

        self.thicken_border(node_id)
        self.thicken_edge(node_id, children)
        for child in children:
            self.iterate_tree(child, min_pos, best_nodes_threshold, maxrat, minrat)

    def find_pos_neg_factor(self):
        node = self.get_node('1000000000')
        matches = re.search(r'positives: ([0-9]+(?:.[0-9]+)?) / ([0-9]+(?:.[0.9]+)?)', node[0]['attributes']['label'])
        positives = eval(matches.groups(0)[0])
        total = eval(matches.groups(0)[1])
        negatives = total - positives
        pos, neg = self.get_pos_neg('0')

        return positives / pos, negatives / neg

    def find_pos_neg(self):
        node = self.get_node('1000000000')
        matches = re.search(r'positives: ([0-9]+(?:.[0-9]+)?) / ([0-9]+(?:.[0.9]+)?)', node[0]['attributes']['label'])
        positives = eval(matches.groups(0)[0])
        total = eval(matches.groups(0)[1])
        negatives = total - positives
        return positives, negatives

    def find_best_node(self, node_id):
        gini = self.get_gini(node_id)
        samples = self.get_samples(node_id)
        class_ = self.get_class(node_id)
        children = self.find_children(node_id)
        if not children and gini == 0 and class_:
            return node_id, samples
        elif not children:
            return node_id, 0

        child_scores = [self.find_best_node(child_id) for child_id in children]

        best_child = max(child_scores, key=lambda x: x[1])

        return best_child

    def get_best_node(self):
        best_node = self.find_best_node('0')
        return best_node

    def get_feature(self, node_id):
        node = self.get_node(node_id)
        return re.split('[<>=]', node[0]['attributes']['label'])[0][1:-1]

    def find_used_features(self, node_id, features):
        children = self.find_children(node_id)
        if children:
            features.append(self.get_feature(node_id))
        for child in children:
            self.find_used_features(child, features)

    def all_features(self):
        features = list()
        self.find_used_features('0', features)
        return features

    def get_depth(self, node_id):
        if node_id == '0':
            return 0
        edges = self.tree.obj_dict['edges']
        for edge in edges:
            if edge[1] == node_id:
                return 1 + self.get_depth(edge[0])
        return None

    def get_average_depth(self):
        sum_depth = 0
        all_ids = set(self.tree.obj_dict['nodes']) - {'1000000000', 'node', 'edge'}
        for node_id in all_ids:
            if not self.find_children(node_id):
                depth = self.get_depth(node_id)
                number = self.get_pos_neg(node_id)[0]
                sum_depth += depth * number
        average_depth = sum_depth / self.get_pos_neg('0')[0]
        return average_depth

    def get_confusion_matrix(self):
        node = self.get_node('1000000000')
        matches = re.search(r'(\[\[(?:.|\n)*\]\])', node[0]['attributes']['label'])
        matrix = literal_eval(re.sub('(?<!(\s|\[))\s+(?!($|\s|\]))', ',', matches.groups(0)[0]))
        return matrix


class PrettyTreeWK(PrettyTree):
    pass


class PrettyTreeSK(PrettyTree):
    pass


def main():
    parser = argparse.ArgumentParser(description='prettyfy a tree')
    parser.add_argument('-i', required=True, metavar='INPUT', type=str, help='input file')
    parser.add_argument('-o', required=True, metavar='OUTPUT', type=str, help='output file')
    parser.add_argument('-minpos', metavar='MinimalPositives', type=int, default=0,
                        help='minimal positives in a node')
    parser.add_argument('-highlight', metavar='HighlightNodes', type=int, default=0,
                        help='highlight nodes with more positives')
    parser.add_argument('-minrat', metavar='MinimalRatio', type=float, default=0,
                        help='hide nodes with lower pos/neg ratio')
    parser.add_argument('-maxrat', metavar='MaximalRatio', type=float, default=1,
                        help='hide nodes with higher pos/neg ratio')

    args = parser.parse_args()

    with open(args.i) as f:
        string = f.read()

    tree = PrettyTree(string)

    with open(args.o, 'w') as f:
        f.write(tree.get_pretty_tree(args.minpos,
                                     args.highlight,
                                     maxrat=args.maxrat,
                                     minrat=args.minrat))


if __name__ == "__main__":
    main()
