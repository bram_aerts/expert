import logging
from pandas import Series, DataFrame
import pandas as pd
from datetime import datetime, timedelta
import numpy as np
import bisect


def rolling_time_apply_at_ts(data, window, function, timestamp, min_periods_i=1, exclusive=False):
    milli = timedelta(0, 0, 1)
    from_timestamp = timestamp - pd.datetools.to_offset(window).delta - milli
    to_timestamp = timestamp - (milli if exclusive else timedelta(0))
    dslice = data[from_timestamp: to_timestamp]

    if isinstance(dslice, pd.DataFrame) and dslice.shape[0] < min_periods_i:
        df = pd.DataFrame([np.zeros(len(data.columns))])
        df.columns = data.columns
        return function(df)
    elif isinstance(dslice, pd.Series) and dslice.size < min_periods_i:
        return 0
    else:
        return function(dslice)


def rolling_time_apply_time_series(data, window, function, time_series, exclusive=False):
    logging.debug("rolling time: apply time series...")
    df_function = pd.DataFrame([rolling_time_apply_at_ts(data, window, function, x, exclusive=exclusive)
                                for x in time_series])
    df_function = df_function.set_index(time_series)
    logging.debug("rolling time: apply time series!")
    return df_function


def time_since(data, time_series, value, max_time_influence=600):
    def func_numba():
        time_since_array = []
        for c in df2:
            col = df2[c].dropna()
            time_since_array.append([])
            for t in time_series:
                index = bisect.bisect(col.index, t) - 1
                if index == -1:
                    before = df2.index[0]
                else:
                    before = col.index[index]
                tdelta = (t - before)
                time_since_array[-1].append(min(max_time_influence, tdelta.days * 24 * 60 + tdelta.seconds // 60))
        return time_since_array

    logging.debug("rolling time: time since...")
    df2 = data.copy()
    logging.debug("rolling time: replacing...")
    df2[df2 != value] = np.nan
    logging.debug("rolling time: executing function...")

    df_function = pd.DataFrame(np.transpose(func_numba()))
    logging.debug("rolling time: setting index and cols...")

    df_function = df_function.set_index(time_series)
    df_function.columns = data.columns
    logging.debug("rolling time: time since!")

    return df_function


def rolling_time_apply(data, window, function, min_periods_i=1, exclusive=False):
    """ Function that computes features of a rolling window with fixed duration

    Parameters
    ----------
    data : DataFrame or Series
           If a DataFrame is passed, the rolling_time is computed for all columns.
    window : int or string
             If int is passed, window is the number of observations used for calculating
             the statistic, as defined by the function pd.rolling_mean()
             If a string is passed, it must be a frequency string, e.g. '90S'. This is
             internally converted into a DateOffset object, representing the window size.
    function : function
               apply this function to the rolling window
    min_periods_i : int
                  Minimum number of observations in window required to have a value.
    exclusive : Bool,
                If True do not use information about current timeslot

    Returns
    -------
    Series or DataFrame, if more than one column
    """

    def calculate_mean_at_ts(ts):
        milli = timedelta(0, 0, 1)
        from_timestamp = ts - pd.datetools.to_offset(window).delta - milli

        to_timestamp = ts - (milli if exclusive else timedelta(0))
        dslice = data[from_timestamp: to_timestamp]

        if isinstance(dslice, pd.DataFrame) and dslice.shape[0] < min_periods_i:
            # noinspection PyUnresolvedReferences
            df = pd.DataFrame([np.zeros(len(data.columns))])
            # noinspection PyUnresolvedReferences
            df.columns = data.columns
            # return [x for x in function(df)]
            return function(df)
        elif isinstance(dslice, pd.Series) and dslice.size < min_periods_i:
            return 0
        else:
            return function(dslice)

    if isinstance(window, int):
        # noinspection PyUnresolvedReferences
        return_value = pd.rolling(window).apply(function)
    elif isinstance(window, str):
        idx_ser = pd.Series(data.index, index=data.index)
        return_value = idx_ser.apply(calculate_mean_at_ts)
    else:
        return_value = None
    return return_value


def main():
    # Example
    idx = [datetime(2011, 2, 7, 0, 0),
           datetime(2011, 2, 7, 0, 1),
           datetime(2011, 2, 7, 0, 1, 30),
           datetime(2011, 2, 7, 0, 1, 30),
           datetime(2011, 2, 7, 0, 2),
           datetime(2011, 2, 7, 0, 4),
           datetime(2011, 2, 7, 0, 5),
           datetime(2011, 2, 7, 0, 5, 10),
           datetime(2011, 2, 7, 0, 6),
           datetime(2011, 2, 7, 0, 8),
           datetime(2011, 2, 7, 0, 9)]
    idx = pd.Index(idx)
    vals = np.asarray([np.nan, 0, 1, 0, -1, 0, 0, 0, 1, 0, -1])
    s = Series(vals, index=idx)
    rm = rolling_time_apply(s, '3min', lambda x: np.sum(np.abs(x)))
    print(rm)


if __name__ == "__main__":
    main()
