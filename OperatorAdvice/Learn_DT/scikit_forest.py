from sklearn import ensemble, cross_validation, metrics
import os
import numpy as np
from matplotlib import pyplot as plt
import seaborn


class ScikitLearner(object):
    def __init__(self, alarm_name, columns, labels, features, output_path):
        self.alarm_name = alarm_name
        self.columns = columns
        self.labels = labels
        self.features = features
        self.output_path = output_path
        self.clf = ensemble.ExtraTreesClassifier(n_estimators=200, random_state=0)
        self.clf.fit(self.features, self.labels)

        importances = self.clf.feature_importances_
        std = np.std([tree.feature_importances_ for tree in self.clf.estimators_], axis=0)
        indices = np.argsort(importances)[::-1]

        # Print the feature ranking
        print("Feature ranking:")

        shape_ = 40
        indices = indices[:shape_]

        for f in range(shape_):
            print("%d. %100s (%f)" % (f + 1, columns[indices[f]], importances[indices[f]]))

        # Plot the feature importances of the forest
        # plt.figure()
        # plt.title("Feature importances")
        # plt.bar(range(shape_), importances[indices],
        #         color="r", yerr=std[indices], align="center")
        # plt.xticks(range(shape_), [columns[x] for x in indices], rotation=-90)
        # plt.xlim([-1, shape_])
        # plt.show()


