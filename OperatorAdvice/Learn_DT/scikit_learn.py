from sklearn import tree, cross_validation, metrics
import os


class ScikitLearner(object):
    def __init__(self, alarm_name, columns, labels, features, output_path, options=None):
        self.alarm_name = alarm_name
        self.columns = columns
        self.labels = labels
        self.features = features
        self.output_path = output_path
        self.clf = tree.DecisionTreeClassifier(**options)
        self.confusion_matrix = self._get_confusion_matrix()
        self._create_dotfile()

    def get_confusion_matrix(self):
        return self.confusion_matrix

    def _get_confusion_matrix(self):
        predicted = cross_validation.cross_val_predict(self.clf, self.features, self.labels, cv=3)
        confusion_matrix = metrics.confusion_matrix(self.labels, predicted)
        return confusion_matrix

    def _create_dotfile(self):
        model = self.clf.fit(self.features, self.labels)
        try:
            tree.export_graphviz(model, feature_names=self.columns, class_names=["false", "true"], filled=True,
                                 rounded=True)
        except ValueError:
            tree.export_graphviz(model, feature_names=self.columns, class_names=["false", "true"], rounded=True)

        extra_text = "positives: " + str(sum(self.labels)) + " / " + str(len(self.labels))

        with open('tree.dot') as f:
            string = f.read()
        print_string = string.split("}")[0]
        print_string += '1000000000 [label="' + self.alarm_name + "\n"
        print_string += extra_text + '\n' + str(self.confusion_matrix) + '" fillcolor="#FFFFFF"]\n'
        print_string += '}\n'
        if os.path.isdir(self.output_path):
            file_name = os.path.join(self.output_path, 'scikit_tree')
        else:
            file_name = self.output_path
        with open(file_name, "w") as f:
            print(print_string, file=f)
