import logging

import numpy as np
import pandas as pd

from utils import add_examples


def read_csv(csv_file, columns, subsystem=None):
    """
    :param csv_file: filename of csv file
    :param columns: specify columns in csv file, _name means drop,
                    required: timestamp, status, component, _group
    :param subsystem: which subsystem to use (default None)
    :return: dataframe, indexed by timestamp
             (0.0: inactive, 1.0: active)
    """
    logging.info('reading csv...')
    df = pd.read_csv(csv_file, header=None)

    df.columns = columns

    df['timestamp'] = pd.to_datetime(df['timestamp'])

    if subsystem:
        df = df[(df['_group'] == subsystem)]

    logging.debug('dropping unnecessary columns...')

    df.drop([x for x in df.columns if x[0] == "_"], axis=1, inplace=True)
    logging.debug('sorting indexes...')

    df.sort_values(by="timestamp", inplace=True)
    df.set_index("timestamp", inplace=True)
    df.reset_index(inplace=True)

    logging.info('read csv finished!')

    return df


def process_status(df, status_type):
    logging.info('updating status...')

    if status_type == "alarm":
        df["status"] = df["status"].apply(lambda x: 0.0 if x == 2 else float(x))
    elif status_type == "visu":
        # def xor(x, y):
        #     return 1 if (x and not y) or (y and not x) else 0
        # df["status1"] = df["status"].apply(lambda x: xor(x & 2**19, x & 2**9))
        # df["status2"] = df["status"].apply(lambda x: xor(x & 2**20, x & 2**9))
        # df["status3"] = df["status"].apply(lambda x: xor(x & 2**12, x & 2**9))
        pass
    elif status_type == "eu":
        pass
    else:
        raise ValueError('type should be either alarm, visu or eu')
    logging.info('status updated!')
    return df


def create_status_space(df, status_type, add_negatives=False):
    """
    :param df: dataframe, chronological list of changes in inputlevel
    :param status_type: 'visu', 'eu' or 'alarm'
    :param add_negatives: Bool, if True then add extra negative examples every hour
    :return: chronological dataframe with a column for each distinct input parameter
    """
    def _create(column_name):
        status = df2[column_name]
        first_values = [status[c][status[c].first_valid_index()] for c in status]
        initial_values = [list(map(lambda y: 1.0 if y == 0.0 else 0.0 if y == 1.0 else y, first_values))]

        for i, col in enumerate(status):
            x = status[col][status.index[0]]
            if x is None or np.isnan(x):
                df2.loc[status.index[0], (column_name, col)] = initial_values[0][i]

        if add_negatives:
            df_status = add_examples(df2[column_name], 3600)
        else:
            df_status = df2[column_name]

        if status_type == 'visu':
            extra = ' (' + column_name + ')'
        else:
            extra = ''
        df_status.columns = [x + extra for x in df2[column_name].columns]

        logging.debug("filling gaps...")
        df_status.fillna(method="pad", inplace=True)

        if 'burst' in df2:
            if add_negatives:
                df_burst = add_examples(df2['burst'], 3600, pad=False)
            else:
                df_burst = df2['burst']
            df_burst.fillna(value=False, inplace=True)
            df_status = df_status[df_burst != True]

        return df_status

    def _convert_bit(bit):
        conversion_to = list(range(24, 32)) + list(range(16, 24)) + list(range(8, 16)) + list(range(0, 8))
        return conversion_to[bit]

    def _add_visu_columns():
        logging.debug('_add_visu_columns() ...')
        _df = pd.DataFrame()

        for column_name in df2.status:
            if name in bits[type_map[column_name]].keys():
                logging.debug('making additional column ...')
                bit = bits[type_map[column_name]][name]

                def __and_with_bit(x):
                    if np.isnan(x):
                        return x
                    else:
                        return 1 if (int(x) & 2**_convert_bit(bit)) else 0

                _df[(name, column_name)] = df2.status[column_name].apply(__and_with_bit)
        logging.debug('_add_visu_columns() executed!')
        return _df

    logging.info("creating status space...")

    df2 = df.pivot_table(index="timestamp", columns=["component"], aggfunc=np.max)

    if status_type == 'visu':
        from config.config import config
        bits = config.visu_bits
        type_map = config.visu_type_mapping
        all_bit_names = set(item for sublist in [x.keys() for x in bits.values()] for item in sublist) - {'status'}
        logging.info('creating initial columns...')
        for name in all_bit_names:
            df2 = pd.concat([df2, _add_visu_columns()], axis=1)
        for col in df2.status:
            del df2[('status', col)]
        used_bit_names = list(set(x[0] for x in df2))
        logging.info('creating resulting columns...')
        states = [_create(column_name) for column_name in used_bit_names]
        df_status_out = pd.concat(states, axis=1)

    else:
        df_status_out = _create('status')

    logging.info("status space created!")

    return df_status_out


def create_status_space_from_csv(csv_file_name, status_type, columns=None, add_negatives=False, subsystem=None):
    """
    :param subsystem: self explanatory
    :param status_type: str 'alarm', 'valve' or 'analog'
    :param csv_file_name: filename of csv file
    :param columns: specify columns in csv file, _name means drop
                    (1:ON, 2:OFF)
    :param add_negatives: Bool, if True then add extra negative examples every hour
    :return: chronological dataframe with a column for each distinct input parameter
    """
    df = read_csv(csv_file_name, columns=columns, subsystem=subsystem)
    df = process_status(df, status_type=status_type)
    logging.info("df shape: " + str(df.shape))
    df_status = create_status_space(df, add_negatives=add_negatives, status_type=status_type)
    logging.info("df_status shape: " + str(df_status.shape))
    return df_status
