import datetime
import logging
import os

import pandas as pd


def to_file_name(input_string):
    """
    :param input_string: str, string to make
    :return:
    """
    if input_string is None:
        return ""
    return input_string.replace("/", "_")


class DateParser(object):
    def __init__(self, date_format):
        if date_format:
            self.date_format = date_format
        else:
            self.date_format = '%Y-%m-%d %H:%M:%S.%f'

    def date_parser(self, val):
        """
        :param val: time in format
        :return: datetime object
        """
        return pd.to_datetime(val, format=self.date_format)


def round_time(dt, date_delta=datetime.timedelta(minutes=1), method=None):
    """Round a datetime object to a multiple of a timedelta
    :param dt: datetime.datetime object
    :param date_delta: timedelta object, rounds to a multiple of this, default 1 minute.
    :param method: str, round, ceil or floor
    """
    round_to = date_delta.total_seconds()
    epoch = datetime.datetime.utcfromtimestamp(0)
    seconds = (dt - epoch).seconds
    rounding_correction = round_to if method == "ceil" else 0 if method == "floor" else round_to / 2
    rounding = (seconds + rounding_correction) // round_to * round_to
    bloep = dt + datetime.timedelta(0, rounding - seconds, -dt.microsecond)
    return bloep


def add_examples(df_status, argument, is_list=False, delete_rest=False, pad=True):
    """
    :param df_status: dataframe
    :param is_list: whether the input argument is a list of timestamps or a frequency
    :param delete_rest: remove original timestamps or keep them
    :param pad: fill missing values or not
    :param argument: desired frequency of negative examples, or
    :return: original dataframe extended with more negative examples
    """
    logging.debug("adding examples...")
    if not is_list:
        frequency = datetime.timedelta(seconds=argument)
        first_index = min([df_status[c].first_valid_index() for c in df_status])
        start_index = round_time(first_index, date_delta=frequency, method="ceil")
        last_index = max([df_status[c].last_valid_index() for c in df_status])
        end_index = round_time(last_index, date_delta=frequency, method="floor")
        date_range = pd.date_range(start=start_index, end=end_index, freq=frequency)
    else:
        date_range = argument
    if pad:
        df_reindex = df_status.reindex(date_range, method="pad")
    else:
        df_reindex = df_status.reindex(date_range)
    logging.debug("sorting indexes...")

    if delete_rest:
        df_reindex.sort_index(inplace=True)
    else:
        df_reindex = df_reindex.append(df_status)
        df_reindex = df_reindex.sort_index()
        df_reindex = df_reindex.groupby(df_reindex.index).last()

    logging.debug("examples added!")
    return df_reindex


def add_to_path(inlist, string):
    if inlist and not os.path.exists(os.path.join(*inlist)):
        os.makedirs(os.path.join(*inlist))
    return inlist + [string]
