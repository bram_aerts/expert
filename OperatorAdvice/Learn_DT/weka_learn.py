import csv
import json
import os
import re

import numpy as np


def counter():
    __counter = 0

    def _counter():
        nonlocal __counter
        __counter += 1
        return __counter

    return _counter


def read_labels_features(filename):
    with open(filename) as f:
        labels, features = json.load(f)
    return labels, features


def read_columns(filename):
    with open(filename) as f:
        lab_columns, feat_columns = json.load(f)
    return feat_columns


class WekaNode(object):
    def __init__(self):
        self.criterion = None
        self.rightchild = None
        self.leftchild = None
        self.positives = None
        self.negatives = None

    def set_criterion(self, criterion):
        self.criterion = criterion

    def set_rightchild(self, node):
        self.rightchild = node

    def set_leftchild(self, node):
        self.leftchild = node

    def set_positives(self, value):
        self.positives = value

    def set_negatives(self, value):
        self.negatives = value

    def to_string(self):
        if self.negatives < self.positives:
            base_color = '399de5'
        else:
            base_color = 'e58139'
        try:
            factor = abs((self.positives - self.negatives) / (self.positives + self.negatives))
            saturation = hex(int(round(factor * 255)))[-2:].replace('x', '0')
        except ZeroDivisionError:
            saturation = '00'

        fillcolor = '#' + base_color + saturation
        nsamples = self.positives + self.negatives
        string = '[fillcolor="' + fillcolor + '", label = "'
        if self.criterion is not None:
            string += self.criterion + r'\n'
        string += 'samples = ' + str(nsamples) + r'\n'
        string += 'value = [' + str(self.negatives) + ',' + str(self.positives) + ']", penwidth = "1"];'
        return string

    def generate_negatives_positives(self):
        if self.positives is None or self.negatives is None:
            left_neg, left_pos = self.leftchild.generate_negatives_positives()
            right_neg, right_pos = self.rightchild.generate_negatives_positives()
            self.negatives = left_neg + right_neg
            self.positives = left_pos + right_pos
        return self.negatives, self.positives


class WekaTree(object):
    def __init__(self, alarm_name, string, negatives, positives, conf_text, balanced=False):
        self.alarm_name = alarm_name
        self.string = string.strip()
        self.total_negatives = negatives
        self.total_positives = positives
        if balanced:
            self.positives_factor = self.total_positives
            self.negatives_factor = self.total_negatives
        else:
            self.positives_factor = 1
            self.negatives_factor = 1
        self.root = None
        self.read_tree()
        self.counter = -1
        self.conf_text = conf_text

    def count(self):
        self.counter += 1
        return str(self.counter)

    def values2number(self, values, ispos=True):
        total_number = (self.negatives_factor + self.positives_factor)
        if ispos:
            pos_value = values[0] - values[1]
            neg_value = values[1]
        else:
            pos_value = values[1]
            neg_value = values[0] - values[1]

        pos_number = int(round(self.positives_factor * 2 * pos_value / total_number))
        neg_number = int(round(self.negatives_factor * 2 * neg_value / total_number))
        return [neg_number, pos_number]

    def process_subtree(self, lines):
        if len(lines) != 1:
            return None

        line = lines[0]
        group = re.findall(r'(?:: )(\d+(?:.\d+)? \(\d+(?:.\d+)?(?:/\d+(?:.\d+)?)?\))', line)
        class_type = float(group[0].split('(')[0])
        numbers = group[0].split('(')[1].split(')')[0].split('/')
        if len(numbers) == 1:
            numbers = float(numbers[0]), 0.0
        else:
            numbers = list(map(float, numbers))

        numbers = self.values2number(numbers, ispos=class_type == 1.0)

        node = WekaNode()
        node.negatives, node.positives = numbers
        return node

    @staticmethod
    def get_depth(line):
        return line.count('|   ')

    def read_subtree(self, prev_depth, lines):
        lines_left = []
        lines_right = []

        if len(lines) == 1 and lines[0].strip()[0] == ':':
            return self.process_subtree(lines)

        a = lines_left
        a.append(lines[0])

        for line in lines[1:]:
            if self.get_depth(line) == prev_depth:
                a = lines_right
            a.append(line)

        node = WekaNode()
        try:
            if re.findall(r': \d+(?:.\d+)? \(\d+(?:.\d+)?(?:/\d+(?:.\d+)?)?\)', lines[0]):
                node.criterion = re.findall(r'(?:[| ]*)(.+)(?:: \d+(?:.\d+)? \(\d+(?:.\d+)?(?:/\d+(?:.\d+)?)?\))',
                                            lines[0])[0]
            else:
                node.criterion = re.findall(r'(?:[| ]*)(.+)',
                                            lines[0])[0]
        except IndexError:
            print('oops, something went wrong')
        node_left = self.process_subtree(lines_left)
        if node_left is not None:
            node.leftchild = node_left
        else:
            node.leftchild = self.read_subtree(prev_depth + 1, lines_left[1:])

        node_right = self.process_subtree(lines_right)
        if node_right is not None:
            node.rightchild = node_right
        else:
            count_number_of_decisions = sum([self.get_depth(line) == prev_depth for line in lines_right])
            if count_number_of_decisions > 1:
                node.rightchild = self.read_subtree(prev_depth, lines_right)
            else:
                node.rightchild = self.read_subtree(prev_depth + 1, lines_right[1:])

        return node

    def read_tree(self):
        prev_depth = 0
        lines = self.string.split('\n')
        self.root = self.read_subtree(prev_depth, lines)
        self.root.generate_negatives_positives()

    def to_dot(self):
        header = r"""digraph Tree {
node [color="black", fontname=helvetica, shape=box, style="filled, rounded"];
edge [fontname=helvetica];"""
        pos_text = "positives: " + str(self.total_positives) + " / " + str(self.total_positives + self.total_negatives)

        footer = '1000000000 [label="' + self.alarm_name + "\n"
        footer += pos_text + '\n' + self.conf_text + '" fillcolor="#FFFFFF"]\n}'

        return header + '\n' + self._to_dot(self.root) + footer

    def _to_dot(self, node, parent=None):
        c = self.count()
        string = c + ' ' + node.to_string()

        if parent == '0' and c == '1':
            string += '\n' + parent + '->' + c
            string += ' [headlabel="True", labelangle=45, labeldistance="2.5", penwidth="1"];\n'
        elif parent == '0':
            string += '\n' + parent + '->' + c
            string += ' [headlabel="False", labelangle="-45",  labeldistance="2.5", penwidth="1"];\n'
        elif parent is not None:
            string += '\n' + parent + '->' + c + ' [penwidth="1"];\n'

        if node.leftchild is not None:
            string += self._to_dot(node.leftchild, c) + '\n'

            string += self._to_dot(node.rightchild, c) + '\n'
        return string


class WekaLearner(object):
    def __init__(self, alarm_name, columns, labels, features, output_dir='/tmp', output_file=None, options=None):
        self.alarm_name = alarm_name
        self.columns = columns
        self.labels = labels
        self.features = features
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        self.output_dir = output_dir

        if output_file is None:
            self.output_file = os.path.join(self.output_dir, 'weka_tree')
        else:
            self.output_file = output_file
        self._write_arff_file()
        if options is None:
            options = {}
        self.balanced = 'class_weight' in options and options['class_weight'] is not None
        self.pruned = 'pruned' in options and options['pruned'] is not None
        self._learn_weka()
        self.confusion_matrix = self._get_confusion_matrix()
        self._create_dotfile()

    def _write_arff_file(self):
        ofile = os.path.join(self.output_dir, 'weka_input.arff')
        with open(ofile, 'w') as outarff:
            outarff.write('@RELATION "' + self.alarm_name + '"\n\n')
            for i, column in enumerate(self.columns):
                list_of_feature_values = set([feature_row[i] for feature_row in self.features])
                outarff.write('@ATTRIBUTE "' + column + '" ')
                if len(list_of_feature_values) > 2:
                    outarff.write('numeric\n')
                else:
                    outarff.write('{' + ','.join([str(x) for x in list_of_feature_values]) + '}\n')
            outarff.write('@ATTRIBUTE label {0.0,1.0}\n')
            outarff.write('@DATA \n')
        with open(ofile, 'a') as outarff:
            writer = csv.writer(outarff)
            writer.writerows([feature_row + [label] for feature_row, label in zip(self.features, self.labels)])

    def _learn_weka(self):
        arff_file = os.path.join(self.output_dir, 'weka_input.arff')
        model_file = os.path.join(self.output_dir, 'weka_output.model')

        command_list = ['java -classpath ~/Programs/weka-3-8-0/weka.jar']
        if self.balanced:
            command_list += ['weka.classifiers.meta.FilteredClassifier',
                             '-F weka.filters.supervised.instance.ClassBalancer',
                             '-W weka.classifiers.trees.J48']

        else:
            command_list += ['weka.classifiers.trees.J48']

        # if not self.pruned:
        #     command_list += ['-O -R -N 3 -Q 1 -M 2']

        command_list += ['-t ' + arff_file.replace(' ', '\ '),
                         '-x 2',
                         '-U',
                         '> ' + model_file.replace(' ', '\ ')]
        print(' '.join(command_list))
        os.system(' '.join(command_list))

    def _get_confusion_matrix(self):
        model_file = os.path.join(self.output_dir, 'weka_output.model')
        with open(model_file) as f:
            string = f.read()
        results = string.split('Stratified cross-validation')[1].split('Confusion Matrix')[1]
        found = list(map(int, re.findall(r'(?<=\s)(\d+)(?=\s)', results)))

        matrix = np.array([[found[0], found[1]], [found[2], found[3]]])
        return matrix

    def get_confusion_matrix(self):
        return self.confusion_matrix

    def _get_tree(self):
        model_file = os.path.join(self.output_dir, 'weka_output.model')
        with open(model_file) as f:
            string = f.read()
        result = string.split('------------------')[1].split('Number of Leaves')[0].strip()
        return result

    def _create_dotfile(self):
        string = self._get_tree()
        conf_matrix = self.get_confusion_matrix()
        conf_text = str(conf_matrix)
        tree = WekaTree(self.alarm_name, string, sum(conf_matrix[0]), sum(conf_matrix[1]), conf_text,
                        balanced=self.balanced)
        dot_string = tree.to_dot()
        with open(self.output_file, 'w') as f:
            f.write(dot_string)


def main():
    string = ''': 0.0 (3367.0/6.0)
'''

    tree = WekaTree('bloeperdebloep', string, 3114, 136, '', balanced=True)
    tree.to_dot()

    with open('test2.dot', 'w') as f:
        print(tree.to_dot(), file=f)


if __name__ == '__main__':
    main()
