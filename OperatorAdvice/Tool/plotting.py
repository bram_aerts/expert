''' Project:	 E'XperT project
    File:		 plotting.py
    Description: Alarm management tool - plotting functions for statistics.
'''
__author__ = 'Shana Van Dessel'

import numpy as np
from copy import deepcopy
import calendar
import matplotlib.pyplot as plt
import mpld3
mpld3.enable_notebook()


		
# STATIC / DYNAMIC PLOT ---------------------------------------------------------------------------------------------------------

def ScatterPlot(xdata,ydata,labels,colors,xlabel=None,ylabel=None,title=None):
	''' Show scatter plot of the components' lambda parameters versus its number of occurrences,
		with interactive artists showing a label when hovered.
	'''
	fig, ax = plt.subplots(subplot_kw=dict(axisbg='#EEEEEE'),figsize=(12,8))
	ax.grid(color='white', linestyle='solid')

	if not title: title = 'Alarm messages'
	if not xlabel: xlabel = 'Poisson parameters'
	if not ylabel: ylabel = 'Number of occurrences'
	ax.set_title(title)
	ax.set_xlabel(xlabel)
	ax.set_ylabel(ylabel)

	scatter = ax.scatter(xdata, ydata, c=colors, s=25)
	tooltip = mpld3.plugins.PointLabelTooltip(scatter, labels=labels)  
	mpld3.plugins.connect(fig, tooltip)
	#mpld3.show()
	mpld3.enable_notebook()
	return

def GetStaticPlot(static_lam,static_noo,title):
	''' Create static scatter plot. 
	'''
	box25 = np.percentile(static_lam.values(), 25) , np.percentile(static_lam.values(), 1)
	#box75 = np.percentile(static_lam.values(), 75) , np.percentile(static_lam.values(), 99)
	colors = ['g' if (box25[0] <= static_lam[x]) #< box75[0]) 
		          else ('darkorange' if (box25[1] <= static_lam[x]) #< box75[1]) 
		                             else 'r') for x in static_lam]
	ScatterPlot(static_lam.values(), static_noo.values(), static_lam.keys(), colors, title = title)
	return

def GetDynamicPlot(dynamic_lam,dynamic_noo):
	''' Create dynamic scatter plot.
	'''
	colors = ['g' if dynamic_noo[x] < -0.5 else ('darkorange' if dynamic_noo[x] < 0.5 else 'r') for x in dynamic_noo]
	ScatterPlot(dynamic_lam.values(), dynamic_noo.values(), dynamic_lam.keys(), colors)
	return 


# DETAILED PLOT -----------------------------------------------------------------------------------------------------------------

def ExponentialFunction(x, lamb):
    ''' Exponential distribution. 
    '''
    return lamb * np.exp(-lamb*x)

def Histogram(data, lamb, element):
	bins = int(np.max(data))
	fig, ax = plt.subplots(subplot_kw=dict(axisbg='#EEEEEE'),figsize=(12, 5))
	ax.grid(color='white', linestyle='solid')
	ax.set_title('Histogram {} (L={})'.format(element,lamb))
	ax.set_xlabel("Time between successive alarms")
	ax.set_ylabel("Number of occurrences")
	hist = plt.hist(data,bins=bins)
	line = np.linspace(0, np.max(data), np.max(data)*10)
	ax.plot(line, ExponentialFunction(line, *[lamb])*int(np.max(hist[0])), 'r', lw=2)
	#mpld3.show()
	mpld3.enable_notebook()
	return

def Overview(data, element):
	''' Show an overview of the number of occurences (of the different messages).
	'''
	d = deepcopy(data)
	d['Total'] = d.sum(axis=1)
	cnt = d['Total'].sum()
	fig, ax = plt.subplots(subplot_kw=dict(axisbg='#EEEEEE'),figsize=(12, 5))
	ax.grid(color='white', linestyle='solid')
	ax.set_title('Overview {} (#={:d})'.format(element,cnt))
	ax.set_ylabel("Number of occurrences")
	d.plot(ax=ax)                                    
	#data.plot.bar(ax=ax,stacked=True)                                    
	plt.xticks(rotation='vertical')
	return

def Details(data, element):
	''' Show a detailed view of the number of occurences (of the different messages) per month using stacked bar plots. 
	'''
	gr = data.groupby(data.index.map(lambda x:'{}-{:02}'.format(x.year,x.month)))                   
	d_gr = {x:gr.get_group(x).copy() for x in gr.groups}
	for i,key in enumerate(sorted(d_gr)):   
		fig, ax = plt.subplots(subplot_kw=dict(axisbg='#EEEEEE'),figsize=(12, 5))
		ax.set_title('Details {} ({} {})'.format(element,calendar.month_name[int(key.split('-')[1])],key.split('-')[0]))
		ax.set_ylabel("Number of occurrences")
		d_gr[key].plot.bar(ax=ax, stacked=True)                 # plot per month
		if plt.ylim()[1] < 1:                                   # set axis for empty plots
			plt.ylim(0,1)
		if plt.ylim()[1] < 10:
			yt = int(plt.ylim()[1])
			plt.yticks(range(yt+1),range(yt+1),fontsize=10)
		plt.xticks(range(len(d_gr[key])),range(1,len(d_gr[key])+1))
		ax.legend().set_visible(False)                          # hide legend  
	return 

def GetDetailsPlot(deltas, lamb, counts, element=''):
	Histogram(deltas,lamb,element)
	Overview(counts,element)
	Details(counts,element)
	return 

# -------------------------------------------------------------------------------------------------------------------------------

def main():
	ScatterPlot([1,2,3],[4,5,6],['a','b','c'],['r','g','b'])
	#Histogram([1,2,1,2,3], 1.9, 'Component 1')
	return
	
if __name__ == "__main__":
   main()
