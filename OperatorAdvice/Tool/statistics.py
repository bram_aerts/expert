from __future__ import division, print_function

''' Project:	 E'XperT project
    File:		 statistics.py
    Description: Alarm management tool - statistics (number of occurences vs. poisson parameter lambda).
'''
__author__ = 'Shana Van Dessel'

import pandas 	as pd
import numpy 	as np
from copy 		import deepcopy
from datetime 	import timedelta
from datetime 	import date
from scipy.optimize import curve_fit
from scipy.misc 	import factorial
import calendar

Files = {'Acetate': "../Data_Acetate/acetateAlarms.csv",
         'Tracing': "../Data_Acetate/acetateAlarmsTr.csv" }

InDays = {'Day':1,'Week':7, 'Month':31, 'Half year':182, 'Year': 365}
    
Groupby = 'Category'        
Compare = ['Month','Half year']
Element = 'FT'

# (INPUT) DATA -----------------------------------------------------------------------------------------------------------------

# Input data
def GetInputData(files,systems=None):
    ''' Read the input data (one or more files, with the possibility to give the data from each file a label)
    	and remove irrelevant entries (alarms being acknowledged or going off / alarms that are not really errors).
    '''
    dfs = [pd.read_csv(f,encoding="utf-8-sig",parse_dates=['Timestamp']) for f in files]
    if systems and len(systems) == len(files):
        for i,x in enumerate(systems):
            dfs[i]['System'] = x
    df = pd.concat(dfs)
    if 'State' in df.columns:			# upcoming alarms (no OFF/ACK)
        df = df[df['State']==1]                                         
        del df['State']
    if 'Class' in df.columns:			# error messages (no System/Tolerance)
        df = df[df['Class']=='Error']
        del df['Class']
    df.sort_values(by='Timestamp',inplace=True)
    return df
    
def GetTimeSpan(df):
	''' Get time span of the data.
	'''
	return [min(df['Timestamp'])-timedelta(seconds=1),max(df['Timestamp'])+timedelta(seconds=1)]
	
def GetData(files,systems=None):
	''' Get the input data and its time span.
	'''
	df = GetInputData(files,systems)
	dfSpan = GetTimeSpan(df)
	return df, dfSpan

# Group data
def GetCategory(x):
	''' Return what category of component it is.
	'''
	y = x.split('-')
	# category at the front XX-00-00
	if y[0] in ['CF','FCV','FT','HS','HV','LSH','LSL','MS','PDIT','PIT','PT','PTWW','PWW','SP','TSH','TT','TTWW','XA','XS','XV','XVWW']:
		return y[0]
	# category at the end 00-00-XX
	if y[-1][:-1] in ['EL','EM','EP','EQ','ERA','ET','ETOV','EV']:
		return y[-1][:-1]
	# category at the front X000
	if y[0][0] in ['P','T']:
		return y[0][0]
	return '?'
	
def GetGroupedData(df,groupby=Groupby):
	''' Group data by one of its columns ('Message'/'Id'/'Group'/'System') or by the components' category. 
	'''
	if groupby in df.columns:
		grp = df.groupby(df[groupby])
	elif groupby ==  'Category':
		grp = df.groupby(df['Id'].map(GetCategory))
	else: 
		grp = df.groupby(df['Message'])               
	df_grp = {x: grp.get_group(x).copy() for x in grp.groups}    
	return df_grp


# STATIC DATA ------------------------------------------------------------------------------------------------------------------
	
# Number of occurrences
def GetNumberOfOccurrences(dfs):
    ''' Get for each key (Component/ID/Group) the number of occurrences. 
    '''
    return {key: dfs[key]['Message'].count() for key in dfs}

# Delta time
def AddPrior(dfs,dfSpan):
    ''' Add for each key (Component/ID/Group) an entry before and after its data.
    '''
    for key in dfs:
        for t in dfSpan:									# add 2 priors
            dfs_prior = dfs[key][:1].copy() 				# copy an entry
            dfs_prior['Timestamp'] = t 						# change its timestamp to one before/after the first/last known timestamp
            dfs[key] = pd.concat([dfs[key],dfs_prior])		# add this new entry to the data
        dfs[key].sort_values('Timestamp',inplace=True) 	# sort the data on timestamp
    return
    
def GetDeltas(dfs,dfSpan):
    ''' Get for each key (Component/ID/Group) the time between the entries (in hours, with a precision of 0.01 hour).
    '''
    dfs = deepcopy(dfs)
    AddPrior(dfs,dfSpan)
    dfs_delta = dict()
    for key in dfs:
        dfs_delta[key] = list(dfs[key]['Timestamp'] - dfs[key]['Timestamp'].shift(1))[1:]
        dfs_delta[key] = list(map((lambda x: x.days*24 + x.seconds//3600 + int((x.seconds%3600)/36)/100) , dfs_delta[key]))
    return dfs_delta

# Poisson parameter lambda
def ExponentialFunction(x, lamb):
    ''' Exponential distribution. 
    '''
    return lamb * np.exp(-lamb*x)

def GetLambdas(dfs_delta):
	''' Fit an exponential distribution on the histogram of the data per ID and return the resulting poisson parameters.
	'''
	dfs_lam = dict()
	for key in dfs_delta:
		bins = int(np.max(dfs_delta[key]))
		entries, bin_edges = np.histogram(dfs_delta[key],bins)  
		bin_middles = 0.5*(bin_edges[1:] + bin_edges[:-1])  
		parameters, _ = curve_fit(ExponentialFunction, bin_middles, entries, maxfev=1000)
		dfs_lam[key] = int(parameters[0]*1000)/1000  
	return dfs_lam

def GetStaticData(dfs,dfSpan):
	''' Get all data needed for the static plot. 
	'''
	dfs_noo = GetNumberOfOccurrences(dfs)
	dfs_del = GetDeltas(dfs,dfSpan)
	dfs_lam = GetLambdas(dfs_del)  
	return dfs_noo, dfs_lam, dfs_del

# DYNAMIC DATA ------------------------------------------------------------------------------------------------------------------
	
# Select data
def GetDataSelection(dfs,dfSpan,today=None,d_from=1,d_till=0):
	''' Get all data in the specified range of weeks. Starting from 'w_from' weeks ago, till 'w_till' weeks ago (from 'today').
	'''
	if not today: 
		today = date.today()
	t_from = today - timedelta(d_till + d_from -1)
	t_till = today - timedelta(d_till)
	if (t_from > dfSpan[1].date()) or (t_till < dfSpan[0].date()):
		print('There is no data of the specified selection. ({} -> {})'.format(t_from,t_till))
		return None
	weeks = d_from + d_till
	if (today-dfSpan[0].date()).days/7 < weeks:
		weeks = int((today-dfSpan[0].date()).days/7)
		#print("There isn't data of all the speficied weeks, only {} weeks where selected.".format(weeks))

	dfs_select = deepcopy(dfs)
	for key in sorted(dfs_select):
		dfs_select[key] = dfs_select[key][dfs_select[key]['Timestamp'].map(lambda x: t_from <= x.date() <= t_till)]
		if len(dfs_select[key]) == 0:
		    del dfs_select[key]
	return dfs_select, weeks, [t_from,t_till]

def GetDynamicData(dfs,dfSpan, compare=Compare):
	''' Get all data needed for the dynamic plot. 
	'''
	today = date(2016, 4, 13)
	# Current
	curr, curr_now, curr_ts = GetDataSelection(dfs,dfSpan,today,d_from=InDays[compare[0]],d_till=0)
	curr_lam = GetLambdas(GetDeltas(curr,dfSpan))
	curr_noo = GetNumberOfOccurrences(curr)
	# Previous
	prev, prev_now, prev_ts = GetDataSelection(dfs,dfSpan,today,d_from=(InDays[compare[0]]+InDays[compare[1]]),d_till=InDays[compare[0]])
	prev_lam = GetLambdas(GetDeltas(prev,dfSpan))
	prev_noo = GetNumberOfOccurrences(prev)
	# Difference
	scale = prev_now/curr_now
	diff_lam = {x: curr_lam[x] - prev_lam[x] for x in curr_lam if x in prev_lam}
	diff_noo = {x: curr_noo[x] - prev_noo[x]/scale for x in curr_noo if x in prev_noo}

	return diff_lam, diff_noo, [curr_ts,prev_ts]


# DETAILED DATA ----------------------------------------------------------------------------------------------------------------

# Number of occurrences per message
def GetNumberOfMessages(dfs, dfSpan, element):
	''' Count for each ID the number of times each message occured per date. 
	''' 
	df_nom = deepcopy(dfs[element])
	span = [dfSpan[0].date().replace(day=1) , dfSpan[1] + pd.offsets.MonthEnd(0)] 
	df_nom['Timestamp'] = df_nom['Timestamp'].map(lambda x: x.date())
	df_nom['Count'] = df_nom['Message'].map(lambda x: 1)
	df_nom = df_nom[['Timestamp','Message','Count']]
	df_nom = pd.pivot_table(df_nom,index=["Timestamp"],columns=["Message"],aggfunc=[sum])
	df_nom.columns = df_nom.columns.droplevel([0,1])    									# remove irrelevant top levels
	df_nom.replace(np.NaN,0,inplace=True)                      								# convert NaN to 0
	df_nom = df_nom.reindex(pd.date_range(span[0],span[1],normalize=True),fill_value=0)		# convert to dense index
	df_nom.index = df_nom.index.map(lambda x: x.date())
	return df_nom

def GetDetailsData(dfs,dfSpan,element=Element):
	''' Get all data needed for the detailed plots. 
	'''
	df_cnt = GetNumberOfMessages(dfs,dfSpan,element)
	return df_cnt

# ------------------------------------------------------------------------------------------------------------------------------

def main():
	data, dataSpan = GetData(Files.values(),Files.keys())
	#print('Data\n----\n{}\n'.format(data.head()))
	data_grp = GetGroupedData(data)
	#print('Groups ({})\n------\n{}\n'.format(len(data_grp),list(data_grp.keys())))
	sdata_noo, sdata_lam, sdata_del = GetStaticData(data_grp,dataSpan)
	ddata_noo, ddata_lam, ddata_ts = GetDynamicData(data_grp,dataSpan)
	data_cnt = GetDetailsData(data_grp,dataSpan)
	return
	
if __name__ == "__main__":
   main()
