''' Project: 	 E'XperT project
    File: 	 	 tool.py
    Description: Alarm management tool.
'''
__author__ = 'Bram Aerts, Shana Van Dessel'

#from __future__ import division, print_function

import statistics
#import pandas as pd
#import numpy as np
#from copy import deepcopy
#from datetime import timedelta
#from datetime import date
#import calendar
#from scipy.optimize import curve_fit
#from scipy.misc import factorial

#import ipywidgets as ipw

#from IPython.display import Image, display_png, display
#from matplotlib.pyplot import imshow
#import os
#import re
#import pretty_tree
#import time
#import collections
#import glob
#import subprocess
#import sys

#import matplotlib.pyplot as plt
#import mpld3
#mpld3.enable_notebook()
    
#import warnings
#warnings.filterwarnings('ignore')

Files = {'Acetate': "../Data_Acetate/acetateAlarms.csv",
         'Tracing': "../Data_Acetate/acetateAlarmsTr.csv" }

Groupby = 'Kind'					# Choose the group by criterium
Compare = ['month', 'half year'] 	# Choose timespan to compare
Element = 'MS'						# Choose an element to take a closer look at 	

def GetData(files=Files):
	data, dataSpan = statistics.GetData(files.values(),files.keys())
	return data, dataSpan
	
def GetGrouped(data, dataSpan, groupby=Groupby):
	data_grp = statistics.GetGroupedData(data,groupby)
	data_cnt = statistics.GetDetailedData(data_grp,dataSpan)
	return data_grp, data_cnt

def GetStatic(data_grp,dataSpan):
	data_noo, data_lam, data_del = statistics.GetStaticData(data_grp,dataSpan)
	#TODO PLOT
	return data_noo, data_lam, data_del

def GetDynamic(data_grp, dataSpan, compare=Compare):
	data_noo, data_lam, data_ts = statistics.GetDynamicData(data_grp,dataSpan,compare)
	#TODO PLOT
	return

def GetDetails(data_noo, data_lam, data_del, data_cnt, element=Element):
	#data_noo[element], data_lam[element], data_del[element], data_cnt[element]
	#TODO PLOT
	return 

def GetDecisionTree():
	#TODO
	return

def main():
	data, dataSpan = GetData()
	data_grp, data_cnt = GetGrouped(data, dataSpan)
	data_noo, data_lam, data_del = GetStatic(data_grp, dataSpan)
	GetDynamic(data_grp, dataSpan)
	GetDetails(data_noo, data_lam, data_del, data_cnt)
	return
	
if __name__ == "__main__":
   main()
