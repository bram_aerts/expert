""" Project: 	 E'XperT project
	File: 	 	 compressProcessVariables.py
	Description: Compress the data about the process variables from the database 
				 by creating an onChange, tresholded list.
"""
__author__ = "Shana Van Dessel"

import sys
import copy
import collections

PError = True																# *error print* on/off
PDebug = False 																# *debug print* on/off
PInfo = True 																# *info print*  on/off

inputFile  = "nextProcessVariables.txt"										# next data to process
outputFile = "processVariables.csv" 										# processed data
tmpFile    = "prevProcessVariables.csv"										# temporarily store the last results

def CompressProcessVariables(fin=inputFile,fout=outputFile,ftmp=tmpFile):
	""" Compress queried data day by day. Read and compress the queried data 
		and write the remaining data. Hereby taking into account the results 
		from the previous day and storing this day's results for the next day.
	"""
	logPrev = ReadLastResults(ftmp)											# read last results from previous run
	data, date = ReadAndCompressData(fin)									# read and compress the queried data 
	logNext = WriteData(fout, data, date, logPrev)							# write data (taking into account last run's results)
	WriteLastResults(ftmp,logNext)											# write last results from this run
	return

# READ & COMPRESS ----------------------------------------------------------------------------------------------------------------------

def ReadAndCompressData(file):
	""" Read the queried data from a file and compress this data (while reading),
		using an onChange policy and extra thresholding for analog components (ending on '-EU').
	"""
	if PInfo: cntLns = 0													# *info print*
																	
	data = dict()															# key: id, values: dict of ts (key: time, value: time2,gr,vf)
	date = None
	with open(file,'r') as f:												# open input file
		for _ in range(4):													# skip first 2 lines incl. the empty line between them
			next(f)							
		
		for line in f:	
			if len(line.strip()) <= 1:										# check for end of file
				PrintX("End of file.",'d')									# *debug print* 
				break
		
			if PInfo: cntLns +=1											# *info print*
			id, gr, date, vf, time = GetParts(line)							# split line in wanted parts
			PrintX("L{:<8} | {:24} | {:10} | {:24} | T{:<5}"
				.format(cntLns, id, ToTimestamp(time), vf, time),'d')
			
			if id not in data:												# new id, create new time
				data[id] = collections.OrderedDict()
				data[id][time] = {'vf':vf,'gr':gr,'time2':time}
				next(f)														
				continue
			
			prv, nxt = Locate(id,data[id],time,vf)							# find previous and next time 
			
			if (prv is None) or not Compress(id,vf,data[id][prv]['vf']):	# create new
				data[id][time] = {'vf':vf,'gr':gr,'time2':time}
			else:															# alter previous
				data[id][prv]['time2'] = time
				time = prv
				
			if (nxt is not None) and Compress(id,vf,data[id][nxt]['vf']):	# alter next
				data[id][time]['time2'] = data[id][nxt]['time2']
				del(data[id][nxt])
				
			next(f)		
			
	PrintX("# entries:      {}".format(cntLns), 'i')						# *info print* 
	return data, date
	
def GetParts(s):
	""" Get parts from line, remove NULL and SPACE.
	"""
	parts = list()
	
	s2 = ""	
	for c in s.strip():														# remove ascii NULL characters
		if ord(c) != 00:						
			s2 += c
			
	cnt = 0
	part = ""		
	for c in s2:															# remove redundant SPACE characters and split into the right parts
		if c == ' ':
			cnt += 1
			continue
		elif cnt == 1:
			part += ' '
		elif cnt > 1:
			parts.append(part)
			part = ""
		part += c
		cnt = 0
	parts.append(part)	
	
	if len(parts) > 4:														# check if line matches the right format
		PrintX("! Line does not match format. {}".format(parts),'e')		# *error print*
		parts = parts[:4]
		
	ts = parts[2]															# split timestamp in date and time (in seconds)
	d,t = (ts.split('.')[0]).split(' ')										# first split in date and time
	parts[2] = d
	parts.append(FromTimestamp(t))
	return parts

def Locate(id, times, time, vf):
	""" Determine whether or not the time connects to any known time (before as wel as after).
		the time connects to another when their is exactly 2 seconds between them.
	"""
	prv = [next(reversed(times))][0]										# get latest time available
	nxt = None
	
	if (time - prv) < 2:													# before last (<2s)
		PrintX("! Time not in line with last time. ({} - {} (p:{}))"
				.format(id,time,prv),'e')										# *error print*
		prv = None										
		for t in times:														# find right location
			if t > time:
				nxt = t
				break
			prv = t
			
	if prv is not None and (time - times[prv]['time2']) != 2:				# not in line with previous
		PrintX("Time not in line with the previous one. ({} - {} (p:{}))"
				.format(id,time,prv),'e')									# *error print* 
		prv = None
		
	if nxt is not None and (nxt - time) != 2:								# not in line with next
		PrintX("Time not in line with the next one. ({} - {} (n:{}))"
				.format(id,time,nxt),'e')									# *error print* 
		nxt = None
	return prv,nxt
	
def Compress(id,val1,val2):
	""" Check if 2 lines can be compressed to one, based on it's value. For 'EU' values an on change policy is applied
		and for 'VISU' values the values are first thresholded.
	"""
	if (id.endswith("-VISU")):
		return val1 == val2
	
	if (id.endswith("-EU")):		# XXX TODO XXX
		v1 = round(float(val1),2)
		v2 = round(float(val2),2)
		return v1 == v2
		
	PrintX("! Unknown ID-tag (onChange policy used). {}".format(id),'e')
	return val1 == val2
	
# WRITE --------------------------------------------------------------------------------------------------------------------------------

def WriteData(file,data,date,logged):
	""" Write the compressed data to the result file, taking into account the last results of the previous day/run
		(only needed when adding the first timestamp of this day/run).
	"""
	if PInfo:																# check *info print* (otherwise not relevant)
		cntAdd = 0															# count added lines 
		cntIds = list()														# count changed id's
	
	with open(file,'a') as f:												
		for id in data:
			first = True
			for t in data[id]:
				vf = data[id][t]['vf']
				gr = data[id][t]['gr']
				ts = date + " " + ToTimestamp(t)							# create timestamp (date + time)
				
				if first:	
					first = False
					if (id in logged) and Compress(id,vf,logged[id]['vf']):	# check if the value is the same as the last one of the previous run
						continue
						
				f.write("{},{},{},{}\n".format(id,gr,vf,ts))				# add line
				if PInfo:													# check *info print* (otherwise not relevant)
					cntAdd += 1											
					if(id not in cntIds): cntIds.append(id)				
					
			logged[id] = {'gr':gr, 'vf':vf, 'ts':ts}						# log last results of this run

	PrintX("# changed ID's: {}".format(str(len(cntIds))), 'i')				# *info print* 
	PrintX("# added rules:  {}".format(str(cntAdd)), 'i')					# *info print* 
	return logged
	
# Store(d) last results ----------------------------------------------------------------------------------------------------------------

def ReadLastResults(file):
	""" Get the last results from the previous day/run (if there was any previous day/run).
	"""
	logPrev = dict()														# key: id, values: Group, ValueFloat, Timestamp
	try:												
		with open(file,'r') as f:						
			for line in f:
				id, gr, vf,ts = line.split(',')
				logPrev[id] = {'gr':gr, 'vf':vf, 'ts':ts.strip()}	
		PrintX("# logged ID's:  {}".format(str(len(logPrev))),'d')			# *debug print*
		
	except (FileNotFoundError):												
		PrintX("! Found no previous results.",'e')							# *error print*
		pass
	return logPrev
	
def WriteLastResults(file,logNext):
	""" Set the last results from this day/run (dict keys = id's, values = Group, ValueFloat, Timestamp)
	"""
	with open(file,'w') as f:
		for id in logNext:
			f.write("{},{},{},{}\n".format(id,logNext[id]['gr'],logNext[id]['vf'],logNext[id]['ts']))
					 
	PrintX("# logged ID's:  {}".format(str(len(logNext))),'d')				# *debug print*
	return 

# --------------------------------------------------------------------------------------------------------------------------------------
	
def FromTimestamp(s):
	""" Convert timestamp between 00:00:00 and 23:59:59 to value between 0 and 86399.
	"""
	p = s.split(':')
	i = int(p[0])*60*60 + int(p[1])*60 + int(p[2])
	return i
	
def ToTimestamp(i):
	""" Convert value between 0 and 86399 to timestamp between 00:00:00 and 23:59:59. 
	"""
	tsH,i = divmod(i,3600)
	tsM,tsS = divmod(i,60)
	s = "{:02d}:{:02d}:{:02d}".format(tsH,tsM,tsS)
	return s
	
def PrintX(string,type):
	""" Print different types of messages (info, debug or error).
	"""
	if (PError and type == 'e') or (PDebug and type == 'd') or (PInfo  and type == 'i'):
		print("    " + type + ": " + string)
	return
	
def main():
	""" Call 'CompressProcessVariables()' with various arguments.
	"""
	if len(sys.argv) > 2: 	CompressProcessVariables(fin=sys.argv[1],fout=sys.argv[2])
	elif len(sys.argv) > 1: CompressProcessVariables(fin=sys.argv[1])
	else: 					CompressProcessVariables()
	PrintX("Done.",'d')														# *debug print*
	
if __name__ == "__main__":
	main()