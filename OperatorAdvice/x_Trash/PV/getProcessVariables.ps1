# Project:		E'XperT project
# Author:		S. Van Dessel
# File: 		getProcessVariables.ps1
# Description: 	get all process variables of components of the acetate installation. 

# Get process variables for all ID's, day per day, and compress data --------------------------------------------------------------------------------------------

echo "" "+---------------------------------------------------------------+" 
echo    "| GET PROCESS VARIABLES FROM DATABASE (per day)                 |"
echo    "+---------------------------------------------------------------+" ""
$sw = [Diagnostics.Stopwatch]::StartNew()																	

$initDay = Get-Date -Date "2015-12-13"																	# Set initial date
$lastDay = Get-Date -Date "2016-01-12"																	# Set final date

for ($day = $initDay; $day -le $lastDay; $day = $day.AddDays(1))										# Stay between initial and final date																				
{	
	echo "$(Get-Date $day -Format dd/MM/yyyy)" 															# Print day
			
	$strDay = Get-Date $day -Format yyyy-MM-dd															# Create strings for the start and end of the day
	$start = "'" + $strDay + " 00:00:00.2460000 +01:00'"
	$end = "'" + $strDay + " 23:59:59.9999999 +01:00'"
	
	echo "-> SQL extraction..."
	$swi = [Diagnostics.Stopwatch]::StartNew()															
	sqlcmd -S DESKTOP-TR5G6TI\HISTORIANSTORAGE -d HistorianStorage -Q "SELECT TOP 36504000 [Id], [GroupId], [Timestamp], [Value] FROM [VAcetateTag] WHERE [Timestamp] between $start and $end" > nextProcessVariables.txt
	$swi.Stop()																									
	$ti = $swi.Elapsed.Hours, $swi.Elapsed.Minutes, $swi.Elapsed.Seconds, $swi.Elapsed.Milliseconds		# Print SQL execution time					
	echo "   [$($ti[0])h $($ti[1])m $($ti[2]).$($ti[3])s]"								
	
	echo "-> Python compression..."
	$swi = [Diagnostics.Stopwatch]::StartNew()															
	python compressProcessVariables.py nextProcessVariables.txt
	$swi.Stop()																									
	$ti = $swi.Elapsed.Hours, $swi.Elapsed.Minutes, $swi.Elapsed.Seconds, $swi.Elapsed.Milliseconds		# Print Python execution time
	echo "   [$($ti[0])h $($ti[1])m $($ti[2]).$($ti[3])s]"				
	
	$te = $sw.Elapsed.Days, $sw.Elapsed.Hours, $sw.Elapsed.Minutes, $sw.Elapsed.Seconds					# Print total time so far
	echo "" "[$($te[0])d $($te[1])h $($te[2])m $($te[3])s]" ""
}

$sw.Stop()	
echo "-----------------------------------------------------------------"
$te = $sw.Elapsed.Days, $sw.Elapsed.Hours, $sw.Elapsed.Minutes																							
echo "Retrieved $(($lastDay-$initDay).Days + 1) days of data in $($te[0])d $($te[1])h $($te[2])m." ""	# Print findings & time