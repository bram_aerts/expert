""" E'XperT project - attempt_1.py: Search for patterns in alarm logging data (first attempt)
"""
__author__ = "Shana Van Dessel, Bram Aerts and Wannes Meert"
from __future__ import print_function
from datetime import datetime
import logging as lg
import pandas as pd
import sys
import re

from ppmg import PPMG

logModes = {'debug': lg.DEBUG,'info': lg.INFO,'warning': lg.WARNING,'error': lg.ERROR,'critical': lg.CRITICAL}

componentInputFile = "../../Data/component_testfile1.csv"	# component data
alarmInputFile = "../../Data/alarm_testfile.csv"			# alarm logging data
graphOutputFile = "attempt1.dot"							# resulting graph(s)

# ****************************************************************************************************
def Attempt1(fAlarm=alarmInputFile,fComp=componentInputFile,fOut=graphOutputFile):
	""" Try to find patterns in the alarm logging data, using specific components
	"""
	print("Read alarm logging data from CSV file (" + fAlarm.split('/')[-1] + ") ...")
	alarmData = ReadAlarmsFromCsv(fAlarm)
	alarmData["description_ack"] = alarmData["description"] + ' ' + alarmData["ack"]
	
	print("Read component data from CSV file (" + fComp.split('/')[-1] + ") ...")
	compData = ReadComponentsFromCSV(fComp)
	# TODO

	
	return

# ----------------------------------------------------------------------------------------------------

def ReadAlarmsFromCsv(file):
	""" Get alarm logging data from csv file.
	"""
	data = pd.read_csv(file, sep=',', index_col=0, parse_dates=[["date","time"]], date_parser=date_parser)
	lg.info("  Alarm data:\n" + str(data[:10]) + "\n...\n")
	return data

def date_parser(x, y):
	return datetime.strptime(x+' '+y, '%d/%m/%y %H:%M:%S')

# ----------------------------------------------------------------------------------------------------

def ReadComponentsFromCSV(file):
	""" Get component data from csv file.
	"""
	data = pd.read_csv(file, sep=',', index_col=0)
	lg.info("  Component data:\n" + str(data[:10]) + "\n...\n")
	return data

# ----------------------------------------------------------------------------------------------------

def Patterns(fn,seq,depth,occur):

	PatternsNaive(fn,data)
	PatternsOn(fn,data)
	return

def SearchPatterns(seq,depth=6):
	lg.info("  ppmg with depth of {}".format(depth))
	model = PPMG(depth=depth, autofill_graph="greedy")
	model.learn_sequence(seq)
	return model

def VisualizePatterns(fn,model,occur=1):
	""" Create graph (.dot) of model, include only patterns which occur enough."""
	lg.info("  Graph with minimal appearance of {}".format(occur))
	with open(fn, 'w') as f:
		print(model.to_dot(min_amount=occur),file=f)
	return



def PatternsOn(fn,data):
	fn = CreateFilename(fn,post="On")
	dataOn = data[data["ack"]=="ON"]
	seq = dataOn["description"]
	
	if len(seq) == 0:
		lg.error(" No sequence found!")
		return

	lg.info("  Data for upcoming alarms patterns:\n" + "\n".join( (seq[:10] if len(seq)>10 else seq) ))
	SearchForPatterns(fn,seq)
	return

def PatternsNaive(fn,data,depth=8,occur=1):
	fn = CreateFilename(fn,post="Naive")
	seq = data["description_ack"]
	
	if len(seq) == 0:
		lg.error(" No sequence found!")
		return

	lg.info("  Data for naive patterns:\n" + "\n".join( (seq[:10] if len(seq)>10 else seq) ))
	SearchForPatterns(fn,seq,depth=depth,occur=occur)
	return

# ****************************************************************************************************

def Logging():
	""" Logging. """
	if len(sys.argv) > 1: 
		lg.basicConfig(level=logModes.get(sys.argv[1], lg.NOTSET))
	else:
		lg.basicConfig(level=lg.CRITICAL)
	return

# ****************************************************************************************************

def CreateFilename(base,pre='',post=''):
	name = base.split('/')[-1]
	path = base[:-len(name)]
	name,ext = name.split('.')
	fn = path + pre + name + post + '.' + ext
	lg.debug(" " + base.split('/')[-1] + " -> " + fn.split('/')[-1])
	return fn

def main():
	Logging()
	Attempt1()
	print("Done.")
	
if __name__ == "__main__":									
	main()
