from __future__ import print_function
""" E'XperT project - patterns.py: Search for patterns in alarm logging
"""
__author__ = "Shana Van Dessel, Bram Aerts and Wannes Meert"

import logging as lg
import pandas as pd
import sys
import re
from datetime import datetime

from ppmg import PPMG

logModes = {'debug': lg.DEBUG,'info': lg.INFO,'warning': lg.WARNING,'error': lg.ERROR,'critical': lg.CRITICAL}

attempt = 1
xmlFile = "../../Data/component_testfile1.csv"

alarmInputFile = "../../Data/alarm_testfile.csv"	# alarm logging data
graphOutputFile = "Graphs/patterns.dot"				# resulting graph(s)

# ****************************************************************************************************
def Patterns(fCsv=alarmInputFile,fXml=xmlFile,fDot=graphOutputFile,attempt=attempt):
	print("Read alarm logging data from csv file (" + fCsv.split('/')[-1] + ") ...")
	data = ReadAlarmsFromCsv(fCsv)
	data["description_ack"] = data["description"] + ' ' + data["ack"]
	
	if attempt == 1:   
		fDot = CreateFilename(fDot,pre="attempt1")	
		Attempt1(fXml,fDot,data)
	elif attempt == 2: 
		fDot = CreateFilename(fDot,pre="attempt2")	
		Attempt2(fXml,fDot,data)
	elif attempt == 3: 
		fDot = CreateFilename(fDot,pre="attempt3")	
		Attempt3(fXml,fDot,data)
	return

def Attempt1(fn,data):
	""" Try to find (useful) patterns in the alarm logging data, using specific components
	"""
	print("Attempt 1 ...")
	PatternsNaive(fn,data)
	PatternsOn(fn,data)
	# TODO
	return

def Attempt2(fn,data):
	""" Try to find (useful) patterns in the alarm logging data, using specific components
		with data about the influence between components (components which influence eachother
		are grouped together).
	"""
	print("Attempt 2 ...")
	# TODO	
	return

def Attempt3(fn,data):
	""" Try to find (useful) patterns in the alarm logging data, using specific components
		with data about the specific link between components (all connections between components 
		are specified).
	"""
	print("Attempt 3 ...")
	# TODO	
	return

def CreateFilename(base,pre='',post=''):
	name = base.split('/')[-1]
	path = base[:-len(name)]
	name,ext = name.split('.')
	fn = path + pre + name + post + '.' + ext
	lg.debug(" " + base.split('/')[-1] + " -> " + fn.split('/')[-1])
	return fn

# ----------------------------------------------------------------------------------------------------

def ReadAlarmsFromCsv(file):
	""" Get alarm logging data from csv file.
	"""
	data = pd.read_csv(file, sep=',', index_col=0, parse_dates=[["date","time"]], date_parser=date_parser)
	lg.info("  Data:\n" + str(data[:10]) + "\n...\n")
	return data

def date_parser(x, y):
	return datetime.strptime(x+' '+y, '%d/%m/%y %H:%M:%S')

# ----------------------------------------------------------------------------------------------------

def SearchForPatterns(fn,seq,depth=6,occur=1):
	lg.info("  ppmg with depth of {} and minimal appearance of {}".format(depth,occur))
	model = PPMG(depth=depth, autofill_graph="greedy")
	model.learn_sequence(seq)
	with open(filename, 'w') as f:
		print(model.to_dot(min_amount=occur),file=f)
	print("... " + fn.split('/')[-1])
	return

# -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

def PatternsOn(fn,data):
	fn = CreateFilename(fn,post="On")
	dataOn = data[data["ack"]=="ON"]
	seq = dataOn["description"]
	
	if len(seq) == 0:
		lg.error(" No sequence found!")
		return

	lg.info("  Data for upcoming alarms patterns:\n" + "\n".join( (seq[:10] if len(seq)>10 else seq) ))
	SearchForPatterns(fn,seq)
	return

def PatternsNaive(fn,data,depth=8,occur=1):
	fn = CreateFilename(fn,post="Naive")
	seq = data["description_ack"]
	
	if len(seq) == 0:
		lg.error(" No sequence found!")
		return

	lg.info("  Data for naive patterns:\n" + "\n".join( (seq[:10] if len(seq)>10 else seq) ))
	SearchForPatterns(fn,seq,depth=depth,occur=occur)
	return

# ****************************************************************************************************

def Logging():
	""" Logging. """
	if len(sys.argv) > 1: 
		lg.basicConfig(level=logModes.get(sys.argv[1], lg.NOTSET))
	else:
		lg.basicConfig(level=lg.CRITICAL)
	return

# ****************************************************************************************************

def main():
	Logging()
	Patterns()
	print("Done.")
	
if __name__ == "__main__":									
	main()
